# C:\Users\Jonilive\AppData\Roaming\NZXT CAM\logs\renderer.log
# 
# exemplos que ocorreram com os perfis gerados
# {'modeGroup': 'Preset', 'modeName': 'Fixed', 'ledRgb': {'r': 81, 'g': 0, 'b': 122}}
# {'modeGroup': 'Preset', 'modeName': 'Fixed', 'ledRgb': {'r': 81, 'g': 0, 'b': 122}}
# {'modeGroup': 'Audio', 'modeName': 'Bass', 'ledRgb': {'r': 255, 'g': 0, 'b': 0}}
# {'modeGroup': 'Preset', 'modeName': 'Fixed', 'ledRgb': {'r': 26, 'g': 198, 'b': 255}}
# {'modeGroup': 'Preset', 'modeName': 'Pulse', 'speed': 'normal', 'ledRgbs': [{'r': 255, 'g': 0, 'b': 0}, {'r': 255, 'g': 0, 'b': 0}]}
# {'modeGroup': 'Preset', 'modeName': 'Fixed', 'ledRgb': {'r': 170, 'g': 0, 'b': 255}}
# {'modeGroup': 'Preset', 'modeName': 'SpectrumWave', 'speed': 'normal', 'direction': 'forward'}
# {'modeGroup': 'Preset', 'modeName': 'CoveringMarquee', 'speed': 'normal', 'direction': 'forward', 'ledRgbs': [{'r': 0, 'g': 0, 'b': 255}, {'r': 0, 'g': 0, 'b': 0}, {'r': 0, 'g': 255, 'b': 0}, {'r': 0, 'g': 0, 'b': 0}]}
# {'modeGroup': 'Preset', 'modeName': 'Fixed', 'ledRgb': {'r': 255, 'g': 255, 'b': 255}}
# {'modeGroup': 'Preset', 'modeName': 'RainbowFlow', 'speed': 'slow', 'direction': 'forward'}
# {'modeGroup': 'Preset', 'modeName': 'SuperRainbow', 'speed': 'normal', 'direction': 'forward'}
# {'modeGroup': 'Preset', 'modeName': 'CoveringMarquee', 'speed': 'normal', 'direction': 'forward', 'ledRgbs': [{'r': 0, 'g': 201, 'b': 255}, {'r': 255, 'g': 0, 'b': 205}]}
# {'modeGroup': 'Preset', 'modeName': 'Fixed', 'ledRgb': {'r': 81, 'g': 0, 'b': 122}}
import re, time, json, os

def fixjson(s):
    s = str(s).replace("'", '"')
    s = re.sub("((?=D)w+):", r'"1":',  s)
    s = s.replace('modeGroup:', '"modeGroup":')
    s = s.replace('modeName:', '"modeName":')
    s = s.replace('speed:', '"speed":')
    s = s.replace('ledRgb:', '"ledRgb":')
    s = s.replace('ledRgbs:', '"ledRgbs":')
    s = s.replace('direction:', '"direction":')
    s = s.replace('ledSize:', '"ledSize":')
    s = s.replace('moving:', '"moving":')
    s = s.replace('colorsWithNumbers:', '"colorsWithNumbers":')
    s = s.replace('value:', '"value":')
    s = s.replace(' r:', ' "r":')
    s = s.replace(' g:', ' "g":')
    s = s.replace(' b:', ' "b":')

    result = s
    return result

nzxt_file = r"C:\Users\Jonilive\AppData\Roaming\NZXT CAM\logs\renderer.log"
previous_mtime = ""

try:
    while True:
        time.sleep(1)

        if(os.stat(nzxt_file).st_mtime == previous_mtime):
            continue

        previous_mtime = os.stat(nzxt_file).st_mtime

        file = open(nzxt_file, "r")
        string = file.read()
        match = re.findall("(?<='channel 2',)(.*?)(?=\]\n\[)", string, flags=re.S)  
        jsonstring = fixjson(match[-1])
        #print( jsonstring )
        NZXT = json.loads(jsonstring)
        print( NZXT['modeName'] )
        exit()

except KeyboardInterrupt:
    pass