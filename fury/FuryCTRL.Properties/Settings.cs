using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace FuryCTRL.Properties;

[CompilerGenerated]
[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
internal sealed class Settings : ApplicationSettingsBase
{
	private static Settings defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());

	public static Settings Default => defaultInstance;

	[UserScopedSetting]
	[DebuggerNonUserCode]
	[DefaultSettingValue("True")]
	public bool bStartup
	{
		get
		{
			return (bool)this["bStartup"];
		}
		set
		{
			this["bStartup"] = value;
		}
	}

	[UserScopedSetting]
	[DebuggerNonUserCode]
	[DefaultSettingValue("2")]
	public uint UI_Style
	{
		get
		{
			return (uint)this["UI_Style"];
		}
		set
		{
			this["UI_Style"] = value;
		}
	}

	[UserScopedSetting]
	[DebuggerNonUserCode]
	[DefaultSettingValue("0")]
	public double WindowWidth
	{
		get
		{
			return (double)this["WindowWidth"];
		}
		set
		{
			this["WindowWidth"] = value;
		}
	}

	[UserScopedSetting]
	[DebuggerNonUserCode]
	[DefaultSettingValue("0")]
	public double WindowHeight
	{
		get
		{
			return (double)this["WindowHeight"];
		}
		set
		{
			this["WindowHeight"] = value;
		}
	}
}
