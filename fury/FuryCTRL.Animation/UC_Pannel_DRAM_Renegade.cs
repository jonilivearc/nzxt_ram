using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace FuryCTRL.Animation;

public class UC_Pannel_DRAM_Renegade : UserControl, IComponentConnector
{
	private Style StyleResource;

	private Storyboard[] Array_Storyboard = new Storyboard[1];

	private int SleepTime = 10;

	internal Grid LayoutRoot;

	internal Path bg;

	internal Canvas Running_default;

	internal Path Running_default_1;

	internal Path Running_default_2;

	internal Path Running_default_3;

	internal Path Running_default_4;

	internal Path Running_default_5;

	internal Path Running_one;

	internal Canvas Running_five;

	internal Path Running_five_1_5;

	internal Path Running_five_1_4;

	internal Path Running_five_1_3;

	internal Path Running_five_1_2;

	internal Path Running_five_1_1;

	internal Path Running_five_2_5;

	internal Path Running_five_2_4;

	internal Path Running_five_2_3;

	internal Path Running_five_2_2;

	internal Path Running_five_2_1;

	internal Path Running_five_3_5;

	internal Path Running_five_3_4;

	internal Path Running_five_3_3;

	internal Path Running_five_3_2;

	internal Path Running_five_3_1;

	internal Path Running_five_4_5;

	internal Path Running_five_4_4;

	internal Path Running_five_4_3;

	internal Path Running_five_4_2;

	internal Path Running_five_4_1;

	internal Path Running_five_5_5;

	internal Path Running_five_5_4;

	internal Path Running_five_5_3;

	internal Path Running_five_5_2;

	internal Path Running_five_5_1;

	internal Path Breath_default;

	internal Path Breath_one;

	internal Canvas Breath_five;

	internal Path Breath_five_5;

	internal Path Breath_five_4;

	internal Path Breath_five_3;

	internal Path Breath_five_2;

	internal Path Breath_five_1;

	internal Path Rainbow_default;

	internal Path Meteor_default;

	internal Path Meteor_one;

	internal Path Blink_default;

	internal Path Blink_one;

	internal Canvas Blink_five;

	internal Path Blink_five_1;

	internal Path Blink_five_2;

	internal Path Blink_five_3;

	internal Path Blink_five_4;

	internal Path Blink_five_5;

	internal Path Static_default;

	internal Path Static_one;

	internal Canvas Static_five;

	internal Path Static_five_1;

	internal Path Static_five_2;

	internal Path Static_five_3;

	internal Path Static_five_4;

	internal Path Static_five_5;

	internal Path DoubleBlink_default;

	internal Path DoubleBlink_one;

	internal Canvas DoubleBlink_five;

	internal Path DoubleBlink_five_1;

	internal Path DoubleBlink_five_2;

	internal Path DoubleBlink_five_3;

	internal Path DoubleBlink_five_4;

	internal Path DoubleBlink_five_5;

	internal Path ColorCycle_default;

	private bool _contentLoaded;

	public UC_Pannel_DRAM_Renegade()
	{
		InitializeComponent();
	}

	public void SetStyleEffect(string sMode, string sOption, byte[] In_ColorValue0, byte[] In_ColorValue1, byte[] In_ColorValue2, byte[] In_ColorValue3, byte[] In_ColorValue4)
	{
		for (int i = 0; i < Array_Storyboard.Length; i++)
		{
			if (Array_Storyboard[i] != null)
			{
				Array_Storyboard[i].Stop();
			}
		}
		byte a = byte.MaxValue;
		byte a2 = byte.MaxValue;
		byte a3 = byte.MaxValue;
		byte a4 = byte.MaxValue;
		byte a5 = byte.MaxValue;
		if (In_ColorValue0[0] <= 15 && In_ColorValue0[1] <= 15 && In_ColorValue0[2] <= 15)
		{
			a = 0;
		}
		if (In_ColorValue1[0] <= 15 && In_ColorValue1[1] <= 15 && In_ColorValue1[2] <= 15)
		{
			a2 = 0;
		}
		if (In_ColorValue2[0] <= 15 && In_ColorValue2[1] <= 15 && In_ColorValue2[2] <= 15)
		{
			a3 = 0;
		}
		if (In_ColorValue3[0] <= 15 && In_ColorValue3[1] <= 15 && In_ColorValue3[2] <= 15)
		{
			a4 = 0;
		}
		if (In_ColorValue4[0] <= 15 && In_ColorValue4[1] <= 15 && In_ColorValue4[2] <= 15)
		{
			a5 = 0;
		}
		Running_default.Visibility = Visibility.Hidden;
		Running_one.Visibility = Visibility.Hidden;
		Running_five.Visibility = Visibility.Hidden;
		Breath_default.Visibility = Visibility.Hidden;
		Breath_one.Visibility = Visibility.Hidden;
		Breath_five.Visibility = Visibility.Hidden;
		Rainbow_default.Visibility = Visibility.Hidden;
		Meteor_default.Visibility = Visibility.Hidden;
		Meteor_one.Visibility = Visibility.Hidden;
		Blink_default.Visibility = Visibility.Hidden;
		Blink_one.Visibility = Visibility.Hidden;
		Blink_five.Visibility = Visibility.Hidden;
		Static_default.Visibility = Visibility.Hidden;
		Static_one.Visibility = Visibility.Hidden;
		Static_five.Visibility = Visibility.Hidden;
		DoubleBlink_default.Visibility = Visibility.Hidden;
		DoubleBlink_one.Visibility = Visibility.Hidden;
		DoubleBlink_five.Visibility = Visibility.Hidden;
		ColorCycle_default.Visibility = Visibility.Hidden;
		Thread.Sleep(SleepTime);
		switch (sMode)
		{
		case "running":
			switch (sOption)
			{
			case "DefaultColor":
				Running_default.Visibility = Visibility.Visible;
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			case "OneColor":
				Running_one.Visibility = Visibility.Visible;
				Running_one.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			case "FiveColor":
				Running_five.Visibility = Visibility.Visible;
				Running_five_1_1.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Running_five_1_2.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Running_five_1_3.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Running_five_1_4.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Running_five_1_5.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Running_five_2_1.Fill = new SolidColorBrush(Color.FromArgb(a2, In_ColorValue1[0], In_ColorValue1[1], In_ColorValue1[2]));
				Running_five_2_2.Fill = new SolidColorBrush(Color.FromArgb(a2, In_ColorValue1[0], In_ColorValue1[1], In_ColorValue1[2]));
				Running_five_2_3.Fill = new SolidColorBrush(Color.FromArgb(a2, In_ColorValue1[0], In_ColorValue1[1], In_ColorValue1[2]));
				Running_five_2_4.Fill = new SolidColorBrush(Color.FromArgb(a2, In_ColorValue1[0], In_ColorValue1[1], In_ColorValue1[2]));
				Running_five_2_5.Fill = new SolidColorBrush(Color.FromArgb(a2, In_ColorValue1[0], In_ColorValue1[1], In_ColorValue1[2]));
				Running_five_3_1.Fill = new SolidColorBrush(Color.FromArgb(a3, In_ColorValue2[0], In_ColorValue2[1], In_ColorValue2[2]));
				Running_five_3_2.Fill = new SolidColorBrush(Color.FromArgb(a3, In_ColorValue2[0], In_ColorValue2[1], In_ColorValue2[2]));
				Running_five_3_3.Fill = new SolidColorBrush(Color.FromArgb(a3, In_ColorValue2[0], In_ColorValue2[1], In_ColorValue2[2]));
				Running_five_3_4.Fill = new SolidColorBrush(Color.FromArgb(a3, In_ColorValue2[0], In_ColorValue2[1], In_ColorValue2[2]));
				Running_five_3_5.Fill = new SolidColorBrush(Color.FromArgb(a3, In_ColorValue2[0], In_ColorValue2[1], In_ColorValue2[2]));
				Running_five_4_1.Fill = new SolidColorBrush(Color.FromArgb(a4, In_ColorValue3[0], In_ColorValue3[1], In_ColorValue3[2]));
				Running_five_4_2.Fill = new SolidColorBrush(Color.FromArgb(a4, In_ColorValue3[0], In_ColorValue3[1], In_ColorValue3[2]));
				Running_five_4_3.Fill = new SolidColorBrush(Color.FromArgb(a4, In_ColorValue3[0], In_ColorValue3[1], In_ColorValue3[2]));
				Running_five_4_4.Fill = new SolidColorBrush(Color.FromArgb(a4, In_ColorValue3[0], In_ColorValue3[1], In_ColorValue3[2]));
				Running_five_4_5.Fill = new SolidColorBrush(Color.FromArgb(a4, In_ColorValue3[0], In_ColorValue3[1], In_ColorValue3[2]));
				Running_five_5_1.Fill = new SolidColorBrush(Color.FromArgb(a5, In_ColorValue4[0], In_ColorValue4[1], In_ColorValue4[2]));
				Running_five_5_2.Fill = new SolidColorBrush(Color.FromArgb(a5, In_ColorValue4[0], In_ColorValue4[1], In_ColorValue4[2]));
				Running_five_5_3.Fill = new SolidColorBrush(Color.FromArgb(a5, In_ColorValue4[0], In_ColorValue4[1], In_ColorValue4[2]));
				Running_five_5_4.Fill = new SolidColorBrush(Color.FromArgb(a5, In_ColorValue4[0], In_ColorValue4[1], In_ColorValue4[2]));
				Running_five_5_5.Fill = new SolidColorBrush(Color.FromArgb(a5, In_ColorValue4[0], In_ColorValue4[1], In_ColorValue4[2]));
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			}
			break;
		case "blink":
			switch (sOption)
			{
			case "DefaultColor":
				Blink_default.Visibility = Visibility.Visible;
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			case "OneColor":
				Blink_one.Visibility = Visibility.Visible;
				Blink_one.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			case "FiveColor":
				Blink_five.Visibility = Visibility.Visible;
				Blink_five_5.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Blink_five_4.Fill = new SolidColorBrush(Color.FromArgb(a2, In_ColorValue1[0], In_ColorValue1[1], In_ColorValue1[2]));
				Blink_five_3.Fill = new SolidColorBrush(Color.FromArgb(a3, In_ColorValue2[0], In_ColorValue2[1], In_ColorValue2[2]));
				Blink_five_2.Fill = new SolidColorBrush(Color.FromArgb(a4, In_ColorValue3[0], In_ColorValue3[1], In_ColorValue3[2]));
				Blink_five_1.Fill = new SolidColorBrush(Color.FromArgb(a5, In_ColorValue4[0], In_ColorValue4[1], In_ColorValue4[2]));
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			}
			break;
		case "breath":
			switch (sOption)
			{
			case "DefaultColor":
				Breath_default.Visibility = Visibility.Visible;
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			case "OneColor":
				Breath_one.Visibility = Visibility.Visible;
				Breath_one.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			case "FiveColor":
				Breath_five.Visibility = Visibility.Visible;
				Breath_five_1.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Breath_five_2.Fill = new SolidColorBrush(Color.FromArgb(a2, In_ColorValue1[0], In_ColorValue1[1], In_ColorValue1[2]));
				Breath_five_3.Fill = new SolidColorBrush(Color.FromArgb(a3, In_ColorValue2[0], In_ColorValue2[1], In_ColorValue2[2]));
				Breath_five_4.Fill = new SolidColorBrush(Color.FromArgb(a4, In_ColorValue3[0], In_ColorValue3[1], In_ColorValue3[2]));
				Breath_five_5.Fill = new SolidColorBrush(Color.FromArgb(a5, In_ColorValue4[0], In_ColorValue4[1], In_ColorValue4[2]));
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			}
			break;
		case "static_color":
			switch (sOption)
			{
			case "DefaultColor":
				Static_default.Visibility = Visibility.Visible;
				break;
			case "OneColor":
				Static_one.Visibility = Visibility.Visible;
				Static_one.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				break;
			case "FiveColor":
				Static_five.Visibility = Visibility.Visible;
				Static_five_5.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Static_five_4.Fill = new SolidColorBrush(Color.FromArgb(a2, In_ColorValue1[0], In_ColorValue1[1], In_ColorValue1[2]));
				Static_five_3.Fill = new SolidColorBrush(Color.FromArgb(a3, In_ColorValue2[0], In_ColorValue2[1], In_ColorValue2[2]));
				Static_five_2.Fill = new SolidColorBrush(Color.FromArgb(a4, In_ColorValue3[0], In_ColorValue3[1], In_ColorValue3[2]));
				Static_five_1.Fill = new SolidColorBrush(Color.FromArgb(a5, In_ColorValue4[0], In_ColorValue4[1], In_ColorValue4[2]));
				break;
			}
			break;
		case "rainbow":
			Rainbow_default.Visibility = Visibility.Visible;
			Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
			if (Array_Storyboard[0] != null)
			{
				Array_Storyboard[0].Begin();
			}
			break;
		case "double_blink":
			switch (sOption)
			{
			case "DefaultColor":
				DoubleBlink_default.Visibility = Visibility.Visible;
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			case "OneColor":
				DoubleBlink_one.Visibility = Visibility.Visible;
				DoubleBlink_one.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			case "FiveColor":
				DoubleBlink_five.Visibility = Visibility.Visible;
				DoubleBlink_five_5.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
				DoubleBlink_five_4.Fill = new SolidColorBrush(Color.FromArgb(a2, In_ColorValue1[0], In_ColorValue1[1], In_ColorValue1[2]));
				DoubleBlink_five_3.Fill = new SolidColorBrush(Color.FromArgb(a3, In_ColorValue2[0], In_ColorValue2[1], In_ColorValue2[2]));
				DoubleBlink_five_2.Fill = new SolidColorBrush(Color.FromArgb(a4, In_ColorValue3[0], In_ColorValue3[1], In_ColorValue3[2]));
				DoubleBlink_five_1.Fill = new SolidColorBrush(Color.FromArgb(a5, In_ColorValue4[0], In_ColorValue4[1], In_ColorValue4[2]));
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
				break;
			}
			break;
		case "meteor":
			if (!(sOption == "DefaultColor"))
			{
				if (sOption == "OneColor")
				{
					Meteor_one.Visibility = Visibility.Visible;
					Meteor_one.Fill = new SolidColorBrush(Color.FromArgb(a, In_ColorValue0[0], In_ColorValue0[1], In_ColorValue0[2]));
					Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
					if (Array_Storyboard[0] != null)
					{
						Array_Storyboard[0].Begin();
					}
				}
			}
			else
			{
				Meteor_default.Visibility = Visibility.Visible;
				Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
				if (Array_Storyboard[0] != null)
				{
					Array_Storyboard[0].Begin();
				}
			}
			break;
		case "color_cycle":
			ColorCycle_default.Visibility = Visibility.Visible;
			Array_Storyboard[0] = TryFindResource(sMode + "_" + sOption) as Storyboard;
			if (Array_Storyboard[0] != null)
			{
				Array_Storyboard[0].Begin();
			}
			break;
		case "all_off":
			Static_one.Visibility = Visibility.Visible;
			Static_one.Fill = new SolidColorBrush(Color.FromArgb(a, 125, 125, 125));
			break;
		}
	}

	[DebuggerNonUserCode]
	[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
	public void InitializeComponent()
	{
		if (!_contentLoaded)
		{
			_contentLoaded = true;
			Uri resourceLocator = new Uri("/FURYCTRL;component/animation/uc_pannel_dram_renegade.xaml", UriKind.Relative);
			Application.LoadComponent(this, resourceLocator);
		}
	}

	[DebuggerNonUserCode]
	[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
	[EditorBrowsable(EditorBrowsableState.Never)]
	void IComponentConnector.Connect(int connectionId, object target)
	{
		switch (connectionId)
		{
		case 1:
			LayoutRoot = (Grid)target;
			break;
		case 2:
			bg = (Path)target;
			break;
		case 3:
			Running_default = (Canvas)target;
			break;
		case 4:
			Running_default_1 = (Path)target;
			break;
		case 5:
			Running_default_2 = (Path)target;
			break;
		case 6:
			Running_default_3 = (Path)target;
			break;
		case 7:
			Running_default_4 = (Path)target;
			break;
		case 8:
			Running_default_5 = (Path)target;
			break;
		case 9:
			Running_one = (Path)target;
			break;
		case 10:
			Running_five = (Canvas)target;
			break;
		case 11:
			Running_five_1_5 = (Path)target;
			break;
		case 12:
			Running_five_1_4 = (Path)target;
			break;
		case 13:
			Running_five_1_3 = (Path)target;
			break;
		case 14:
			Running_five_1_2 = (Path)target;
			break;
		case 15:
			Running_five_1_1 = (Path)target;
			break;
		case 16:
			Running_five_2_5 = (Path)target;
			break;
		case 17:
			Running_five_2_4 = (Path)target;
			break;
		case 18:
			Running_five_2_3 = (Path)target;
			break;
		case 19:
			Running_five_2_2 = (Path)target;
			break;
		case 20:
			Running_five_2_1 = (Path)target;
			break;
		case 21:
			Running_five_3_5 = (Path)target;
			break;
		case 22:
			Running_five_3_4 = (Path)target;
			break;
		case 23:
			Running_five_3_3 = (Path)target;
			break;
		case 24:
			Running_five_3_2 = (Path)target;
			break;
		case 25:
			Running_five_3_1 = (Path)target;
			break;
		case 26:
			Running_five_4_5 = (Path)target;
			break;
		case 27:
			Running_five_4_4 = (Path)target;
			break;
		case 28:
			Running_five_4_3 = (Path)target;
			break;
		case 29:
			Running_five_4_2 = (Path)target;
			break;
		case 30:
			Running_five_4_1 = (Path)target;
			break;
		case 31:
			Running_five_5_5 = (Path)target;
			break;
		case 32:
			Running_five_5_4 = (Path)target;
			break;
		case 33:
			Running_five_5_3 = (Path)target;
			break;
		case 34:
			Running_five_5_2 = (Path)target;
			break;
		case 35:
			Running_five_5_1 = (Path)target;
			break;
		case 36:
			Breath_default = (Path)target;
			break;
		case 37:
			Breath_one = (Path)target;
			break;
		case 38:
			Breath_five = (Canvas)target;
			break;
		case 39:
			Breath_five_5 = (Path)target;
			break;
		case 40:
			Breath_five_4 = (Path)target;
			break;
		case 41:
			Breath_five_3 = (Path)target;
			break;
		case 42:
			Breath_five_2 = (Path)target;
			break;
		case 43:
			Breath_five_1 = (Path)target;
			break;
		case 44:
			Rainbow_default = (Path)target;
			break;
		case 45:
			Meteor_default = (Path)target;
			break;
		case 46:
			Meteor_one = (Path)target;
			break;
		case 47:
			Blink_default = (Path)target;
			break;
		case 48:
			Blink_one = (Path)target;
			break;
		case 49:
			Blink_five = (Canvas)target;
			break;
		case 50:
			Blink_five_1 = (Path)target;
			break;
		case 51:
			Blink_five_2 = (Path)target;
			break;
		case 52:
			Blink_five_3 = (Path)target;
			break;
		case 53:
			Blink_five_4 = (Path)target;
			break;
		case 54:
			Blink_five_5 = (Path)target;
			break;
		case 55:
			Static_default = (Path)target;
			break;
		case 56:
			Static_one = (Path)target;
			break;
		case 57:
			Static_five = (Canvas)target;
			break;
		case 58:
			Static_five_1 = (Path)target;
			break;
		case 59:
			Static_five_2 = (Path)target;
			break;
		case 60:
			Static_five_3 = (Path)target;
			break;
		case 61:
			Static_five_4 = (Path)target;
			break;
		case 62:
			Static_five_5 = (Path)target;
			break;
		case 63:
			DoubleBlink_default = (Path)target;
			break;
		case 64:
			DoubleBlink_one = (Path)target;
			break;
		case 65:
			DoubleBlink_five = (Canvas)target;
			break;
		case 66:
			DoubleBlink_five_1 = (Path)target;
			break;
		case 67:
			DoubleBlink_five_2 = (Path)target;
			break;
		case 68:
			DoubleBlink_five_3 = (Path)target;
			break;
		case 69:
			DoubleBlink_five_4 = (Path)target;
			break;
		case 70:
			DoubleBlink_five_5 = (Path)target;
			break;
		case 71:
			ColorCycle_default = (Path)target;
			break;
		default:
			_contentLoaded = true;
			break;
		}
	}
}
