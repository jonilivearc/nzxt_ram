using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Windows;

[assembly: AssemblyTitle("Kingston FURY CTRL")]
[assembly: AssemblyDescription("Kingston FURY CTRL 1.0.12.0 - Aug 2021 Release")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Kingston Technology Company, Inc.")]
[assembly: AssemblyProduct("Kingston FURY CTRL")]
[assembly: AssemblyCopyright("Copyright ⓒ 2021 Kingston Technology Company, Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]
[assembly: AssemblyFileVersion("1.0.0.20")]
[assembly: AssemblyVersion("1.0.0.20")]
