using System.Collections.Generic;

namespace FuryCTRL;

public class DRAMCmdObj
{
	public string api { get; set; }

	public string status { get; set; }

	public string version { get; set; }

	public string keep { get; set; }

	public string is8dimm { get; set; }

	public DRAMKernelInfo kernel_info { get; set; }

	public Dictionary<string, DRAMInfoObj> dram { get; set; }

	public DRAMCtrlObj ctrl_settings { get; set; }

	public DRAMCmdObj()
	{
	}

	public DRAMCmdObj(string _api)
	{
		api = _api;
	}

	public DRAMCmdObj(string _api, DRAMCtrlObj _ctrl_settings)
	{
		api = _api;
		ctrl_settings = new DRAMCtrlObj(_ctrl_settings.mode, _ctrl_settings.ctrl_mode);
	}
}
