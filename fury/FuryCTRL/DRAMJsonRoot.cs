namespace FuryCTRL;

public class DRAMJsonRoot
{
	public DRAMCmdObj root { get; set; }

	public DRAMJsonRoot()
	{
	}

	public DRAMJsonRoot(string api)
	{
		root = new DRAMCmdObj(api);
	}

	public DRAMJsonRoot(string _api, string _status)
	{
		root.api = _api;
		root.status = _status;
	}

	public DRAMJsonRoot(string _api, string _status, DRAMKernelInfo _kernel_info)
	{
		root.api = _api;
		root.status = _status;
		root.kernel_info = _kernel_info;
	}

	public DRAMJsonRoot(string _api, string _status, DRAMCmdObj _root)
	{
		root = _root;
		root.api = _api;
		root.status = _status;
	}

	public DRAMJsonRoot(string api, DRAMCtrlObj _ctrl_settings)
	{
		root = new DRAMCmdObj(api, _ctrl_settings);
	}
}
