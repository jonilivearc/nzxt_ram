using System.Windows;
using FuryCTRL.Properties;

namespace FuryCTRL;

public static class C_ScreenView
{
	internal static double ScreenDPI = 1.0;

	internal static double DisplayMulti = 0.798;

	public static double ActualWidth = 0.0;

	public static double ActualHeight = 0.0;

	public static void InitScreenDPI(Window In_Window)
	{
		ScreenDPI = PresentationSource.FromVisual(In_Window).CompositionTarget.TransformToDevice.M11;
	}

	public static Size TransferWindowSize(Window In_Window, double Width, double Height)
	{
		DisplayMulti = 1.0;
		double windowWidth = Settings.Default.WindowWidth;
		double windowHeight = Settings.Default.WindowHeight;
		double num = SystemParameters.WorkArea.Height / 2160.0 * Height * DisplayMulti;
		double num2 = num * (Width / Height);
		if (num2 > SystemParameters.WorkArea.Width)
		{
			double num3 = SystemParameters.WorkArea.Width / num2;
			num2 *= num3;
			num = num2 / (Width / Height);
		}
		if (In_Window != null)
		{
			if (windowHeight != 0.0 && windowWidth != 0.0)
			{
				In_Window.Width = windowWidth;
				In_Window.Height = windowHeight;
			}
			else
			{
				In_Window.Width = num2;
				In_Window.Height = num;
				Settings.Default.WindowWidth = num2;
				Settings.Default.WindowHeight = num;
				Settings.Default.Save();
			}
			ActualHeight = num;
			ActualWidth = num2;
		}
		return new Size(num2, num);
	}

	public static void TransferWindowPostion(Window In_Window, double In_Left = 0.0, double In_Top = 0.0)
	{
		if (In_Window == null)
		{
			return;
		}
		In_Window.WindowStartupLocation = WindowStartupLocation.Manual;
		if (In_Left == 0.0 && In_Top == 0.0)
		{
			if (!double.IsNaN(In_Window.ActualWidth) && !double.IsNaN(In_Window.ActualHeight))
			{
				In_Window.Left = (SystemParameters.WorkArea.Width - ActualWidth) / 2.0;
				In_Window.Top = (SystemParameters.WorkArea.Height - ActualHeight) / 2.0;
			}
		}
		else
		{
			In_Window.Left = In_Left;
			In_Window.Top = In_Top;
		}
	}
}
