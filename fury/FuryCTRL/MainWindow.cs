using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
using FuryCTRL.Animation;
using FuryCTRL.Properties;
using Microsoft.Win32;
using WebSocketSharp;
using Windows.ApplicationModel;

namespace FuryCTRL;

public class MainWindow : Window, IComponentConnector
{
	public enum Enum_Optration
	{
		get_DRAM_info,
		get_DRAM_LED_style,
		set_DRAM_LED_style
	}

	private delegate void UpdateDeviceInfoUIDispatcherDelegate();

	private delegate void UpdateUIDispatcherDelegate();

	private delegate void DisplayMsg();

	private System.Windows.Size Size_MainWindow;

	private ContextMenuStrip contextMenuStrip;

	private bool windowfirstload = true;

	public int iSystemDefaultLangID = CultureInfo.CurrentCulture.LCID;

	public ResourceDictionary resStyleDictionary = new ResourceDictionary();

	public ResourceDictionary resLangDictionary = new ResourceDictionary();

	private bool bMaxWindow;

	private static Version UWPVersion = new Version("1.0.13.0");

	private IEnumerable<LanguageItem> _languageList = LanguageList.GetLanguages();

	private bool bFirstLoad = true;

	private static Version SDKVersionFromKernel = null;

	private static Version SDKVersionInSystem = null;

	private static Version TargetSDKVersion = new Version("1.0.0.12");

	private const string AppSubkeyPath = "Software\\Kingston Fury\\FuryCTRL";

	private const string AppSubkeyValueName = "AppVersion";

	private readonly string sFirstSetupFileName = "FURY_CTRL.exe";

	private readonly string sFirstSetupArgs = "/verysilent /norestart";

	private readonly string sSetupFileName = "FuryCTRL_SDK.exe";

	private readonly string sSetupArgs = "/verysilent /norestart";

	public Enum_Optration enCurrentOptration;

	private bool bPalette = true;

	public bool bStartupEnable;

	private bool bIs8Dimm;

	private bool bRandom;

	private Random rand;

	private int iRandom_R;

	private int iRandom_G;

	private int iRandom_B;

	private int deviceCount;

	private int createdCount;

	private string sProductName = "";

	private int slotIndex = -1;

	private string sMode = "rainbow";

	private string sCtrl_mode = "independence";

	private string sSetDRAMLEDStyle = "";

	private bool flag_FirstSetting;

	private List<List<int>> listColor = new List<List<int>>();

	private string sOption = "DefaultColor";

	public int speedValue = 98;

	public int brightnessValue = 100;

	public int darknessValue;

	private bool flag_First_Load_Profile = true;

	private int CurrentProfileIndex;

	private int total_profiles;

	private int createdCount_profiles;

	private int index_AddRow = 2;

	private string tempFileName = "";

	private int rowIndex;

	private string oldFileName = "";

	private UC_Pannel_DRAM_Fury Panel_DRAM_Fury0;

	private UC_Pannel_DRAM_Fury Panel_DRAM_Fury1;

	private UC_Pannel_DRAM_Fury Panel_DRAM_Fury2;

	private UC_Pannel_DRAM_Fury Panel_DRAM_Fury3;

	private UC_Pannel_DRAM_Beast Pannel_DRAM_Beast0;

	private UC_Pannel_DRAM_Beast Pannel_DRAM_Beast1;

	private UC_Pannel_DRAM_Beast Pannel_DRAM_Beast2;

	private UC_Pannel_DRAM_Beast Pannel_DRAM_Beast3;

	private UC_Pannel_DRAM_Predator Pannel_DRAM_Predator0;

	private UC_Pannel_DRAM_Predator Pannel_DRAM_Predator1;

	private UC_Pannel_DRAM_Predator Pannel_DRAM_Predator2;

	private UC_Pannel_DRAM_Predator Pannel_DRAM_Predator3;

	private UC_Pannel_DRAM_Renegade Pannel_DRAM_Renegade0;

	private UC_Pannel_DRAM_Renegade Pannel_DRAM_Renegade1;

	private UC_Pannel_DRAM_Renegade Pannel_DRAM_Renegade2;

	private UC_Pannel_DRAM_Renegade Pannel_DRAM_Renegade3;

	private DRAMJsonRoot _DRAMInfoRootObj;

	private DRAMJsonRoot _DRAMGetDRAMLED;

	private Dictionary<string, DRAMCtrlColorObj> _DRAM_colors;

	private WebSocket ws;

	private string sPortNumber = "55599";

	private string sOrigin = "ksws-dramledctrl://5E7EFB96-6632-40D5-882F-51CE1E62CA3F";

	private int iConnectRetryLimitationCount = 10;

	private int iConnectRetryCount;

	private string pw = "3m23s45i599";

	private Random R = new Random(DateTime.Now.Millisecond);

	private double LastWidth;

	private double LastHeight;

	private float AspectRatio = 1.85f;

	private Random r = new Random(DateTime.Now.Millisecond);

	private int total_colors = 10;

	private int index_Column = 5;

	private int index_Row = 5;

	private int target_index_customize_color = -1;

	private List<System.Windows.Media.Color> Circle_Color_list = new List<System.Windows.Media.Color>();

	internal Grid dc_main;

	internal Thumb ThumbTop;

	internal System.Windows.Controls.Image Img_Logo;

	internal System.Windows.Controls.Button Button_Profile;

	internal System.Windows.Controls.Button Button_Infomation;

	internal Grid gr_SwitchLED;

	internal TextBlock TextBlock_LED;

	internal System.Windows.Controls.CheckBox Button_LEDlightOnOff;

	internal System.Windows.Controls.Button Button_Min;

	internal System.Windows.Controls.Button Button_Exit;

	internal Grid gr_DevicePanel;

	internal Grid gr_DevicePanel_Childs;

	internal Grid gr_DevicePanel_8DIMM_1;

	internal Grid gr_DevicePanel_8DIMM_1_Childs;

	internal Grid gr_DevicePanel_8DIMM_2;

	internal Grid gr_DevicePanel_8DIMM_2_Childs;

	internal TextBlock TextBlock_Effec;

	internal System.Windows.Controls.RadioButton Button_Running;

	internal System.Windows.Controls.RadioButton Button_Blink;

	internal System.Windows.Controls.RadioButton Button_Breath;

	internal System.Windows.Controls.RadioButton Button_Static;

	internal System.Windows.Controls.RadioButton Button_Rainbow;

	internal System.Windows.Controls.RadioButton Button_DoubleBlink;

	internal System.Windows.Controls.RadioButton Button_Meteor;

	internal System.Windows.Controls.RadioButton Button_ColorCycle;

	internal System.Windows.Controls.RadioButton Button_DefaultColor;

	internal System.Windows.Controls.RadioButton Button_OneColor;

	internal System.Windows.Controls.RadioButton Button_FiveColor;

	internal UC_ColorPalette3 ColorPalette;

	internal Grid canOneColor;

	internal System.Windows.Controls.Button bt_Add_Circle_Color;

	internal System.Windows.Controls.Button bt_Delete_Circle_Color;

	internal Grid canMultiColors;

	internal Grid gr_MultiColors_Childs;

	internal System.Windows.Controls.Button Button_RandomColor;

	internal TextBlock TextBlock_Speed;

	internal Slider Slider_Speed;

	internal TextBlock TextBlock_Low;

	internal TextBlock TextBlock_Fast;

	internal TextBlock TextBlock_Brightness;

	internal Slider Slider_Brightness;

	internal TextBlock TextBlock_Dark;

	internal TextBlock TextBlock_Bright;

	internal TextBlock TextBlock_Darknesss;

	internal Slider Slider_Darkness;

	internal TextBlock TextBlock_Darkness_Dark;

	internal TextBlock TextBlock_Darkness_Bright;

	internal System.Windows.Controls.Button Button_Rest;

	internal System.Windows.Controls.Button Button_Apply;

	internal Grid gr_Profile_mask;

	internal Grid gr_Profile;

	internal System.Windows.Controls.Button Button_Close_Profile;

	internal Grid gr_Profile_list;

	internal TextBlock TextBox_Profile;

	internal Grid gr_profile_position1;

	internal Grid gr_profile_position2;

	internal Grid gr_profile_position3;

	internal Grid gr_profile_position4;

	internal Grid gr_profile_position5;

	internal Grid gr_AddProfile;

	internal System.Windows.Controls.Button Button_AddProfile;

	internal System.Windows.Controls.TextBox TextBox_Modify_Profile_FileName;

	internal Grid gr_Infomation_mask;

	internal Grid gr_Infomation;

	internal System.Windows.Controls.Button Button_Close_Information;

	internal TextBlock TextBox_Language;

	internal System.Windows.Controls.ComboBox cb_Language;

	internal TextBlock TextBox_Software;

	internal TextBlock TextBox_App_description;

	internal TextBlock TextBox_App_Copyright;

	internal TextBlock TextBox_URL_Privacy;

	internal TextBlock TextBox_Startup;

	internal System.Windows.Controls.CheckBox cb_keep_status;

	internal TextBlock TextBox_Startup_Content;

	internal Thumb ThumbTop_ForMask;

	internal Grid gr_wait_mask;

	internal TextBlock tbx_waiting_message;

	internal Grid gr_eula_mask;

	internal TextBlock tb_Privacy_Page_Title;

	internal ScrollViewer sv_Privacy;

	internal TextBlock tb_Privacy_Title;

	internal TextBlock tb_Privacy_Version;

	internal TextBlock tb_Privacy_Acction1;

	internal TextBlock tb_Privacy_Acction2;

	internal TextBlock tb_Privacy_Acction3_1;

	internal TextBlock tb_Privacy_Acction3_2;

	internal TextBlock tb_Privacy_Acction3_3;

	internal TextBlock tb_Privacy_Acction3_4;

	internal TextBlock tb_Privacy_Acction3_5;

	internal TextBlock tb_Privacy_Acction3_6;

	internal TextBlock tb_Privacy_Acction3_7;

	internal TextBlock tb_Privacy_Acction3_8;

	internal TextBlock tb_Privacy_Acction3_9;

	internal TextBlock tb_Privacy_Content_Title1_1;

	internal TextBlock tb_Privacy_Content_Title1_2;

	internal TextBlock tb_Privacy_Content1;

	internal TextBlock tb_Privacy_Content_Title2_1;

	internal TextBlock tb_Privacy_Content_Title2_2;

	internal TextBlock tb_Privacy_Content2;

	internal TextBlock tb_Privacy_Content_Title3_1;

	internal TextBlock tb_Privacy_Content_Title3_2;

	internal TextBlock tb_Privacy_Content3;

	internal TextBlock tb_Privacy_Content_Title4_1;

	internal TextBlock tb_Privacy_Content_Title4_2;

	internal TextBlock tb_Privacy_Content4;

	internal TextBlock tb_Privacy_Content_Title5_1;

	internal TextBlock tb_Privacy_Content_Title5_2;

	internal TextBlock tb_Privacy_Content5_1;

	internal TextBlock tb_Privacy_Content5_2;

	internal TextBlock tb_Privacy_Content5_3;

	internal TextBlock tb_Privacy_Content5_4;

	internal TextBlock tb_Privacy_Content5_5;

	internal TextBlock tb_Privacy_Content5_6;

	internal TextBlock tb_Privacy_Content5_7;

	internal TextBlock tb_Privacy_Content5_8;

	internal TextBlock tb_Privacy_Content5_9;

	internal TextBlock tb_Privacy_Content_Title6_1;

	internal TextBlock tb_Privacy_Content_Title6_2;

	internal TextBlock tb_Privacy_Content6;

	internal TextBlock tb_Privacy_Content_Title7_1;

	internal TextBlock tb_Privacy_Content_Title7_2;

	internal TextBlock tb_Privacy_Content7;

	internal TextBlock tb_Privacy_Content_Title8_1;

	internal TextBlock tb_Privacy_Content_Title8_2;

	internal TextBlock tb_Privacy_Content8;

	internal TextBlock tb_Privacy_Content_Title9_1;

	internal TextBlock tb_Privacy_Content_Title9_2;

	internal TextBlock tb_Privacy_Content9;

	internal TextBlock tb_Privacy_Content_Title10_1;

	internal TextBlock tb_Privacy_Content_Title10_2;

	internal TextBlock tb_Privacy_Content10;

	internal TextBlock tb_Privacy_Content_Title11_1;

	internal TextBlock tb_Privacy_Content_Title11_2;

	internal TextBlock tb_Privacy_Content11;

	internal TextBlock tb_Privacy_Content_Title12_1;

	internal TextBlock tb_Privacy_Content_Title12_2;

	internal TextBlock tb_Privacy_Content12_1;

	internal TextBlock tb_Privacy_Content12_2;

	internal TextBlock tb_Privacy_Content12_3;

	internal TextBlock tb_Privacy_Content12_4;

	internal TextBlock tb_Privacy_Content12_5;

	internal TextBlock tb_Privacy_Content_Title13_1;

	internal TextBlock tb_Privacy_Content_Title13_2;

	internal TextBlock tb_Privacy_Content13;

	internal TextBlock tb_Privacy_Content_Title14_1;

	internal TextBlock tb_Privacy_Content_Title14_2;

	internal TextBlock tb_Privacy_Content14;

	internal TextBlock tb_Privacy_Content_Title15_1;

	internal TextBlock tb_Privacy_Content_Title15_2;

	internal TextBlock tb_Privacy_Content15;

	internal TextBlock tb_Privacy_Content_Title16_1;

	internal TextBlock tb_Privacy_Content_Title16_2;

	internal TextBlock tb_Privacy_Content16;

	internal TextBlock tb_Privacy_Content_Title17_1;

	internal TextBlock tb_Privacy_Content_Title17_2;

	internal TextBlock tb_Privacy_Content17_1;

	internal TextBlock tb_Privacy_Content17_2;

	internal TextBlock tb_Privacy_Content17_3;

	internal TextBlock tb_Privacy_Content17_4;

	internal TextBlock tb_Privacy_Content17_5;

	internal TextBlock tb_Privacy_Content_Title18_1;

	internal TextBlock tb_Privacy_Content_Title18_2;

	internal TextBlock tb_Privacy_Content18_1;

	internal TextBlock tb_Privacy_Content18_2;

	internal TextBlock tb_Privacy_Content18_3;

	internal TextBlock tb_Privacy_Content18_4;

	internal TextBlock tb_Privacy_Content18_5;

	internal TextBlock tb_Privacy_Content18_6;

	internal TextBlock tb_Privacy_Content18_7;

	internal TextBlock tb_Privacy_Content18_8;

	internal TextBlock tb_Privacy_Content_Title19_1;

	internal TextBlock tb_Privacy_Content_Title19_2;

	internal TextBlock tb_Privacy_Content19;

	internal TextBlock tb_Privacy_Content_Title20_1;

	internal TextBlock tb_Privacy_Content_Title20_2;

	internal TextBlock tb_Privacy_Content20;

	internal TextBlock tb_Privacy_Content_Title21_1;

	internal TextBlock tb_Privacy_Content_Title21_2;

	internal TextBlock tb_Privacy_Content21;

	internal TextBlock tb_Privacy_Content_Title22_1;

	internal TextBlock tb_Privacy_Content_Title22_2;

	internal TextBlock tb_Privacy_Content22;

	internal TextBlock tb_Privacy_Content_Title23_1;

	internal TextBlock tb_Privacy_Content_Title23_2;

	internal TextBlock tb_Privacy_Content23;

	internal TextBlock tb_Privacy_Content_Title24_1;

	internal TextBlock tb_Privacy_Content_Title24_2;

	internal TextBlock tb_Privacy_Content24;

	internal System.Windows.Controls.CheckBox chk_ReadPrivacy;

	internal System.Windows.Controls.Button bt_apply;

	internal System.Windows.Controls.Button bt_cancel;

	internal Grid gr_setup_progress_mask;

	internal TextBlock tb_setup_progress;

	internal Grid gr_update_setup_progress_mask;

	internal TextBlock tb_update_setup_progress;

	internal Grid gr_wait_sdk_come_back_mask;

	internal TextBlock tb_wait_sdk_come_back;

	internal Grid gr_reinstall_app_mask;

	private bool _contentLoaded;

	[DllImport("kernel32.dll")]
	private static extern bool SetProcessWorkingSetSize(IntPtr proc, int min, int max);

	public MainWindow()
	{
		InitializeComponent();
		try
		{
			Log.RecordLogWriter("LCID: " + iSystemDefaultLangID, bFailed: false);
			if (Check_File_Exists("Custom"))
			{
				INI iNI = new INI("Custom.cfg");
				if (iNI.IniReadValue("Language", "lan") != "" && iNI.IniReadValue("Language", "lan") != null)
				{
					iSystemDefaultLangID = Convert.ToInt32(iNI.IniReadValue("Language", "lan"));
					resLangDictionary.Source = new Uri($"pack://application:,,,/Resources/Langs/{iSystemDefaultLangID}.xaml", UriKind.RelativeOrAbsolute);
					Log.RecordLogWriter("LangID: " + iSystemDefaultLangID, bFailed: false);
				}
			}
			else
			{
				iSystemDefaultLangID = CultureInfo.CurrentCulture.LCID;
				resLangDictionary.Source = new Uri($"pack://application:,,,/Resources/Langs/{iSystemDefaultLangID}.xaml", UriKind.RelativeOrAbsolute);
				SaveCustom_Language();
				Log.RecordLogWriter("LangID: " + iSystemDefaultLangID, bFailed: false);
			}
		}
		catch (Exception)
		{
			try
			{
				resLangDictionary.Source = new Uri("pack://application:,,,/Resources/Langs/1033.xaml", UriKind.RelativeOrAbsolute);
				iSystemDefaultLangID = 1033;
				SaveCustom_Language();
				Log.RecordLogWriter("Not support Lang, use default LangID: " + iSystemDefaultLangID, bFailed: false);
			}
			catch (Exception)
			{
			}
		}
		if (resLangDictionary.Source != null)
		{
			base.Resources.MergedDictionaries.Add(resLangDictionary);
		}
		if (bFirstLoad)
		{
			if (cb_Language.Items.Count < 1)
			{
				foreach (LanguageItem language in _languageList)
				{
					ComboBoxItem comboBoxItem = new ComboBoxItem();
					string resourceKey = "T_Lang_" + language.Id;
					comboBoxItem.Content = TryFindResource(resourceKey) as string;
					comboBoxItem.Tag = language.Id.ToString();
					cb_Language.Items.Add(comboBoxItem);
					if (iSystemDefaultLangID == language.Id)
					{
						cb_Language.SelectedItem = comboBoxItem;
					}
				}
				cb_Language.SelectionChanged += cb_Wlan_Interfaces_SelectionChanged;
			}
			else
			{
				ComboBoxItem selectedItem = new ComboBoxItem
				{
					Content = (TryFindResource("T_Lang_" + iSystemDefaultLangID) as string)
				};
				cb_Language.SelectedItem = selectedItem;
			}
			bFirstLoad = false;
		}
		flag_FirstSetting = true;
		gr_DevicePanel_Childs.Children.Clear();
		gr_MultiColors_Childs.Children.Clear();
		if (!CheckKernelInstallOrNot())
		{
			gr_eula_mask.Visibility = Visibility.Visible;
			Log.RecordLogWriter("Need install kernel.", bFailed: false);
		}
		else
		{
			StartConnectToKernelServer();
			Log.RecordLogWriter("Already install kernel, start connect to kernel.", bFailed: false);
		}
		bPalette = false;
		ColorPaletteEnable(bPalette);
		try
		{
			UC_ColorPalette3 colorPalette = ColorPalette;
			colorPalette.SetColorCallback = (Func<object>)Delegate.Combine(colorPalette.SetColorCallback, new Func<object>(ColorChangeCallBack));
		}
		catch
		{
		}
		if (!Check_File_Exists("ProfileIndex"))
		{
			SaveProfile_Device();
		}
	}

	private void Sv_Privacy_ScrollChanged(object sender, ScrollChangedEventArgs e)
	{
		if ((sender as ScrollViewer).VerticalOffset == (sender as ScrollViewer).ScrollableHeight)
		{
			chk_ReadPrivacy.IsEnabled = true;
		}
	}

	private void Chk_ReadPrivacy_Click(object sender, RoutedEventArgs e)
	{
		if ((sender as System.Windows.Controls.CheckBox).IsChecked == true)
		{
			bt_apply.IsEnabled = true;
		}
	}

	private void Button_Close_Profile_Click(object sender, RoutedEventArgs e)
	{
		gr_Profile_mask.Visibility = Visibility.Hidden;
		ThumbTop_ForMask.Visibility = Visibility.Hidden;
	}

	private void Button_Profile_Click(object sender, RoutedEventArgs e)
	{
		gr_Profile_mask.Visibility = Visibility.Visible;
		ThumbTop_ForMask.Visibility = Visibility.Visible;
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateUI_Profile));
	}

	private void Button_AddProfile_Click(object sender, RoutedEventArgs e)
	{
		if (total_profiles != 5)
		{
			Log.RecordLogWriter("[Profile] >> Add new profile", bFailed: false);
			total_profiles++;
			tempFileName = "Profile" + DateTime.Now.ToString("hh-mm-ss-f");
			AddProfileInRow();
			rowIndex = createdCount_profiles;
			SaveProfile_Device();
		}
	}

	private void AddProfileInRow()
	{
		createdCount_profiles++;
		INI iNI = new INI("ProfileIndex.cfg");
		TextBlock textBlock = new TextBlock();
		textBlock.Name = "tb_Profile_displayName_" + createdCount_profiles;
		if (createdCount_profiles == Convert.ToInt32(iNI.IniReadValue("CurrentProfileIndex", "Index")))
		{
			textBlock.Style = (Style)FindResource("style_tb_current_choose_Profile");
		}
		else
		{
			textBlock.Style = (Style)FindResource("style_tb_choose_Profile");
		}
		textBlock.Text = tempFileName;
		System.Windows.Controls.Button button = new System.Windows.Controls.Button();
		button.Name = "bt_Profile_edit_" + createdCount_profiles;
		button.Style = (Style)FindResource("style_bt_choose_Profile_edit");
		button.Click += Button_Profile_Modify_Click;
		System.Windows.Controls.Button button2 = new System.Windows.Controls.Button();
		button2.Name = "bt_Profile_save_" + createdCount_profiles;
		button2.Style = (Style)FindResource("style_bt_choose_Profile_save");
		button2.Click += Button_Profile_Save_Click;
		System.Windows.Controls.Button button3 = new System.Windows.Controls.Button();
		button3.Name = "bt_Profile_delete_" + createdCount_profiles;
		button3.Style = (Style)FindResource("style_bt_choose_Profile_delete");
		button3.Click += Button_Profile_Dele_Click;
		System.Windows.Controls.Button button4 = new System.Windows.Controls.Button();
		button4.Name = "bt_Profile_apply_" + createdCount_profiles;
		button4.Style = (Style)FindResource("style_bt_choose_Profile_apply");
		button4.Click += Button_Profile_Apply_Click;
		if (createdCount_profiles == 1)
		{
			gr_profile_position1.Children.Clear();
			if (createdCount_profiles == CurrentProfileIndex)
			{
				gr_profile_position1.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(80, 138, byte.MaxValue));
			}
			else
			{
				gr_profile_position1.Background = System.Windows.Media.Brushes.Transparent;
			}
			gr_profile_position1.Children.Add(textBlock);
			gr_profile_position1.Children.Add(button);
			gr_profile_position1.Children.Add(button2);
			gr_profile_position1.Children.Add(button3);
			gr_profile_position1.Children.Add(button4);
		}
		if (createdCount_profiles == 2)
		{
			gr_profile_position2.Children.Clear();
			if (createdCount_profiles == CurrentProfileIndex)
			{
				gr_profile_position2.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(80, 138, byte.MaxValue));
			}
			else
			{
				gr_profile_position2.Background = System.Windows.Media.Brushes.Transparent;
			}
			gr_profile_position2.Children.Add(textBlock);
			gr_profile_position2.Children.Add(button);
			gr_profile_position2.Children.Add(button2);
			gr_profile_position2.Children.Add(button3);
			gr_profile_position2.Children.Add(button4);
		}
		if (createdCount_profiles == 3)
		{
			gr_profile_position3.Children.Clear();
			if (createdCount_profiles == CurrentProfileIndex)
			{
				gr_profile_position3.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(80, 138, byte.MaxValue));
			}
			else
			{
				gr_profile_position3.Background = System.Windows.Media.Brushes.Transparent;
			}
			gr_profile_position3.Children.Add(textBlock);
			gr_profile_position3.Children.Add(button);
			gr_profile_position3.Children.Add(button2);
			gr_profile_position3.Children.Add(button3);
			gr_profile_position3.Children.Add(button4);
		}
		if (createdCount_profiles == 4)
		{
			gr_profile_position4.Children.Clear();
			if (createdCount_profiles == CurrentProfileIndex)
			{
				gr_profile_position4.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(80, 138, byte.MaxValue));
			}
			else
			{
				gr_profile_position4.Background = System.Windows.Media.Brushes.Transparent;
			}
			gr_profile_position4.Children.Add(textBlock);
			gr_profile_position4.Children.Add(button);
			gr_profile_position4.Children.Add(button2);
			gr_profile_position4.Children.Add(button3);
			gr_profile_position4.Children.Add(button4);
		}
		if (createdCount_profiles == 5)
		{
			gr_profile_position5.Children.Clear();
			if (createdCount_profiles == CurrentProfileIndex)
			{
				gr_profile_position5.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(80, 138, byte.MaxValue));
			}
			else
			{
				gr_profile_position5.Background = System.Windows.Media.Brushes.Transparent;
			}
			gr_profile_position5.Children.Add(textBlock);
			gr_profile_position5.Children.Add(button);
			gr_profile_position5.Children.Add(button2);
			gr_profile_position5.Children.Add(button3);
			gr_profile_position5.Children.Add(button4);
		}
		if (createdCount_profiles == 5)
		{
			gr_AddProfile.Visibility = Visibility.Hidden;
			index_AddRow = 7;
		}
		else
		{
			gr_AddProfile.Visibility = Visibility.Visible;
			index_AddRow++;
		}
		Grid.SetRow(gr_AddProfile, index_AddRow);
	}

	private void Button_Profile_Apply_Click(object sender, RoutedEventArgs e)
	{
		string[] array = ((System.Windows.Controls.Button)sender).Name.Split('_');
		if (array.Length < 4)
		{
			return;
		}
		int num = (CurrentProfileIndex = Convert.ToInt32(array[3]));
		rowIndex = 1;
		INI iNI = new INI("ProfileIndex.cfg");
		iNI.IniWriteValue("CurrentProfileIndex", "Index", CurrentProfileIndex.ToString());
		if (iNI.IniReadValue("FileName", "Profile_" + CurrentProfileIndex) == null)
		{
			return;
		}
		for (int i = rowIndex; i <= total_profiles; i++)
		{
			tempFileName = iNI.IniReadValue("FileName", "Profile_" + i);
			if (i == CurrentProfileIndex)
			{
				LoadProfile_Device();
			}
			base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateUI_Modify_Filename));
			if (i < total_profiles)
			{
				rowIndex++;
			}
		}
	}

	private void Button_Profile_Save_Click(object sender, RoutedEventArgs e)
	{
		string[] array = ((System.Windows.Controls.Button)sender).Name.Split('_');
		if (array.Length >= 4)
		{
			rowIndex = Convert.ToInt32(array[3]);
			INI iNI = new INI("ProfileIndex.cfg");
			tempFileName = iNI.IniReadValue("FileName", "Profile_" + rowIndex);
			SaveProfile_Device();
		}
	}

	private void Button_Profile_Dele_Click(object sender, RoutedEventArgs e)
	{
		string[] array = ((System.Windows.Controls.Button)sender).Name.Split('_');
		if (array.Length < 4)
		{
			return;
		}
		rowIndex = Convert.ToInt32(array[3]);
		INI iNI = new INI("ProfileIndex.cfg");
		CurrentProfileIndex = Convert.ToInt32(iNI.IniReadValue("CurrentProfileIndex", "Index"));
		oldFileName = iNI.IniReadValue("FileName", "Profile_" + rowIndex);
		if (rowIndex == Convert.ToInt32(iNI.IniReadValue("CurrentProfileIndex", "Index")))
		{
			iNI.IniWriteValue("CurrentProfileIndex", "Index", "0");
			gr_profile_position1.Background = System.Windows.Media.Brushes.Transparent;
			gr_profile_position2.Background = System.Windows.Media.Brushes.Transparent;
			gr_profile_position3.Background = System.Windows.Media.Brushes.Transparent;
			gr_profile_position4.Background = System.Windows.Media.Brushes.Transparent;
			gr_profile_position5.Background = System.Windows.Media.Brushes.Transparent;
			CurrentProfileIndex = Convert.ToInt32(iNI.IniReadValue("CurrentProfileIndex", "Index"));
		}
		else if (rowIndex < CurrentProfileIndex)
		{
			CurrentProfileIndex--;
			iNI.IniWriteValue("CurrentProfileIndex", "Index", CurrentProfileIndex.ToString());
		}
		for (int i = rowIndex; i <= total_profiles; i++)
		{
			tempFileName = iNI.IniReadValue("FileName", "Profile_" + (i + 1));
			iNI.IniWriteValue("FileName", "Profile_" + i, tempFileName);
			base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateUI_Modify_Filename));
			if (i < total_profiles)
			{
				rowIndex++;
				continue;
			}
			if (i == 1)
			{
				gr_profile_position1.Children.Clear();
			}
			if (i == 2)
			{
				gr_profile_position2.Children.Clear();
			}
			if (i == 3)
			{
				gr_profile_position3.Children.Clear();
			}
			if (i == 4)
			{
				gr_profile_position4.Children.Clear();
			}
			if (i == 5)
			{
				gr_profile_position5.Children.Clear();
			}
		}
		total_profiles--;
		createdCount_profiles--;
		index_AddRow--;
		Delete_Profile();
		if (index_AddRow < 7)
		{
			gr_AddProfile.Visibility = Visibility.Visible;
		}
		Grid.SetRow(gr_AddProfile, index_AddRow);
		iNI.IniWriteValue("CurrentProfileIndex", "All", total_profiles.ToString());
	}

	private void Delete_Profile()
	{
		string text = $"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\Kingston Fury\\FuryCTRL\\Profile\\";
		if (Check_File_Exists(oldFileName))
		{
			try
			{
				File.Delete(text + oldFileName + ".cfg");
			}
			catch
			{
			}
		}
	}

	private void Button_Profile_Modify_Click(object sender, RoutedEventArgs e)
	{
		string[] array = ((System.Windows.Controls.Button)sender).Name.Split('_');
		INI iNI = new INI("ProfileIndex.cfg");
		if (array.Length >= 4)
		{
			rowIndex = Convert.ToInt32(array[3]);
			TextBox_Modify_Profile_FileName.Visibility = Visibility.Visible;
			CurrentProfileIndex = Convert.ToInt32(iNI.IniReadValue("CurrentProfileIndex", "Index"));
			if (rowIndex == CurrentProfileIndex)
			{
				TextBox_Modify_Profile_FileName.Style = (Style)FindResource("style_tbx_current_choose_Profile");
			}
			else
			{
				TextBox_Modify_Profile_FileName.Style = (Style)FindResource("style_tbx_choose_Profile");
			}
			TextBox_Modify_Profile_FileName.Text = iNI.IniReadValue("FileName", "Profile_" + array[3]);
			TextBox_Modify_Profile_FileName.Select(0, TextBox_Modify_Profile_FileName.Text.Length);
			TextBox_Modify_Profile_FileName.Focus();
			Grid.SetRow(TextBox_Modify_Profile_FileName, rowIndex + 1);
		}
	}

	private void tb_Modify_Profile_FileName_PreviewTextInput(object sender, TextCompositionEventArgs e)
	{
		if (e.Text != "" && e.Text != "\r" && (e.Text.Length > 1 || char.Parse(e.Text) < '0' || (char.Parse(e.Text) < 'A' && char.Parse(e.Text) > '9')))
		{
			e.Handled = true;
		}
		else if (e.Text == "\r")
		{
			(sender as System.Windows.Controls.TextBox).MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
			TextBox_Modify_Profile_FileName.Visibility = Visibility.Hidden;
			Change_Profile_FileName();
			e.Handled = false;
			base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateUI_Modify_Filename));
		}
	}

	private void tb_Modify_Profile_FileName_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
	{
		if (e.Command == ApplicationCommands.Paste)
		{
			e.Handled = true;
		}
	}

	private void Change_Profile_FileName()
	{
		string text = $"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\Kingston Fury\\FuryCTRL\\Profile\\";
		INI iNI = new INI("ProfileIndex.cfg");
		oldFileName = iNI.IniReadValue("FileName", "Profile_" + rowIndex);
		tempFileName = TextBox_Modify_Profile_FileName.Text;
		if (Check_File_Exists(oldFileName))
		{
			if (Check_File_Exists(tempFileName))
			{
				Log.RecordLogWriter("The file name: " + tempFileName + " has exists.", bFailed: true);
				tempFileName = tempFileName + "_" + DateTime.Now.ToString("mm-ss-f");
				try
				{
					File.Move(text + oldFileName + ".cfg", text + tempFileName + ".cfg");
				}
				catch
				{
					Log.RecordLogWriter("Change file name of profile fail", bFailed: true);
				}
			}
			else
			{
				try
				{
					File.Move(text + oldFileName + ".cfg", text + tempFileName + ".cfg");
				}
				catch
				{
					Log.RecordLogWriter("Change file name of profile fail", bFailed: true);
				}
			}
			iNI.IniWriteValue("FileName", "Profile_" + rowIndex, tempFileName);
		}
		else
		{
			Log.RecordLogWriter("Change file name of profile fail, because the old file is not exists.", bFailed: true);
		}
	}

	private void UpdateUI_Profile_DefaultRow()
	{
		gr_profile_position1.Children.Clear();
		gr_profile_position2.Children.Clear();
		gr_profile_position3.Children.Clear();
		gr_profile_position4.Children.Clear();
		gr_profile_position5.Children.Clear();
		gr_profile_position1.Background = System.Windows.Media.Brushes.Transparent;
		gr_profile_position2.Background = System.Windows.Media.Brushes.Transparent;
		gr_profile_position3.Background = System.Windows.Media.Brushes.Transparent;
		gr_profile_position4.Background = System.Windows.Media.Brushes.Transparent;
		gr_profile_position5.Background = System.Windows.Media.Brushes.Transparent;
		Grid.SetRow(gr_AddProfile, index_AddRow);
	}

	private void UpdateUI_Modify_Filename()
	{
		TextBlock textBlock = new TextBlock();
		textBlock.Name = "tb_Profile_displayName_" + rowIndex;
		if (CurrentProfileIndex == rowIndex)
		{
			textBlock.Style = (Style)FindResource("style_tb_current_choose_Profile");
		}
		else
		{
			textBlock.Style = (Style)FindResource("style_tb_choose_Profile");
		}
		textBlock.Text = tempFileName;
		System.Windows.Controls.Button button = new System.Windows.Controls.Button();
		button.Name = "bt_Profile_edit_" + rowIndex;
		button.Style = (Style)FindResource("style_bt_choose_Profile_edit");
		button.Click += Button_Profile_Modify_Click;
		System.Windows.Controls.Button button2 = new System.Windows.Controls.Button();
		button2.Name = "bt_Profile_save_" + rowIndex;
		button2.Style = (Style)FindResource("style_bt_choose_Profile_save");
		button2.Click += Button_Profile_Save_Click;
		System.Windows.Controls.Button button3 = new System.Windows.Controls.Button();
		button3.Name = "bt_Profile_delete_" + rowIndex;
		button3.Style = (Style)FindResource("style_bt_choose_Profile_delete");
		button3.Click += Button_Profile_Dele_Click;
		System.Windows.Controls.Button button4 = new System.Windows.Controls.Button();
		button4.Name = "bt_Profile_apply_" + rowIndex;
		button4.Style = (Style)FindResource("style_bt_choose_Profile_apply");
		button4.Click += Button_Profile_Apply_Click;
		if (rowIndex == 1)
		{
			gr_profile_position1.Children.Clear();
			if (CurrentProfileIndex == rowIndex)
			{
				gr_profile_position1.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(80, 138, byte.MaxValue));
			}
			else
			{
				gr_profile_position1.Background = System.Windows.Media.Brushes.Transparent;
			}
			gr_profile_position1.Children.Add(textBlock);
			gr_profile_position1.Children.Add(button);
			gr_profile_position1.Children.Add(button2);
			gr_profile_position1.Children.Add(button3);
			gr_profile_position1.Children.Add(button4);
		}
		if (rowIndex == 2)
		{
			gr_profile_position2.Children.Clear();
			if (CurrentProfileIndex == rowIndex)
			{
				gr_profile_position2.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(80, 138, byte.MaxValue));
			}
			else
			{
				gr_profile_position2.Background = System.Windows.Media.Brushes.Transparent;
			}
			gr_profile_position2.Children.Add(textBlock);
			gr_profile_position2.Children.Add(button);
			gr_profile_position2.Children.Add(button2);
			gr_profile_position2.Children.Add(button3);
			gr_profile_position2.Children.Add(button4);
		}
		if (rowIndex == 3)
		{
			gr_profile_position3.Children.Clear();
			if (CurrentProfileIndex == rowIndex)
			{
				gr_profile_position3.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(80, 138, byte.MaxValue));
			}
			else
			{
				gr_profile_position3.Background = System.Windows.Media.Brushes.Transparent;
			}
			gr_profile_position3.Children.Add(textBlock);
			gr_profile_position3.Children.Add(button);
			gr_profile_position3.Children.Add(button2);
			gr_profile_position3.Children.Add(button3);
			gr_profile_position3.Children.Add(button4);
		}
		if (rowIndex == 4)
		{
			gr_profile_position4.Children.Clear();
			if (CurrentProfileIndex == rowIndex)
			{
				gr_profile_position4.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(80, 138, byte.MaxValue));
			}
			else
			{
				gr_profile_position4.Background = System.Windows.Media.Brushes.Transparent;
			}
			gr_profile_position4.Children.Add(textBlock);
			gr_profile_position4.Children.Add(button);
			gr_profile_position4.Children.Add(button2);
			gr_profile_position4.Children.Add(button3);
			gr_profile_position4.Children.Add(button4);
		}
		if (rowIndex == 5)
		{
			gr_profile_position5.Children.Clear();
			if (CurrentProfileIndex == rowIndex)
			{
				gr_profile_position5.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(80, 138, byte.MaxValue));
			}
			else
			{
				gr_profile_position5.Background = System.Windows.Media.Brushes.Transparent;
			}
			gr_profile_position5.Children.Add(textBlock);
			gr_profile_position5.Children.Add(button);
			gr_profile_position5.Children.Add(button2);
			gr_profile_position5.Children.Add(button3);
			gr_profile_position5.Children.Add(button4);
		}
		if (CurrentProfileIndex == 0)
		{
			gr_profile_position1.Background = System.Windows.Media.Brushes.Transparent;
			gr_profile_position2.Background = System.Windows.Media.Brushes.Transparent;
			gr_profile_position3.Background = System.Windows.Media.Brushes.Transparent;
			gr_profile_position4.Background = System.Windows.Media.Brushes.Transparent;
			gr_profile_position5.Background = System.Windows.Media.Brushes.Transparent;
		}
	}

	private void UpdateUI_Profile()
	{
		if (flag_First_Load_Profile)
		{
			string text = $"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\Kingston Fury\\FuryCTRL\\Profile\\";
			try
			{
				if (File.Exists(text + "ProfileIndex.cfg"))
				{
					INI iNI = new INI("ProfileIndex.cfg");
					if (iNI.IniReadValue("CurrentProfileIndex", "All") != "" && iNI.IniReadValue("CurrentProfileIndex", "All") != null)
					{
						total_profiles = Convert.ToInt32(iNI.IniReadValue("CurrentProfileIndex", "All"));
						CurrentProfileIndex = Convert.ToInt32(iNI.IniReadValue("CurrentProfileIndex", "Index"));
						for (int i = 0; i < total_profiles; i++)
						{
							if (!(iNI.IniReadValue("FileName", "Profile_" + (i + 1)) != ""))
							{
								continue;
							}
							tempFileName = iNI.IniReadValue("FileName", "Profile_" + (i + 1));
							if (Check_File_Exists(tempFileName))
							{
								AddProfileInRow();
								if (i + 1 == Convert.ToInt32(iNI.IniReadValue("CurrentProfileIndex", "Index")) && Convert.ToInt32(new INI(tempFileName + ".cfg").IniReadValue("LEDInfo", "DeviceCount")) != _DRAMInfoRootObj.root.dram.Count)
								{
									iNI.IniWriteValue("CurrentProfileIndex", "Index", "0");
									CurrentProfileIndex = Convert.ToInt32(iNI.IniReadValue("CurrentProfileIndex", "Index"));
								}
							}
							else
							{
								iNI.IniWriteValue("FileName", "Profile_" + (i + 1), null);
							}
						}
					}
				}
				else
				{
					Log.RecordLogWriter("[Profile] >> the ProfileIndex.cfg is not exist. Create a new one.", bFailed: false);
					SaveProfile_Device();
				}
			}
			catch
			{
			}
			flag_First_Load_Profile = false;
			return;
		}
		INI iNI2 = new INI("ProfileIndex.cfg");
		if (Check_File_Exists("ProfileIndex"))
		{
			int num = Convert.ToInt32(iNI2.IniReadValue("CurrentProfileIndex", "Index"));
			if (CurrentProfileIndex == num && CurrentProfileIndex != 0)
			{
				return;
			}
			rowIndex = 1;
			for (int j = rowIndex; j <= total_profiles; j++)
			{
				tempFileName = iNI2.IniReadValue("FileName", "Profile_" + j);
				base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateUI_Modify_Filename));
				if (j < total_profiles)
				{
					rowIndex++;
				}
			}
		}
		else
		{
			total_profiles = 0;
			CurrentProfileIndex = 0;
			tempFileName = "";
			CurrentProfileIndex = 0;
			createdCount_profiles = 0;
			rowIndex = 0;
			index_AddRow = 2;
			oldFileName = "";
			base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateUI_Profile_DefaultRow));
			SaveProfile_Device();
		}
	}

	private bool Check_File_Exists(string fileName)
	{
		if (!File.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\Kingston Fury\\FuryCTRL\\Profile\\" + fileName + ".cfg"))
		{
			return false;
		}
		return true;
	}

	public void SaveProfile_Device()
	{
		Log.RecordLogWriter("[Profile] >> Creat the current profile info.", bFailed: false);
		INI ini;
		System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate
		{
			ini = new INI("ProfileIndex.cfg");
			ini.IniWriteValue("CurrentProfileIndex", "All", total_profiles.ToString());
			ini.IniWriteValue("CurrentProfileIndex", "Index", CurrentProfileIndex.ToString());
			if (!(tempFileName == "") || total_profiles != 0)
			{
				ini.IniWriteValue("FileName", "Profile_" + rowIndex, tempFileName);
				ini = new INI(tempFileName + ".cfg");
				ini.IniWriteValue("UISetting", "LEDEffect", Button_LEDlightOnOff.IsChecked.Value ? "Off" : "On");
				ini.IniWriteValue("UISetting", "KeepStartup", cb_keep_status.IsChecked.Value ? "Off" : "On");
				ini.IniWriteValue("LEDInfo", "ProductName", sProductName);
				ini.IniWriteValue("LEDInfo", "DeviceCount", _DRAMInfoRootObj.root.dram.Count.ToString());
				ini.IniWriteValue("LEDSetting", "Mode", sMode);
				ini.IniWriteValue("LEDSetting", "Option", sOption);
				ini.IniWriteValue("LEDSetting", "Speed", speedValue.ToString());
				ini.IniWriteValue("LEDSetting", "Birghtness", brightnessValue.ToString());
				ini.IniWriteValue("LEDSetting", "Darkness", darknessValue.ToString());
				if (sOption == "Default")
				{
					ini.IniWriteValue("ColorSetting_0", "ColorR_0", "");
					ini.IniWriteValue("ColorSetting_0", "ColorG_0", "");
					ini.IniWriteValue("ColorSetting_0", "ColorB_0", "");
				}
				else if (sOption == "OneColor")
				{
					string value = ColorPalette.GetCurrentRGB[0].ToString();
					string value2 = ColorPalette.GetCurrentRGB[1].ToString();
					string value3 = ColorPalette.GetCurrentRGB[2].ToString();
					ini.IniWriteValue("ColorSetting_0", "ColorR_0", value);
					ini.IniWriteValue("ColorSetting_0", "ColorG_0", value2);
					ini.IniWriteValue("ColorSetting_0", "ColorB_0", value3);
				}
				else if (sOption == "FiveColor")
				{
					if (_DRAMInfoRootObj.root.dram.Count <= 4)
					{
						for (int i = 0; i < _DRAMInfoRootObj.root.dram.Count; i++)
						{
							int num = 0;
							for (int j = 0; j < 4; j++)
							{
								if (j == _DRAMInfoRootObj.root.dram.ElementAt(i).Value.index)
								{
									_ = "slot_" + j;
									num = j;
								}
							}
							for (int k = 0; k < 5; k++)
							{
								string value4 = _DRAM_colors.ElementAt(i).Value.colors.ElementAt(k).ElementAt(0).ToString();
								string value5 = _DRAM_colors.ElementAt(i).Value.colors.ElementAt(k).ElementAt(1).ToString();
								string value6 = _DRAM_colors.ElementAt(i).Value.colors.ElementAt(k).ElementAt(2).ToString();
								ini.IniWriteValue("ColorSetting_" + num, "ColorR_" + k, value4);
								ini.IniWriteValue("ColorSetting_" + num, "ColorG_" + k, value5);
								ini.IniWriteValue("ColorSetting_" + num, "ColorB_" + k, value6);
							}
						}
					}
					else
					{
						for (int l = 0; l < 4; l++)
						{
							for (int m = 0; m < 5; m++)
							{
								string value7 = _DRAM_colors.ElementAt(l).Value.colors.ElementAt(m).ElementAt(0).ToString();
								string value8 = _DRAM_colors.ElementAt(l).Value.colors.ElementAt(m).ElementAt(1).ToString();
								string value9 = _DRAM_colors.ElementAt(l).Value.colors.ElementAt(m).ElementAt(2).ToString();
								ini.IniWriteValue("ColorSetting_" + l, "ColorR_" + m, value7);
								ini.IniWriteValue("ColorSetting_" + l, "ColorG_" + m, value8);
								ini.IniWriteValue("ColorSetting_" + l, "ColorB_" + m, value9);
							}
						}
					}
				}
				tempFileName = "";
			}
		});
	}

	public void LoadProfile_Device()
	{
		string text = $"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\Kingston Fury\\FuryCTRL\\Profile\\";
		try
		{
			if (!File.Exists(text + tempFileName + ".cfg"))
			{
				Log.RecordLogWriter("[Profile] Not found the profile: Setting_" + CurrentProfileIndex + "=" + tempFileName, bFailed: true);
				return;
			}
			Log.RecordLogWriter("[Profile] Found the profile: Setting_" + CurrentProfileIndex + "=" + tempFileName, bFailed: false);
			INI iNI = new INI(tempFileName + ".cfg");
			if (iNI.IniReadValue("UISetting", "LEDEffect") != null)
			{
				if (iNI.IniReadValue("UISetting", "LEDEffect") == "On")
				{
					Button_LEDlightOnOff.IsChecked = false;
				}
				else if (iNI.IniReadValue("UISetting", "LEDEffect") == "Off")
				{
					Button_LEDlightOnOff.IsChecked = true;
				}
				else
				{
					Log.RecordLogWriter("The setting of LEDlightOnOff format is wrong", bFailed: true);
				}
			}
			else
			{
				Log.RecordLogWriter("The setting of LEDlightOnOff is null.", bFailed: true);
			}
			if (iNI.IniReadValue("UISetting", "KeepStartup") != null)
			{
				if (iNI.IniReadValue("UISetting", "KeepStartup") == "On")
				{
					cb_keep_status.IsChecked = false;
				}
				else if (iNI.IniReadValue("UISetting", "KeepStartup") == "Off")
				{
					cb_keep_status.IsChecked = true;
				}
				else
				{
					Log.RecordLogWriter("The setting of KeepStartup format is wrong", bFailed: true);
				}
			}
			else
			{
				Log.RecordLogWriter("The setting of KeepStartup is null.", bFailed: true);
			}
			if (iNI.IniReadValue("LEDInfo", "DeviceCount") == null)
			{
				return;
			}
			if (Convert.ToInt32(iNI.IniReadValue("LEDInfo", "DeviceCount")) == _DRAMInfoRootObj.root.dram.Count)
			{
				sMode = iNI.IniReadValue("LEDSetting", "Mode");
				sOption = iNI.IniReadValue("LEDSetting", "Option");
				speedValue = Convert.ToInt16(iNI.IniReadValue("LEDSetting", "Speed"));
				brightnessValue = Convert.ToInt16(iNI.IniReadValue("LEDSetting", "Birghtness"));
				darknessValue = Convert.ToInt16(iNI.IniReadValue("LEDSetting", "Darkness"));
				int item = 0;
				int item2 = 0;
				int item3 = 0;
				if (sOption == "OneColor")
				{
					_DRAM_colors = new Dictionary<string, DRAMCtrlColorObj>();
					for (int i = 0; i < _DRAMInfoRootObj.root.dram.Count; i++)
					{
						string text2 = "ColorR_" + i;
						string text3 = "ColorG_" + i;
						string text4 = "ColorB_" + i;
						item = Convert.ToInt32(iNI.IniReadValue("ColorSetting_0", "ColorR_0"));
						item2 = Convert.ToInt32(iNI.IniReadValue("ColorSetting_0", "ColorG_0"));
						item3 = Convert.ToInt32(iNI.IniReadValue("ColorSetting_0", "ColorB_0"));
						List<List<int>> colors = new List<List<int>>
						{
							new List<int> { item, item2, item3 },
							new List<int> { item, item2, item3 },
							new List<int> { item, item2, item3 },
							new List<int> { item, item2, item3 },
							new List<int> { item, item2, item3 }
						};
						string text5 = "slot_";
						int num = 0;
						for (int j = 0; j < _DRAMInfoRootObj.root.dram.Count; j++)
						{
							if (j == _DRAMInfoRootObj.root.dram.ElementAt(i).Value.index)
							{
								text5 = "slot_" + j;
								num = j;
								_DRAM_colors.Add(text5, new DRAMCtrlColorObj(num, colors));
							}
						}
					}
					ColorPalette.Text_ColorR.Text = item.ToString();
					ColorPalette.Text_ColorG.Text = item2.ToString();
					ColorPalette.Text_ColorB.Text = item3.ToString();
				}
				else if (sOption == "FiveColor")
				{
					_DRAM_colors = new Dictionary<string, DRAMCtrlColorObj>();
					for (int k = 0; k < _DRAMInfoRootObj.root.dram.Count; k++)
					{
						string key = "slot_";
						int num2 = 0;
						for (int l = 0; l < 4; l++)
						{
							if (l == _DRAMInfoRootObj.root.dram.ElementAt(k).Value.index)
							{
								key = "slot_" + l;
								num2 = l;
							}
						}
						string section = "ColorSetting_" + num2;
						List<List<int>> list = new List<List<int>>();
						for (int m = 0; m < 5; m++)
						{
							string text2 = "ColorR_" + m;
							string text3 = "ColorG_" + m;
							string text4 = "ColorB_" + m;
							if (iNI.IniReadValue(section, text2) != null && iNI.IniReadValue(section, text2) != "")
							{
								item = Convert.ToInt32(iNI.IniReadValue(section, text2));
								item2 = Convert.ToInt32(iNI.IniReadValue(section, text3));
								item3 = Convert.ToInt32(iNI.IniReadValue(section, text4));
								list.Add(new List<int> { item, item2, item3 });
							}
							else
							{
								list.Add(new List<int> { 0, 0, 0 });
							}
						}
						_DRAM_colors.Add(key, new DRAMCtrlColorObj(num2, list));
					}
				}
				base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
				Send_Setting();
			}
			else
			{
				Log.RecordLogWriter("The device count is different.", bFailed: true);
			}
		}
		catch (Exception ex)
		{
			Log.RecordLogWriter("[Profile] LoadProfile_Device() catch. " + ex, bFailed: true);
		}
	}

	private void TextBox_Modify_Profile_FileName_LostFocus(object sender, RoutedEventArgs e)
	{
		TextBox_Modify_Profile_FileName.Visibility = Visibility.Hidden;
	}

	private void TextBox_Modify_Profile_FileName_GotFocus(object sender, RoutedEventArgs e)
	{
	}

	private void Profile_Was_Changed()
	{
		INI iNI = new INI("ProfileIndex.cfg");
		CurrentProfileIndex = 0;
		iNI.IniWriteValue("CurrentProfileIndex", "Index", CurrentProfileIndex.ToString());
	}

	private void Button_Close_Information_Click(object sender, RoutedEventArgs e)
	{
		gr_Infomation_mask.Visibility = Visibility.Hidden;
		ThumbTop_ForMask.Visibility = Visibility.Hidden;
	}

	private void Button_Infomation_Click(object sender, RoutedEventArgs e)
	{
		gr_Infomation_mask.Visibility = Visibility.Visible;
		ThumbTop_ForMask.Visibility = Visibility.Visible;
	}

	private void cb_Wlan_Interfaces_SelectionChanged(object sender, SelectionChangedEventArgs e)
	{
		if ((sender as System.Windows.Controls.ComboBox).Items.Count != 0)
		{
			LanguageItem languageItem_ID = LanguageList.GetLanguageItem_ID(Convert.ToInt32(((sender as System.Windows.Controls.ComboBox).SelectedItem as ComboBoxItem).Tag));
			if (languageItem_ID != null)
			{
				iSystemDefaultLangID = (int)languageItem_ID.Id;
				Change_Language_Click(null, null);
				SaveCustom_Language();
			}
		}
	}

	private void Change_Language_Click(object sender, RoutedEventArgs e)
	{
		try
		{
			if (cb_Language.SelectedIndex != -1)
			{
				resLangDictionary.Source = new Uri($"pack://application:,,,/Resources/Langs/{iSystemDefaultLangID}.xaml", UriKind.RelativeOrAbsolute);
			}
		}
		catch (Exception ex)
		{
			Console.WriteLine(ex.Message.ToString());
		}
	}

	public void SaveCustom_Language()
	{
		Log.RecordLogWriter("[Language] Save custom language setting", bFailed: false);
		INI ini;
		System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate
		{
			ini = new INI("Custom.cfg");
			ini.IniWriteValue("Language", "lan", iSystemDefaultLangID.ToString());
		});
	}

	private void Hyperlink_Privacy_Click(object sender, RoutedEventArgs e)
	{
		Process.Start(new ProcessStartInfo((sender as Hyperlink).NavigateUri.AbsoluteUri));
	}

	private async void Cb_keep_status_Click(object sender, RoutedEventArgs e)
	{
		StartupTask val = await StartupTask.GetAsync(DataCenter.TaskId);
		if ((sender as System.Windows.Controls.CheckBox).IsChecked ?? true)
		{
			using (File.Create(DataCenter.Path_StartUp_Switch))
			{
			}
			val.Disable();
			Profile_Was_Changed();
			Log.RecordLogWriter("Set startup disabled", bFailed: false);
			return;
		}
		try
		{
			if (File.Exists(DataCenter.Path_StartUp_Switch))
			{
				File.Delete(DataCenter.Path_StartUp_Switch);
			}
		}
		catch
		{
		}
		await val.RequestEnableAsync();
		Profile_Was_Changed();
		Log.RecordLogWriter("Set startup enabled", bFailed: false);
	}

	private void Cb_keep_status_Unchecked(object sender, RoutedEventArgs e)
	{
		SendToWebServer("{\"root\":{\"api\":\"set_keep_status\",\"keep\":\"1\"}");
		Console.WriteLine("{\"root\":{\"api\":\"set_keep_status\",\"keep\":\"1\"}");
		Log.RecordLogWriter("Set startup enabled", bFailed: false);
		bStartupEnable = false;
		Profile_Was_Changed();
	}

	private async void bt_apply_Click(object sender, RoutedEventArgs e)
	{
		gr_eula_mask.Visibility = Visibility.Hidden;
		StartFirstInstallSDK();
		CancellationTokenSource tokenSource = new CancellationTokenSource();
		base.Dispatcher.Invoke(DispatcherPriority.Background, (ThreadStart)delegate
		{
			EnableSetupMask(tokenSource.Token);
		});
		await Task.Run(delegate
		{
			Thread.Sleep(3000);
		});
		tokenSource.Cancel();
		StartConnectToKernelServer();
	}

	private void bt_cancel_Click(object sender, RoutedEventArgs e)
	{
		if (DataCenter.notifyIcon != null)
		{
			DataCenter.notifyIcon.Dispose();
		}
		Environment.Exit(0);
	}

	private void StartFirstInstallSDK()
	{
		try
		{
			Process.Start(new ProcessStartInfo
			{
				UseShellExecute = true,
				WorkingDirectory = Environment.CurrentDirectory,
				FileName = DataCenter.CurrentWorkPath + "\\" + sFirstSetupFileName,
				Arguments = sFirstSetupArgs,
				Verb = "runas"
			}).WaitForExit();
			Log.RecordLogWriter("Install SDK ...", bFailed: false);
			using (File.Create(DataCenter.Path_GDPR_Switch))
			{
			}
		}
		catch (Exception)
		{
			System.Windows.MessageBox.Show("Installation has been canceled!");
			Log.RecordLogWriter("Installation has been canceled!", bFailed: true);
			Environment.Exit(0);
		}
	}

	private void StartConnectToKernelServer()
	{
		ws = new WebSocket($"ws://127.0.0.1:{sPortNumber}/");
		ws.Origin += sOrigin;
		ws.OnMessage += Ws_OnMessage;
		ws.OnOpen += Ws_OnOpen;
		ws.OnClose += Ws_OnClose;
		ws.OnError += Ws_OnError;
		Task.Factory.StartNew(delegate
		{
			try
			{
				ws.Connect();
				Log.RecordLogWriter("Connect start...", bFailed: false);
			}
			catch (Exception)
			{
				Log.RecordLogWriter("Connect fail...", bFailed: true);
			}
		});
	}

	private string StartKernelSDKUpdate()
	{
		string text = "";
		string text2 = string.Format("{0}\\\\{1}", AppDomain.CurrentDomain.BaseDirectory.Replace("\\", "\\\\"), sSetupFileName);
		if (File.Exists(text2))
		{
			text += "{";
			text += "\"root\":";
			text += "{";
			text += "\"api\":\"set_kernel_update\",";
			text += "\"kernel_info\":";
			text += "{";
			text = text + "\"version\":\"" + TargetSDKVersion.ToString() + "\",";
			text = text + "\"setup_path\":\"" + text2 + "\",";
			text = text + "\"setup_args\":\"" + sSetupArgs + "\"";
			text += "}}}";
		}
		Log.RecordLogWriter("Send update kernel CMD", bFailed: false);
		return text;
	}

	private bool CheckKernelInstallOrNot()
	{
		bool result = false;
		try
		{
			object obj = RegistryReadValue(RegistryHive.LocalMachine, "Software\\Kingston Fury\\FuryCTRL", "AppVersion");
			if (obj != null)
			{
				SDKVersionInSystem = Version.Parse((string)obj);
				result = true;
				return result;
			}
			return result;
		}
		catch (Exception)
		{
			return result;
		}
	}

	public static object RegistryReadValue(RegistryHive hKey, string sRegPath, string sValueName, RegistryView hKeyView = RegistryView.Registry32)
	{
		try
		{
			RegistryKey registryKey = RegistryKey.OpenBaseKey(hKey, hKeyView);
			registryKey = registryKey.OpenSubKey(sRegPath, writable: false);
			if (registryKey != null)
			{
				return registryKey.GetValue(sValueName, null);
			}
		}
		catch (Exception)
		{
		}
		return null;
	}

	private void SendToWebServer(string sRequestContent)
	{
		if (!string.IsNullOrEmpty(sRequestContent))
		{
			ws.Send(StringEncryptDecrypt.Encrypt(sRequestContent, pw));
		}
	}

	private void Ws_OnError(object sender, WebSocketSharp.ErrorEventArgs e)
	{
	}

	private async void Ws_OnClose(object sender, CloseEventArgs e)
	{
		Console.WriteLine(e.Code + " : " + e.Reason);
		if (e.Code != 1006)
		{
			return;
		}
		if (iConnectRetryCount < iConnectRetryLimitationCount)
		{
			CancellationTokenSource tokenSource = new CancellationTokenSource();
			base.Dispatcher.Invoke(DispatcherPriority.Background, (ThreadStart)delegate
			{
				EnableWaitSDKComeBackMask(tokenSource.Token);
			});
			await Task.Run(delegate
			{
				Thread.Sleep(3000);
			});
			tokenSource.Cancel();
			Console.WriteLine($"iConnectRetryCount : {iConnectRetryCount.ToString()}");
			ws.ConnectAsync();
			iConnectRetryCount++;
		}
		else
		{
			base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateUIDispatcherDelegate(EnablegrReinstallAppMask));
			Console.WriteLine(string.Format("iConnectRetryCount reset.", iConnectRetryCount.ToString()));
			iConnectRetryCount = 0;
		}
	}

	private void EnableWaitingMask()
	{
		ThumbTop_ForMask.Visibility = Visibility.Visible;
		gr_wait_mask.Visibility = Visibility.Visible;
	}

	private async void EnableSetupMask(CancellationToken token)
	{
		ThumbTop_ForMask.Visibility = Visibility.Visible;
		gr_wait_mask.Visibility = Visibility.Visible;
		gr_setup_progress_mask.Visibility = Visibility.Visible;
		try
		{
			while (!token.IsCancellationRequested)
			{
				tb_setup_progress.Text = (string)FindResource("StringSetupProgress1");
				await Task.Delay(500, token);
				tb_setup_progress.Text = (string)FindResource("StringSetupProgress2");
				await Task.Delay(500, token);
				tb_setup_progress.Text = (string)FindResource("StringSetupProgress");
				await Task.Delay(500, token);
			}
		}
		catch (TaskCanceledException)
		{
			tb_setup_progress.Text = (string)FindResource("StringSetupFail");
		}
	}

	private async void EnableUpdateMask(CancellationToken token)
	{
		ThumbTop_ForMask.Visibility = Visibility.Visible;
		gr_wait_mask.Visibility = Visibility.Visible;
		gr_update_setup_progress_mask.Visibility = Visibility.Visible;
		try
		{
			while (!token.IsCancellationRequested)
			{
				tb_update_setup_progress.Text = (string)FindResource("StringUpdateProgress1");
				await Task.Delay(500, token);
				tb_update_setup_progress.Text = (string)FindResource("StringUpdateProgress2");
				await Task.Delay(500, token);
				tb_update_setup_progress.Text = (string)FindResource("StringUpdateProgress");
				await Task.Delay(500, token);
			}
		}
		catch (TaskCanceledException)
		{
			ThumbTop_ForMask.Visibility = Visibility.Visible;
			tb_update_setup_progress.Text = (string)FindResource("StringUpdateFail");
		}
	}

	private async void EnableWaitSDKComeBackMask(CancellationToken token)
	{
		ThumbTop_ForMask.Visibility = Visibility.Visible;
		gr_wait_mask.Visibility = Visibility.Visible;
		gr_wait_sdk_come_back_mask.Visibility = Visibility.Visible;
		try
		{
			while (!token.IsCancellationRequested)
			{
				tb_wait_sdk_come_back.Text = (string)FindResource("StringWaitSDKConnect1");
				await Task.Delay(500, token);
				tb_wait_sdk_come_back.Text = (string)FindResource("StringWaitSDKConnect2");
				await Task.Delay(500, token);
				tb_wait_sdk_come_back.Text = (string)FindResource("StringWaitSDKConnect");
				await Task.Delay(500, token);
			}
		}
		catch (TaskCanceledException)
		{
			tb_wait_sdk_come_back.Text = (string)FindResource("StringSDKConnectFail");
		}
	}

	private void EnablegrReinstallAppMask()
	{
		ThumbTop_ForMask.Visibility = Visibility.Visible;
		gr_wait_sdk_come_back_mask.Visibility = Visibility.Hidden;
		gr_reinstall_app_mask.Visibility = Visibility.Visible;
	}

	private void Ws_OnOpen(object sender, EventArgs e)
	{
		if (((WebSocket)sender).ReadyState == WebSocketState.Open)
		{
			if (SDKVersionInSystem == null)
			{
				CheckKernelInstallOrNot();
			}
			iConnectRetryCount = 0;
			SendToWebServer("{\"root\":{\"api\":\"get_version\"}");
		}
	}

	public void MessageText()
	{
		System.Windows.MessageBox.Show("not support device found!", "Warning", MessageBoxButton.OK);
		Close();
		Closing_Process();
	}

	private async void Ws_OnMessage(object senderr, MessageEventArgs e)
	{
		string text = StringEncryptDecrypt.Decrypt(e.Data, pw);
		Console.WriteLine(text);
		if (text.IndexOf("\"get_version\"", StringComparison.InvariantCultureIgnoreCase) >= 0)
		{
			try
			{
				DRAMJsonRoot dRAMJsonRoot = JsonClass.JavaScriptDeserialize<DRAMJsonRoot>(text);
				if (dRAMJsonRoot != null && dRAMJsonRoot.root != null && !string.IsNullOrEmpty(dRAMJsonRoot.root.version))
				{
					if (SDKVersionFromKernel == null)
					{
						SDKVersionFromKernel = Version.Parse(dRAMJsonRoot.root.version);
						if (SDKVersionInSystem != null && SDKVersionFromKernel.CompareTo(SDKVersionInSystem) >= 0)
						{
							if (SDKVersionFromKernel.CompareTo(TargetSDKVersion) >= 0)
							{
								Console.WriteLine("Kernel version is:" + dRAMJsonRoot.root.version);
								Log.RecordLogWriter("Kernel version is: " + dRAMJsonRoot.root.version, bFailed: false);
								SendToWebServer("{\"root\":{\"api\":\"get_keep_status\"}");
							}
							else
							{
								Log.RecordLogWriter("Request update kenel", bFailed: false);
								string text2 = StartKernelSDKUpdate();
								if (!string.IsNullOrEmpty(text2))
								{
									SendToWebServer(text2);
								}
								else
								{
									Log.RecordLogWriter("The FuryCTRL_SDK.exe not found.", bFailed: true);
								}
							}
						}
					}
					else
					{
						if (Version.Parse(dRAMJsonRoot.root.version).CompareTo(SDKVersionFromKernel) >= 0)
						{
							Console.WriteLine("Kernel server restart.");
							Log.RecordLogWriter("Kernel server restart.", bFailed: false);
						}
						else
						{
							Console.WriteLine("Kernel server restart manual.");
							Log.RecordLogWriter("Kernel server restart manual", bFailed: false);
						}
						SDKVersionFromKernel = Version.Parse(dRAMJsonRoot.root.version);
						if (SDKVersionFromKernel.CompareTo(TargetSDKVersion) >= 0)
						{
							Console.WriteLine("Kernel version is:" + dRAMJsonRoot.root.version);
							SendToWebServer("{\"root\":{\"api\":\"get_keep_status\"}");
						}
						else
						{
							Log.RecordLogWriter("Request update kenel", bFailed: false);
							string text3 = StartKernelSDKUpdate();
							if (!string.IsNullOrEmpty(text3))
							{
								SendToWebServer(text3);
							}
							else
							{
								Log.RecordLogWriter("The FuryCTRL_SDK.exe not found.", bFailed: true);
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return;
			}
		}
		if (text.IndexOf("\"set_kernel_update\"", StringComparison.InvariantCultureIgnoreCase) >= 0)
		{
			try
			{
				_DRAMInfoRootObj = JsonClass.JavaScriptDeserialize<DRAMJsonRoot>(text);
				if (_DRAMInfoRootObj != null)
				{
					if (_DRAMInfoRootObj.root.status == "0")
					{
						Log.RecordLogWriter("updated kernell is success!", bFailed: false);
						SDKVersionInSystem = null;
						SDKVersionFromKernel = null;
						CancellationTokenSource tokenSource = new CancellationTokenSource();
						base.Dispatcher.Invoke(DispatcherPriority.Background, (ThreadStart)delegate
						{
							EnableUpdateMask(tokenSource.Token);
						});
						await Task.Run(delegate
						{
							Thread.Sleep(3000);
						});
						tokenSource.Cancel();
					}
					else if (_DRAMInfoRootObj.root.status == "1")
					{
						Log.RecordLogWriter("updated kernell is fall! (Unknow error)", bFailed: true);
					}
					else if (_DRAMInfoRootObj.root.status == "2")
					{
						Log.RecordLogWriter("updated kernell is fall! (Object error)", bFailed: true);
					}
					else if (_DRAMInfoRootObj.root.status == "3")
					{
						Log.RecordLogWriter("updated kernell is fall! (Version error)", bFailed: true);
					}
					else if (_DRAMInfoRootObj.root.status == "4")
					{
						Log.RecordLogWriter("updated kernell is fall! (File error)", bFailed: true);
					}
				}
				return;
			}
			catch
			{
				return;
			}
		}
		if (text.IndexOf("\"get_dram_info\"", StringComparison.InvariantCultureIgnoreCase) >= 0)
		{
			try
			{
				Log.RecordLogWriter("Get dream info of response: " + text, bFailed: false);
				_DRAMInfoRootObj = JsonClass.JavaScriptDeserialize<DRAMJsonRoot>(text);
				if (_DRAMInfoRootObj != null)
				{
					if (_DRAMInfoRootObj.root.dram != null && _DRAMInfoRootObj.root.dram.Count != 0)
					{
						_DRAM_colors = new Dictionary<string, DRAMCtrlColorObj>();
					}
					else
					{
						base.Dispatcher.Invoke(DispatcherPriority.Normal, new DisplayMsg(MessageText));
					}
					SendToWebServer("{\"root\":{\"api\":\"get_dram_led\"}");
				}
				return;
			}
			catch (Exception ex2)
			{
				Console.WriteLine(ex2.Message);
				return;
			}
		}
		if (text.IndexOf("\"get_dram_led\"", StringComparison.InvariantCultureIgnoreCase) >= 0)
		{
			try
			{
				Log.RecordLogWriter("Get dream led of response: " + text, bFailed: false);
				_DRAMGetDRAMLED = JsonClass.JavaScriptDeserialize<DRAMJsonRoot>(text);
				if (_DRAMGetDRAMLED != null && _DRAMGetDRAMLED.root != null && !string.IsNullOrEmpty(_DRAMGetDRAMLED.root.status) && _DRAMGetDRAMLED.root.status == "0")
				{
					sMode = _DRAMGetDRAMLED.root.ctrl_settings.mode;
					sOption = _DRAMGetDRAMLED.root.ctrl_settings.ctrl_mode;
					speedValue = _DRAMGetDRAMLED.root.ctrl_settings.speed;
					brightnessValue = _DRAMGetDRAMLED.root.ctrl_settings.brightness;
					darknessValue = _DRAMGetDRAMLED.root.ctrl_settings.darkness;
					if (sMode == "meteor" || sMode == "static")
					{
						brightnessValue = 100;
					}
					if (sOption == "def" || sOption == "ctrl")
					{
						sOption = "DefaultColor";
						for (int i = 0; i < _DRAMInfoRootObj.root.dram.Count; i++)
						{
							List<List<int>> colors = new List<List<int>>
							{
								new List<int> { 255, 0, 0 },
								new List<int> { 255, 0, 0 },
								new List<int> { 255, 0, 0 },
								new List<int> { 255, 0, 0 },
								new List<int> { 255, 0, 0 }
							};
							_DRAM_colors.Add(_DRAMInfoRootObj.root.dram.ElementAt(i).Key, new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(i).Value.index, colors));
						}
					}
					else if (sOption == "ctrl_color")
					{
						sOption = "OneColor";
						for (int j = 0; j < _DRAMInfoRootObj.root.dram.Count; j++)
						{
							List<List<int>> colors2 = _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.ElementAt(0).Value.colors;
							_DRAM_colors.Add(_DRAMInfoRootObj.root.dram.ElementAt(j).Key, new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(j).Value.index, colors2));
							Log.RecordLogWriter("color: " + colors2.ElementAt(0).ElementAt(0) + "," + colors2.ElementAt(0).ElementAt(1) + "," + colors2.ElementAt(0).ElementAt(2), bFailed: false);
						}
					}
					else if (sOption == "independence")
					{
						sOption = "FiveColor";
						try
						{
							int num = 0;
							for (int k = 0; k < _DRAMInfoRootObj.root.dram.Count; k++)
							{
								string[] array = _DRAMInfoRootObj.root.dram.ElementAt(k).Key.Split('_');
								int num2;
								if (array.Length >= 2)
								{
									num2 = Convert.ToInt32(array[1]);
									Log.RecordLogWriter("tempInfoIndex: " + num2, bFailed: false);
								}
								else
								{
									num2 = k;
								}
								string[] array2 = _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.ElementAt(num).Key.Split('_');
								int num3;
								if (array2.Length >= 2)
								{
									num3 = Convert.ToInt32(array2[1]);
									Log.RecordLogWriter("Get previous LED slot number:" + num3, bFailed: false);
								}
								else
								{
									num3 = num;
								}
								if (num3 == k)
								{
									List<List<int>> colors3 = _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.ElementAt(num).Value.colors;
									_DRAM_colors.Add("slot_" + num3, new DRAMCtrlColorObj(num3, colors3));
									Log.RecordLogWriter("Get LED setting: " + num3, bFailed: false);
									if (num + 1 < _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.Count)
									{
										num++;
									}
								}
								else if (k < _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.Count)
								{
									if (_DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.Count < _DRAMInfoRootObj.root.dram.Count)
									{
										List<List<int>> colors3 = _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.ElementAt(k).Value.colors;
										_DRAM_colors.Add("slot_" + k, new DRAMCtrlColorObj(k, colors3));
										Log.RecordLogWriter("(less -> more) Get LED setting: " + k, bFailed: false);
										if (num + 1 < _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.Count)
										{
											num++;
										}
									}
									else if (_DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.Count > _DRAMInfoRootObj.root.dram.Count)
									{
										List<List<int>> colors3 = _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.ElementAt(k).Value.colors;
										_DRAM_colors.Add("slot_" + num3, new DRAMCtrlColorObj(num3, colors3));
										Log.RecordLogWriter("(more -> less) Get LED setting: " + num3, bFailed: false);
										if (num + 1 < _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.Count)
										{
											num++;
										}
									}
									else
									{
										List<List<int>> colors3 = _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.ElementAt(k).Value.colors;
										_DRAM_colors.Add("slot_" + num3, new DRAMCtrlColorObj(num3, colors3));
										Log.RecordLogWriter("(slot num is the same) Get LED setting: " + num3, bFailed: false);
										if (num + 1 < _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.Count)
										{
											num++;
										}
									}
								}
								else
								{
									List<List<int>> colors3 = new List<List<int>>
									{
										new List<int> { 0, 0, 0 },
										new List<int> { 0, 0, 0 },
										new List<int> { 0, 0, 0 },
										new List<int> { 0, 0, 0 },
										new List<int> { 0, 0, 0 }
									};
									_DRAM_colors.Add("slot_" + num2, new DRAMCtrlColorObj(num2, colors3));
									Log.RecordLogWriter("Get LED setting: " + num2, bFailed: false);
								}
								for (int l = 0; l < 5; l++)
								{
									Log.RecordLogWriter("color: " + _DRAM_colors.ElementAt(k).Value.colors.ElementAt(l).ElementAt(0) + "," + _DRAM_colors.ElementAt(k).Value.colors.ElementAt(l).ElementAt(1) + "," + _DRAM_colors.ElementAt(k).Value.colors.ElementAt(l).ElementAt(2), bFailed: false);
								}
							}
						}
						catch
						{
							Log.RecordLogWriter("Recive count of DRAM color is: " + _DRAMGetDRAMLED.root.ctrl_settings.ctrl_color.Count, bFailed: true);
							Log.RecordLogWriter("Recive count of DRAM is:" + _DRAMInfoRootObj.root.dram.Count, bFailed: true);
							Log.RecordLogWriter("Five color setting wrong. Check the slot count.", bFailed: true);
						}
					}
				}
				else
				{
					Log.RecordLogWriter("Set DRAM LED as default", bFailed: false);
					for (int m = 0; m < _DRAMInfoRootObj.root.dram.Count; m++)
					{
						List<List<int>> colors4 = new List<List<int>>
						{
							new List<int> { 255, 0, 0 },
							new List<int> { 255, 0, 0 },
							new List<int> { 255, 0, 0 },
							new List<int> { 255, 0, 0 },
							new List<int> { 255, 0, 0 }
						};
						_DRAM_colors.Add(_DRAMInfoRootObj.root.dram.ElementAt(m).Key, new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(m).Value.index, colors4));
					}
				}
				base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
				Log.RecordLogWriter("UI synchronization completes the LED custom settings.", bFailed: false);
				Send_Setting();
				return;
			}
			catch (Exception ex3)
			{
				Console.WriteLine(ex3.Message);
				return;
			}
		}
		if (text.IndexOf("\"set_dram_led\"", StringComparison.InvariantCultureIgnoreCase) >= 0)
		{
			try
			{
				Log.RecordLogWriter("Get dream led of response.", bFailed: false);
				DRAMJsonRoot dRAMJsonRoot2 = JsonClass.JavaScriptDeserialize<DRAMJsonRoot>(text);
				if (dRAMJsonRoot2 != null && dRAMJsonRoot2.root != null && !string.IsNullOrEmpty(dRAMJsonRoot2.root.status))
				{
					if (dRAMJsonRoot2.root.status == "0")
					{
						Log.RecordLogWriter("Send CMD status: Successful", bFailed: false);
						Console.WriteLine(dRAMJsonRoot2.root.status);
					}
					else
					{
						Log.RecordLogWriter("Send CMD status: fail", bFailed: false);
						Console.WriteLine(dRAMJsonRoot2.root.status);
					}
				}
				return;
			}
			catch (Exception ex4)
			{
				Console.WriteLine(ex4.Message);
				return;
			}
		}
		if (text.IndexOf("\"get_keep_status\"", StringComparison.InvariantCultureIgnoreCase) >= 0)
		{
			try
			{
				DRAMJsonRoot dRAMJsonRoot3 = JsonClass.JavaScriptDeserialize<DRAMJsonRoot>(text);
				if (dRAMJsonRoot3 != null && dRAMJsonRoot3.root != null && dRAMJsonRoot3.root.keep != null)
				{
					if (dRAMJsonRoot3.root.keep == "1")
					{
						Console.WriteLine("Get setup keep: " + dRAMJsonRoot3.root.keep);
						SendToWebServer("{\"root\":{\"api\":\"get_is8dimm_status\"}");
					}
					else
					{
						Console.WriteLine("Get setup keep: " + dRAMJsonRoot3.root.keep);
						SendToWebServer("{\"root\":{\"api\":\"get_is8dimm_status\"}");
					}
				}
				return;
			}
			catch (Exception ex5)
			{
				Log.RecordLogWriter("Get keep status fail. " + ex5, bFailed: true);
				Console.WriteLine(ex5.Message);
				return;
			}
		}
		if (text.IndexOf("\"set_keep_status\"", StringComparison.InvariantCultureIgnoreCase) >= 0)
		{
			try
			{
				Log.RecordLogWriter("Get keep status of response.", bFailed: false);
				DRAMJsonRoot dRAMJsonRoot4 = JsonClass.JavaScriptDeserialize<DRAMJsonRoot>(text);
				if (dRAMJsonRoot4 != null && dRAMJsonRoot4.root != null && dRAMJsonRoot4.root.status != null)
				{
					Log.RecordLogWriter("Set Startup status:" + dRAMJsonRoot4.root.status, bFailed: false);
				}
				return;
			}
			catch (Exception ex6)
			{
				Console.WriteLine(ex6.Message);
				return;
			}
		}
		if (text.IndexOf("\"get_is8dimm_status\"", StringComparison.InvariantCultureIgnoreCase) < 0)
		{
			return;
		}
		try
		{
			DRAMJsonRoot dRAMJsonRoot5 = JsonClass.JavaScriptDeserialize<DRAMJsonRoot>(text);
			if (dRAMJsonRoot5 != null && dRAMJsonRoot5.root != null && dRAMJsonRoot5.root.status != null)
			{
				if (dRAMJsonRoot5.root.is8dimm == "1")
				{
					bIs8Dimm = true;
				}
				else
				{
					bIs8Dimm = false;
				}
				Log.RecordLogWriter("Is 8 dimm mb: " + bIs8Dimm, bFailed: false);
				Console.WriteLine("Is 8 dimm mb: " + dRAMJsonRoot5.root.is8dimm);
				SendToWebServer("{\"root\":{\"api\":\"get_dram_info\"}");
			}
		}
		catch (Exception ex7)
		{
			Log.RecordLogWriter("Get 8 dimm status fail. " + ex7, bFailed: true);
			Console.WriteLine(ex7.Message);
		}
	}

	private void Send_Setting()
	{
		if (ws.ReadyState != WebSocketState.Open)
		{
			return;
		}
		DRAMJsonRoot dRAMJsonRoot = new DRAMJsonRoot();
		dRAMJsonRoot.root = new DRAMCmdObj();
		dRAMJsonRoot.root.ctrl_settings = new DRAMCtrlObj();
		if (sOption == "DefaultColor")
		{
			switch (sMode)
			{
			case "running":
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t running,default", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
				break;
			case "breath":
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.darkness = darknessValue;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t breath,default", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
				break;
			case "rainbow":
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t rainbow,default", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
				break;
			case "meteor":
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t meteor,default", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
				break;
			case "blink":
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t blink,default", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
				break;
			case "double_blink":
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t double blink,default", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
				break;
			case "color_cycle":
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t color cycle,default", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
				break;
			case "static_color":
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "def";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t static,default", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
				break;
			case "all_off":
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "def";
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t all off", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
				break;
			}
		}
		else if (sOption == "OneColor")
		{
			if (sMode == "running")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl_color";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = new Dictionary<string, DRAMCtrlColorObj>();
				for (int i = 0; i < _DRAMInfoRootObj.root.dram.Count; i++)
				{
					if (flag_FirstSetting)
					{
						dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
						continue;
					}
					List<int> list = new List<int>();
					if (bRandom)
					{
						list.Add(iRandom_R);
						list.Add(iRandom_G);
						list.Add(iRandom_B);
					}
					else
					{
						list.Add(ColorPalette.GetCurrentRGB[0]);
						list.Add(ColorPalette.GetCurrentRGB[1]);
						list.Add(ColorPalette.GetCurrentRGB[2]);
					}
					List<List<int>> list2 = new List<List<int>>();
					list2.Add(list);
					DRAMCtrlColorObj value = new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(i).Value.index, list2);
					dRAMJsonRoot.root.ctrl_settings.ctrl_color.Add(_DRAMInfoRootObj.root.dram.ElementAt(i).Key, value);
				}
				flag_FirstSetting = false;
				bRandom = false;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t running,one color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "breath")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl_color";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.darkness = darknessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = new Dictionary<string, DRAMCtrlColorObj>();
				for (int j = 0; j < _DRAMInfoRootObj.root.dram.Count; j++)
				{
					if (flag_FirstSetting)
					{
						dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
						continue;
					}
					List<int> list3 = new List<int>();
					if (bRandom)
					{
						list3.Add(iRandom_R);
						list3.Add(iRandom_G);
						list3.Add(iRandom_B);
					}
					else
					{
						list3.Add(ColorPalette.GetCurrentRGB[0]);
						list3.Add(ColorPalette.GetCurrentRGB[1]);
						list3.Add(ColorPalette.GetCurrentRGB[2]);
					}
					List<List<int>> list4 = new List<List<int>>();
					list4.Add(list3);
					DRAMCtrlColorObj value2 = new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(j).Value.index, list4);
					dRAMJsonRoot.root.ctrl_settings.ctrl_color.Add(_DRAMInfoRootObj.root.dram.ElementAt(j).Key, value2);
				}
				flag_FirstSetting = false;
				bRandom = false;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t breath,one color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "meteor")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl_color";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = new Dictionary<string, DRAMCtrlColorObj>();
				for (int k = 0; k < _DRAMInfoRootObj.root.dram.Count; k++)
				{
					if (flag_FirstSetting)
					{
						dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
						continue;
					}
					List<int> list5 = new List<int>();
					if (bRandom)
					{
						list5.Add(iRandom_R);
						list5.Add(iRandom_G);
						list5.Add(iRandom_B);
					}
					else
					{
						list5.Add(ColorPalette.GetCurrentRGB[0]);
						list5.Add(ColorPalette.GetCurrentRGB[1]);
						list5.Add(ColorPalette.GetCurrentRGB[2]);
					}
					List<List<int>> list6 = new List<List<int>>();
					list6.Add(list5);
					DRAMCtrlColorObj value3 = new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(k).Value.index, list6);
					dRAMJsonRoot.root.ctrl_settings.ctrl_color.Add(_DRAMInfoRootObj.root.dram.ElementAt(k).Key, value3);
				}
				flag_FirstSetting = false;
				bRandom = false;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t meteor,one color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "blink")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl_color";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = new Dictionary<string, DRAMCtrlColorObj>();
				for (int l = 0; l < _DRAMInfoRootObj.root.dram.Count; l++)
				{
					if (flag_FirstSetting)
					{
						dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
						continue;
					}
					List<int> list7 = new List<int>();
					if (bRandom)
					{
						list7.Add(iRandom_R);
						list7.Add(iRandom_G);
						list7.Add(iRandom_B);
					}
					else
					{
						list7.Add(ColorPalette.GetCurrentRGB[0]);
						list7.Add(ColorPalette.GetCurrentRGB[1]);
						list7.Add(ColorPalette.GetCurrentRGB[2]);
					}
					List<List<int>> list8 = new List<List<int>>();
					list8.Add(list7);
					DRAMCtrlColorObj value4 = new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(l).Value.index, list8);
					dRAMJsonRoot.root.ctrl_settings.ctrl_color.Add(_DRAMInfoRootObj.root.dram.ElementAt(l).Key, value4);
				}
				flag_FirstSetting = false;
				bRandom = false;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t blink,one color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "static_color")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl_color";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = new Dictionary<string, DRAMCtrlColorObj>();
				for (int m = 0; m < _DRAMInfoRootObj.root.dram.Count; m++)
				{
					if (flag_FirstSetting)
					{
						dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
						continue;
					}
					List<int> list9 = new List<int>();
					if (bRandom)
					{
						list9.Add(iRandom_R);
						list9.Add(iRandom_G);
						list9.Add(iRandom_B);
					}
					else
					{
						list9.Add(ColorPalette.GetCurrentRGB[0]);
						list9.Add(ColorPalette.GetCurrentRGB[1]);
						list9.Add(ColorPalette.GetCurrentRGB[2]);
					}
					List<List<int>> list10 = new List<List<int>>();
					list10.Add(list9);
					DRAMCtrlColorObj value5 = new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(m).Value.index, list10);
					dRAMJsonRoot.root.ctrl_settings.ctrl_color.Add(_DRAMInfoRootObj.root.dram.ElementAt(m).Key, value5);
				}
				flag_FirstSetting = false;
				bRandom = false;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t static,one color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "double_blink")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl_color";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = new Dictionary<string, DRAMCtrlColorObj>();
				for (int n = 0; n < _DRAMInfoRootObj.root.dram.Count; n++)
				{
					if (flag_FirstSetting)
					{
						dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
						continue;
					}
					List<int> list11 = new List<int>();
					if (bRandom)
					{
						list11.Add(iRandom_R);
						list11.Add(iRandom_G);
						list11.Add(iRandom_B);
					}
					else
					{
						list11.Add(ColorPalette.GetCurrentRGB[0]);
						list11.Add(ColorPalette.GetCurrentRGB[1]);
						list11.Add(ColorPalette.GetCurrentRGB[2]);
					}
					List<List<int>> list12 = new List<List<int>>();
					list12.Add(list11);
					DRAMCtrlColorObj value6 = new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(n).Value.index, list12);
					dRAMJsonRoot.root.ctrl_settings.ctrl_color.Add(_DRAMInfoRootObj.root.dram.ElementAt(n).Key, value6);
				}
				flag_FirstSetting = false;
				bRandom = false;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t double blink,one color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "color_cycle")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "ctrl_color";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = new Dictionary<string, DRAMCtrlColorObj>();
				for (int num = 0; num < _DRAMInfoRootObj.root.dram.Count; num++)
				{
					if (flag_FirstSetting)
					{
						dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
						continue;
					}
					List<int> list13 = new List<int>();
					if (bRandom)
					{
						list13.Add(iRandom_R);
						list13.Add(iRandom_G);
						list13.Add(iRandom_B);
					}
					else
					{
						list13.Add(ColorPalette.GetCurrentRGB[0]);
						list13.Add(ColorPalette.GetCurrentRGB[1]);
						list13.Add(ColorPalette.GetCurrentRGB[2]);
					}
					List<List<int>> list14 = new List<List<int>>();
					list14.Add(list13);
					DRAMCtrlColorObj value7 = new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(num).Value.index, list14);
					dRAMJsonRoot.root.ctrl_settings.ctrl_color.Add(_DRAMInfoRootObj.root.dram.ElementAt(num).Key, value7);
				}
				flag_FirstSetting = false;
				bRandom = false;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t color cycle,one color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "all_off")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "def";
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t all off", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
		}
		else if (sOption == "FiveColor")
		{
			if (sMode == "running")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "independence";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t running,five color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "breath")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "independence";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.darkness = darknessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t breath,five color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "blink")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "independence";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t blink,five color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "static_color")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "independence";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t static,five color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "double_blink")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "independence";
				dRAMJsonRoot.root.ctrl_settings.speed = speedValue;
				dRAMJsonRoot.root.ctrl_settings.brightness = brightnessValue;
				dRAMJsonRoot.root.ctrl_settings.ctrl_color = _DRAM_colors;
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("SetDRAMLEDStyle:\t double blink,five color", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			else if (sMode == "all_off")
			{
				dRAMJsonRoot.root.api = "set_dram_led";
				dRAMJsonRoot.root.ctrl_settings.mode = sMode;
				dRAMJsonRoot.root.ctrl_settings.ctrl_mode = "def";
				sSetDRAMLEDStyle = JsonClass.JavaScriptSerialize(dRAMJsonRoot);
				Log.RecordLogWriter("all", bFailed: false);
				SendToWebServer(sSetDRAMLEDStyle);
			}
			bRandom = false;
			flag_FirstSetting = false;
		}
		Log.RecordLogWriter(sSetDRAMLEDStyle, bFailed: false);
	}

	private void UpdateDeviceInfoUI()
	{
		gr_eula_mask.Visibility = Visibility.Hidden;
		gr_setup_progress_mask.Visibility = Visibility.Hidden;
		gr_update_setup_progress_mask.Visibility = Visibility.Hidden;
		gr_wait_sdk_come_back_mask.Visibility = Visibility.Hidden;
		gr_wait_mask.Visibility = Visibility.Hidden;
		gr_reinstall_app_mask.Visibility = Visibility.Hidden;
		ThumbTop_ForMask.Visibility = Visibility.Hidden;
		try
		{
			deviceCount = _DRAMInfoRootObj.root.dram.Count;
			if (deviceCount == 0)
			{
				return;
			}
			if (bIs8Dimm)
			{
				CheckLEDStyle();
				gr_DevicePanel_8DIMM_1_Childs.Children.Clear();
				gr_DevicePanel_8DIMM_2_Childs.Children.Clear();
				for (int i = 0; i < deviceCount; i++)
				{
					sProductName = _DRAMInfoRootObj.root.dram.ElementAt(i).Value.product;
					int index = Convert.ToInt32(_DRAMInfoRootObj.root.dram.ElementAt(i).Value.index);
					CheckDevicePannel_8DIMM(sProductName, index);
				}
				return;
			}
			CheckLEDStyle();
			gr_MultiColors_Childs.Children.Clear();
			bool flag = false;
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			if (deviceCount >= 4)
			{
				deviceCount = 4;
			}
			for (int j = 0; j < deviceCount; j++)
			{
				sProductName = _DRAMInfoRootObj.root.dram.ElementAt(j).Value.product;
				slotIndex = Convert.ToInt32(_DRAMInfoRootObj.root.dram.ElementAt(j).Value.index);
				CheckDevicePannel();
				string[] array = _DRAMInfoRootObj.root.dram.ElementAt(j).Key.Split('_');
				if (j + 1 < _DRAMInfoRootObj.root.dram.Count)
				{
					string[] array2 = _DRAMInfoRootObj.root.dram.ElementAt(j + 1).Key.Split('_');
					if (array.Length >= 2 && array2.Length >= 2)
					{
						num = Convert.ToInt32(array[1]);
						num2 = Convert.ToInt32(array2[1]);
						if (num < num2)
						{
							if (flag)
							{
								slotIndex = num3;
								flag = false;
							}
							else
							{
								slotIndex = num;
								flag = false;
							}
						}
						else
						{
							slotIndex = num2;
							flag = true;
							num3 = num;
						}
					}
				}
				else
				{
					num = (slotIndex = Convert.ToInt32(array[1]));
				}
				CheckDeviceSlot();
			}
			if (sMode == "all_off")
			{
				Button_LEDlightOnOff.IsChecked = true;
			}
			Slider_Speed.Value = speedValue;
			Slider_Brightness.Value = brightnessValue;
			Slider_Darkness.Value = darknessValue;
		}
		catch (Exception ex)
		{
			Log.RecordLogWriter("UpdateDeviceInfoUI catch. " + ex, bFailed: true);
		}
	}

	internal void SystemEvents_DisplaySettingsChanged(object sender, EventArgs e)
	{
		Stream stream = System.Windows.Application.GetResourceStream(DataCenter.Uri_AppIcon).Stream;
		DataCenter.notifyIcon.Icon = new Icon(stream);
		Size_MainWindow = C_ScreenView.TransferWindowSize(DataCenter._MainWindow, 2960.0, 1600.0);
		Console.WriteLine("Main_Window width = " + Size_MainWindow.Width + ", height=" + Size_MainWindow.Height);
	}

	private void ThumbTop_Move(object sender, MouseButtonEventArgs e)
	{
		DragMove();
	}

	private void Window_Loaded(object sender, RoutedEventArgs e)
	{
		Size_MainWindow = C_ScreenView.TransferWindowSize(this, 2960.0, 1600.0);
		base.MaxWidth = C_ScreenView.ActualWidth;
		base.MaxHeight = C_ScreenView.ActualHeight;
		C_ScreenView.TransferWindowPostion(this);
		SystemEvents.DisplaySettingsChanged += SystemEvents_DisplaySettingsChanged;
		ToolStripMenuItem value = new ToolStripMenuItem("Exit", null, CloseAPP);
		contextMenuStrip = new ContextMenuStrip();
		contextMenuStrip.Items.Add(value);
		DataCenter.notifyIcon = new NotifyIcon();
		DataCenter.notifyIcon.Text = DataCenter.ProjectName;
		Stream stream = System.Windows.Application.GetResourceStream(DataCenter.Uri_AppIcon).Stream;
		DataCenter.notifyIcon.Icon = new Icon(stream);
		DataCenter.notifyIcon.Visible = true;
		DataCenter.notifyIcon.ContextMenuStrip = contextMenuStrip;
		DataCenter.notifyIcon.MouseDoubleClick += notifyIcon_MouseDoubleClick;
		if (DataCenter.bShowGDPR)
		{
			DataCenter.FuryCTRL_WindowStatus = false;
			base.ShowInTaskbar = true;
			base.Topmost = true;
			base.Topmost = false;
		}
		if (DataCenter.bStartupApp)
		{
			Log.RecordLogWriter("[Startup] Hide the window", bFailed: false);
			base.Visibility = Visibility.Hidden;
			base.WindowState = WindowState.Maximized;
			DataCenter.bStartupApp = false;
		}
		cb_keep_status.IsChecked = File.Exists(DataCenter.Path_StartUp_Switch);
		windowfirstload = false;
	}

	private void Button_Min_Click(object sender, RoutedEventArgs e)
	{
		base.WindowState = WindowState.Minimized;
		DataCenter.FuryCTRL_WindowStatus = false;
		FlushMemory();
	}

	private void Button_Max_Click(object sender, RoutedEventArgs e)
	{
		if (!bMaxWindow)
		{
			base.WindowState = WindowState.Maximized;
			DataCenter.FuryCTRL_WindowStatus = false;
			FlushMemory();
			bMaxWindow = true;
		}
		else if (bMaxWindow)
		{
			base.WindowState = WindowState.Normal;
			DataCenter.FuryCTRL_WindowStatus = false;
			FlushMemory();
			bMaxWindow = false;
		}
	}

	private void Button_Close_Click(object sender, RoutedEventArgs e)
	{
		base.Visibility = Visibility.Hidden;
		DataCenter.FuryCTRL_WindowStatus = false;
		FlushMemory();
	}

	public void FlushMemory()
	{
		try
		{
			GC.Collect();
			GC.WaitForPendingFinalizers();
			if (Environment.OSVersion.Platform == PlatformID.Win32NT)
			{
				SetProcessWorkingSetSize(Process.GetCurrentProcess().Handle, -1, -1);
			}
		}
		catch (Exception ex)
		{
			Log.RecordLogWriter("Flush memory error = " + ex.Message.ToString(), bFailed: false);
		}
	}

	private void notifyIcon_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
	{
		if (e.Button == MouseButtons.Left)
		{
			DataCenter.FuryCTRL_WindowStatus = true;
			Restore_MainWindow();
		}
	}

	internal void Restore_MainWindow()
	{
		base.ShowInTaskbar = true;
		base.Visibility = Visibility.Visible;
		dc_main.Visibility = Visibility.Visible;
		base.WindowState = WindowState.Normal;
		foreach (Window window in System.Windows.Application.Current.Windows)
		{
			if (window != this && window.Owner == this && window.Tag != null)
			{
				window.Visibility = (Visibility)window.Tag;
				continue;
			}
			Rect workArea = SystemParameters.WorkArea;
			base.Left = (workArea.Width - base.Width) / 2.0 + workArea.Left;
			base.Top = (workArea.Height - base.Height) / 2.0 + workArea.Top;
		}
	}

	internal void CloseAPP(object sender, EventArgs e)
	{
		Closing_Process();
	}

	internal void Closing_Process()
	{
		try
		{
			if (DataCenter.notifyIcon != null)
			{
				DataCenter.notifyIcon.Dispose();
			}
			Environment.Exit(0);
		}
		catch
		{
			Environment.Exit(0);
		}
	}

	private void Window_Closing(object sender, CancelEventArgs e)
	{
		e.Cancel = true;
		base.Visibility = Visibility.Hidden;
		DataCenter.FuryCTRL_WindowStatus = false;
		FlushMemory();
	}

	private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
	{
		if (e.NewSize.Width == e.PreviousSize.Height * (double)AspectRatio)
		{
			e.Handled = true;
			return;
		}
		if (e.NewSize.Height == e.PreviousSize.Width / (double)AspectRatio)
		{
			e.Handled = true;
			return;
		}
		if (e.HeightChanged && e.WidthChanged)
		{
			if (base.Height <= base.MaxHeight && base.Height > base.MinHeight && base.Width <= base.MaxWidth && base.Width > base.MinWidth)
			{
				if (base.Width > base.Height * (double)AspectRatio)
				{
					base.Width = base.Height * (double)AspectRatio;
				}
				else if (base.Width < base.Height * (double)AspectRatio)
				{
					base.Height = base.Width / (double)AspectRatio;
				}
			}
			else if (base.Height <= base.MinHeight)
			{
				base.Height = base.MinHeight;
				base.Width = base.Height * (double)AspectRatio;
			}
			else if (base.Width <= base.MinWidth)
			{
				base.Width = base.MinWidth;
				base.Height = base.Width / (double)AspectRatio;
			}
			else if (base.Height <= base.MinHeight && base.Width <= base.MinWidth)
			{
				base.Height = base.MinHeight;
				base.Width = base.MinWidth;
			}
			else if (base.Height > base.MaxHeight && base.Width <= base.MaxWidth && base.Width > base.MinWidth)
			{
				base.Height = base.Width / (double)AspectRatio;
			}
			else if (base.Height <= base.MaxHeight && base.Height > base.MinHeight && base.Width > base.MaxWidth)
			{
				base.Width = base.Height * (double)AspectRatio;
			}
			else if (base.Height > base.MaxHeight && base.Width > base.MaxWidth)
			{
				base.Height = base.MaxHeight;
				base.Width = base.MaxWidth;
			}
		}
		else if (e.HeightChanged && !e.WidthChanged)
		{
			if (base.Height >= base.MaxHeight)
			{
				base.Height = base.MaxHeight;
				base.Width = base.Height * (double)AspectRatio;
			}
			else
			{
				base.Width = base.Height * (double)AspectRatio;
			}
		}
		else if (!e.HeightChanged && e.WidthChanged)
		{
			if (base.Width >= base.MaxWidth)
			{
				base.Width = base.MaxWidth;
				base.Height = base.Width / (double)AspectRatio;
			}
			else
			{
				base.Height = base.Width / (double)AspectRatio;
			}
		}
		LastWidth = base.Width;
		LastHeight = base.Height;
		if (!windowfirstload)
		{
			Settings.Default.WindowHeight = base.Height;
			Settings.Default.WindowWidth = base.Width;
			Settings.Default.Save();
		}
	}

	private void Window_MouseUp(object sender, MouseButtonEventArgs e)
	{
		if (base.Height != LastHeight && base.Height <= base.MaxHeight)
		{
			base.Width = base.Height * (double)AspectRatio;
		}
		else if (base.Width != LastWidth && base.Width <= base.MaxWidth)
		{
			base.Height = base.Width / (double)AspectRatio;
		}
		if (base.Height >= base.MaxHeight)
		{
			base.Height = base.MaxHeight;
		}
		if (base.Width >= base.MaxWidth)
		{
			base.Width = base.MaxWidth;
		}
	}

	public void CheckLEDStyle()
	{
		if (sOption == "DefaultColor")
		{
			Button_DefaultColor.IsChecked = true;
			Button_OneColor.IsChecked = false;
			Button_FiveColor.IsChecked = false;
			bPalette = false;
			ColorPaletteEnable(bPalette);
			Button_RandomColor.Visibility = Visibility.Hidden;
			canMultiColors.Visibility = Visibility.Hidden;
		}
		else if (sOption == "OneColor")
		{
			Button_DefaultColor.IsChecked = false;
			Button_OneColor.IsChecked = true;
			Button_FiveColor.IsChecked = false;
			bPalette = true;
			ColorPaletteEnable(bPalette);
			Button_RandomColor.Visibility = Visibility.Visible;
			canMultiColors.Visibility = Visibility.Hidden;
		}
		else if (sOption == "FiveColor")
		{
			Button_DefaultColor.IsChecked = false;
			Button_OneColor.IsChecked = false;
			Button_FiveColor.IsChecked = true;
			bPalette = true;
			ColorPaletteEnable(bPalette);
			Button_RandomColor.Visibility = Visibility.Visible;
			canMultiColors.Visibility = Visibility.Visible;
		}
		Button_Rainbow.IsChecked = false;
		if (sMode == "running")
		{
			Button_Running.IsChecked = true;
			Button_OneColor.IsEnabled = true;
			if (bIs8Dimm)
			{
				Button_FiveColor.IsEnabled = false;
			}
			else
			{
				Button_FiveColor.IsEnabled = true;
			}
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (sMode == "breath")
		{
			Button_Breath.IsChecked = true;
			Button_OneColor.IsEnabled = true;
			Button_FiveColor.IsEnabled = false;
			SpeedSettingOn();
			BrightnessSettingOn();
			DarknessSettingOn();
		}
		else if (sMode == "color_cycle")
		{
			Button_ColorCycle.IsChecked = true;
			Button_OneColor.IsEnabled = false;
			Button_FiveColor.IsEnabled = false;
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (sMode == "rainbow")
		{
			Button_Rainbow.IsChecked = true;
			Button_OneColor.IsEnabled = false;
			Button_FiveColor.IsEnabled = false;
			Button_Rainbow.IsChecked = true;
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (sMode == "blink")
		{
			Button_Blink.IsChecked = true;
			Button_OneColor.IsEnabled = true;
			if (bIs8Dimm)
			{
				Button_FiveColor.IsEnabled = false;
			}
			else
			{
				Button_FiveColor.IsEnabled = true;
			}
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (sMode == "double_blink")
		{
			Button_DoubleBlink.IsChecked = true;
			Button_OneColor.IsEnabled = true;
			if (bIs8Dimm)
			{
				Button_FiveColor.IsEnabled = false;
			}
			else
			{
				Button_FiveColor.IsEnabled = true;
			}
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (sMode == "meteor")
		{
			Button_Meteor.IsChecked = true;
			Button_OneColor.IsEnabled = true;
			Button_FiveColor.IsEnabled = false;
			SpeedSettingOn();
			BrightnessSettingOff();
		}
		else if (sMode == "static_color")
		{
			Button_Static.IsChecked = true;
			Button_OneColor.IsEnabled = true;
			Button_FiveColor.IsEnabled = false;
			if (bIs8Dimm)
			{
				Button_FiveColor.IsEnabled = false;
			}
			else
			{
				Button_FiveColor.IsEnabled = true;
			}
			SpeedSettingOff();
			BrightnessSettingOff();
		}
		else if (sMode == "all_off")
		{
			Button_Running.IsEnabled = false;
			Button_Running.IsChecked = false;
			Button_Breath.IsEnabled = false;
			Button_Breath.IsChecked = false;
			Button_Rainbow.IsEnabled = false;
			Button_Rainbow.IsChecked = false;
			Button_Meteor.IsEnabled = false;
			Button_Meteor.IsChecked = false;
			Button_Blink.IsChecked = false;
			Button_Blink.IsEnabled = false;
			Button_Static.IsChecked = false;
			Button_Static.IsEnabled = false;
			Button_DoubleBlink.IsChecked = false;
			Button_DoubleBlink.IsEnabled = false;
			Button_ColorCycle.IsEnabled = false;
			Button_ColorCycle.IsChecked = false;
			Button_DefaultColor.IsEnabled = false;
			Button_DefaultColor.IsChecked = false;
			Button_OneColor.IsEnabled = false;
			Button_OneColor.IsChecked = false;
			Button_FiveColor.IsEnabled = false;
			Button_FiveColor.IsChecked = false;
			Button_RandomColor.Visibility = Visibility.Hidden;
			bPalette = false;
			ColorPaletteEnable(bPalette);
			canMultiColors.Visibility = Visibility.Hidden;
			SpeedSettingOff();
			BrightnessSettingOff();
			DarknessSettingOff();
		}
	}

	public void CheckDevicePannel_8DIMM(string sProduct, int index)
	{
		switch (sProduct)
		{
		case "Fury":
		{
			UC_Pannel_DRAM_Fury uC_Pannel_DRAM_Fury = new UC_Pannel_DRAM_Fury();
			uC_Pannel_DRAM_Fury.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
			uC_Pannel_DRAM_Fury.VerticalAlignment = VerticalAlignment.Top;
			if (index <= 3)
			{
				Thickness margin5 = default(Thickness);
				margin5.Top = 103 * index;
				uC_Pannel_DRAM_Fury.Margin = margin5;
				gr_DevicePanel_8DIMM_1_Childs.Children.Add(uC_Pannel_DRAM_Fury);
				Log.RecordLogWriter("8DIMM_1 >> Add slot" + index, bFailed: false);
			}
			else
			{
				Thickness margin6 = default(Thickness);
				margin6.Top = 103 * (index - 4);
				uC_Pannel_DRAM_Fury.Margin = margin6;
				gr_DevicePanel_8DIMM_2_Childs.Children.Add(uC_Pannel_DRAM_Fury);
				Log.RecordLogWriter("8DIMM_2 >> Add slot" + index, bFailed: false);
			}
			if (sMode == "all_off")
			{
				byte[] array5 = new byte[1] { 125 };
				uC_Pannel_DRAM_Fury.SetStyleEffect(sMode, sOption, array5, array5, array5, array5, array5);
			}
			else if (sOption == "DefaultColor")
			{
				if (uC_Pannel_DRAM_Fury != null)
				{
					byte[] in_ColorValue19 = new byte[3];
					byte[] in_ColorValue20 = new byte[3];
					byte[] in_ColorValue21 = new byte[3];
					byte[] in_ColorValue22 = new byte[3];
					byte[] in_ColorValue23 = new byte[3];
					uC_Pannel_DRAM_Fury.SetStyleEffect(sMode, sOption, in_ColorValue19, in_ColorValue20, in_ColorValue21, in_ColorValue22, in_ColorValue23);
				}
			}
			else if (sOption == "OneColor")
			{
				byte[] array6 = new byte[3];
				byte[] in_ColorValue24 = new byte[3];
				byte[] in_ColorValue25 = new byte[3];
				byte[] in_ColorValue26 = new byte[3];
				byte[] in_ColorValue27 = new byte[3];
				array6[0] = ColorPalette.GetCurrentRGB[0];
				array6[1] = ColorPalette.GetCurrentRGB[1];
				array6[2] = ColorPalette.GetCurrentRGB[2];
				uC_Pannel_DRAM_Fury.SetStyleEffect(sMode, sOption, array6, in_ColorValue24, in_ColorValue25, in_ColorValue26, in_ColorValue27);
			}
			return;
		}
		case "Beast":
		{
			UC_Pannel_DRAM_Beast uC_Pannel_DRAM_Beast = new UC_Pannel_DRAM_Beast();
			uC_Pannel_DRAM_Beast.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
			uC_Pannel_DRAM_Beast.VerticalAlignment = VerticalAlignment.Top;
			if (index <= 3)
			{
				Thickness margin3 = default(Thickness);
				margin3.Top = 103 * index;
				uC_Pannel_DRAM_Beast.Margin = margin3;
				gr_DevicePanel_8DIMM_1_Childs.Children.Add(uC_Pannel_DRAM_Beast);
				Log.RecordLogWriter("8DIMM_1 >> Add slot" + index, bFailed: false);
			}
			else
			{
				Thickness margin4 = default(Thickness);
				margin4.Top = 103 * (index - 4);
				uC_Pannel_DRAM_Beast.Margin = margin4;
				gr_DevicePanel_8DIMM_2_Childs.Children.Add(uC_Pannel_DRAM_Beast);
				Log.RecordLogWriter("8DIMM_2 >> Add slot" + index, bFailed: false);
			}
			if (sMode == "all_off")
			{
				byte[] array3 = new byte[1] { 125 };
				uC_Pannel_DRAM_Beast.SetStyleEffect(sMode, sOption, array3, array3, array3, array3, array3);
			}
			else if (sOption == "DefaultColor")
			{
				if (uC_Pannel_DRAM_Beast != null)
				{
					byte[] in_ColorValue10 = new byte[3];
					byte[] in_ColorValue11 = new byte[3];
					byte[] in_ColorValue12 = new byte[3];
					byte[] in_ColorValue13 = new byte[3];
					byte[] in_ColorValue14 = new byte[3];
					uC_Pannel_DRAM_Beast.SetStyleEffect(sMode, sOption, in_ColorValue10, in_ColorValue11, in_ColorValue12, in_ColorValue13, in_ColorValue14);
				}
			}
			else if (sOption == "OneColor")
			{
				byte[] array4 = new byte[3];
				byte[] in_ColorValue15 = new byte[3];
				byte[] in_ColorValue16 = new byte[3];
				byte[] in_ColorValue17 = new byte[3];
				byte[] in_ColorValue18 = new byte[3];
				array4[0] = ColorPalette.GetCurrentRGB[0];
				array4[1] = ColorPalette.GetCurrentRGB[1];
				array4[2] = ColorPalette.GetCurrentRGB[2];
				uC_Pannel_DRAM_Beast.SetStyleEffect(sMode, sOption, array4, in_ColorValue15, in_ColorValue16, in_ColorValue17, in_ColorValue18);
			}
			return;
		}
		case "Predator":
		{
			UC_Pannel_DRAM_Predator uC_Pannel_DRAM_Predator = new UC_Pannel_DRAM_Predator();
			uC_Pannel_DRAM_Predator.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
			uC_Pannel_DRAM_Predator.VerticalAlignment = VerticalAlignment.Top;
			if (index <= 3)
			{
				Thickness margin = default(Thickness);
				margin.Top = 103 * index;
				uC_Pannel_DRAM_Predator.Margin = margin;
				gr_DevicePanel_8DIMM_1_Childs.Children.Add(uC_Pannel_DRAM_Predator);
				Log.RecordLogWriter("8DIMM_1 >> Add slot" + index, bFailed: false);
			}
			else
			{
				Thickness margin2 = default(Thickness);
				margin2.Top = 103 * (index - 4);
				uC_Pannel_DRAM_Predator.Margin = margin2;
				gr_DevicePanel_8DIMM_2_Childs.Children.Add(uC_Pannel_DRAM_Predator);
				Log.RecordLogWriter("8DIMM_2 >> Add slot" + index, bFailed: false);
			}
			if (sMode == "all_off")
			{
				byte[] array = new byte[1] { 125 };
				uC_Pannel_DRAM_Predator.SetStyleEffect(sMode, sOption, array, array, array, array, array);
			}
			else if (sOption == "DefaultColor")
			{
				if (uC_Pannel_DRAM_Predator != null)
				{
					byte[] in_ColorValue = new byte[3];
					byte[] in_ColorValue2 = new byte[3];
					byte[] in_ColorValue3 = new byte[3];
					byte[] in_ColorValue4 = new byte[3];
					byte[] in_ColorValue5 = new byte[3];
					uC_Pannel_DRAM_Predator.SetStyleEffect(sMode, sOption, in_ColorValue, in_ColorValue2, in_ColorValue3, in_ColorValue4, in_ColorValue5);
				}
			}
			else if (sOption == "OneColor")
			{
				byte[] array2 = new byte[3];
				byte[] in_ColorValue6 = new byte[3];
				byte[] in_ColorValue7 = new byte[3];
				byte[] in_ColorValue8 = new byte[3];
				byte[] in_ColorValue9 = new byte[3];
				array2[0] = ColorPalette.GetCurrentRGB[0];
				array2[1] = ColorPalette.GetCurrentRGB[1];
				array2[2] = ColorPalette.GetCurrentRGB[2];
				uC_Pannel_DRAM_Predator.SetStyleEffect(sMode, sOption, array2, in_ColorValue6, in_ColorValue7, in_ColorValue8, in_ColorValue9);
			}
			return;
		}
		}
		UC_Pannel_DRAM_Renegade uC_Pannel_DRAM_Renegade = new UC_Pannel_DRAM_Renegade();
		uC_Pannel_DRAM_Renegade.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
		uC_Pannel_DRAM_Renegade.VerticalAlignment = VerticalAlignment.Top;
		if (index <= 3)
		{
			Thickness margin7 = default(Thickness);
			margin7.Top = 103 * index;
			uC_Pannel_DRAM_Renegade.Margin = margin7;
			gr_DevicePanel_8DIMM_1_Childs.Children.Add(uC_Pannel_DRAM_Renegade);
			Log.RecordLogWriter("8DIMM_1 >> Add slot" + index, bFailed: false);
		}
		else
		{
			Thickness margin8 = default(Thickness);
			margin8.Top = 103 * (index - 4);
			uC_Pannel_DRAM_Renegade.Margin = margin8;
			gr_DevicePanel_8DIMM_2_Childs.Children.Add(uC_Pannel_DRAM_Renegade);
			Log.RecordLogWriter("8DIMM_2 >> Add slot" + index, bFailed: false);
		}
		if (sMode == "all_off")
		{
			byte[] array7 = new byte[1] { 125 };
			uC_Pannel_DRAM_Renegade.SetStyleEffect(sMode, sOption, array7, array7, array7, array7, array7);
		}
		else if (sOption == "DefaultColor")
		{
			if (uC_Pannel_DRAM_Renegade != null)
			{
				byte[] in_ColorValue28 = new byte[3];
				byte[] in_ColorValue29 = new byte[3];
				byte[] in_ColorValue30 = new byte[3];
				byte[] in_ColorValue31 = new byte[3];
				byte[] in_ColorValue32 = new byte[3];
				uC_Pannel_DRAM_Renegade.SetStyleEffect(sMode, sOption, in_ColorValue28, in_ColorValue29, in_ColorValue30, in_ColorValue31, in_ColorValue32);
			}
		}
		else if (sOption == "OneColor")
		{
			byte[] array8 = new byte[3];
			byte[] in_ColorValue33 = new byte[3];
			byte[] in_ColorValue34 = new byte[3];
			byte[] in_ColorValue35 = new byte[3];
			byte[] in_ColorValue36 = new byte[3];
			array8[0] = ColorPalette.GetCurrentRGB[0];
			array8[1] = ColorPalette.GetCurrentRGB[1];
			array8[2] = ColorPalette.GetCurrentRGB[2];
			uC_Pannel_DRAM_Renegade.SetStyleEffect(sMode, sOption, array8, in_ColorValue33, in_ColorValue34, in_ColorValue35, in_ColorValue36);
		}
	}

	public void CheckDevicePannel()
	{
		if (createdCount >= deviceCount)
		{
			return;
		}
		switch (sProductName)
		{
		case "Fury":
			switch (deviceCount)
			{
			case 1:
				if (Panel_DRAM_Fury0 == null)
				{
					Panel_DRAM_Fury0 = new UC_Pannel_DRAM_Fury();
					Panel_DRAM_Fury0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
					Panel_DRAM_Fury0.VerticalAlignment = VerticalAlignment.Top;
					Thickness margin40 = default(Thickness);
					margin40.Top = 154.0;
					Panel_DRAM_Fury0.Margin = margin40;
					gr_DevicePanel_Childs.Children.Add(Panel_DRAM_Fury0);
				}
				if (sMode == "all_off")
				{
					byte[] array274 = new byte[1] { 125 };
					Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array274, array274, array274, array274, array274);
				}
				else if (sOption == "DefaultColor")
				{
					if (Panel_DRAM_Fury0 != null)
					{
						byte[] in_ColorValue352 = new byte[3];
						byte[] in_ColorValue353 = new byte[3];
						byte[] in_ColorValue354 = new byte[3];
						byte[] in_ColorValue355 = new byte[3];
						byte[] in_ColorValue356 = new byte[3];
						Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, in_ColorValue352, in_ColorValue353, in_ColorValue354, in_ColorValue355, in_ColorValue356);
					}
				}
				else if (sOption == "OneColor")
				{
					if (Panel_DRAM_Fury0 != null)
					{
						byte[] array275 = new byte[3];
						byte[] in_ColorValue357 = new byte[3];
						byte[] in_ColorValue358 = new byte[3];
						byte[] in_ColorValue359 = new byte[3];
						byte[] in_ColorValue360 = new byte[3];
						array275[0] = ColorPalette.GetCurrentRGB[0];
						array275[1] = ColorPalette.GetCurrentRGB[1];
						array275[2] = ColorPalette.GetCurrentRGB[2];
						Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array275, in_ColorValue357, in_ColorValue358, in_ColorValue359, in_ColorValue360);
					}
				}
				else if (sOption == "FiveColor" && Panel_DRAM_Fury0 != null)
				{
					byte[] array276 = new byte[3];
					byte[] array277 = new byte[3];
					byte[] array278 = new byte[3];
					byte[] array279 = new byte[3];
					byte[] array280 = new byte[3];
					for (int num34 = 0; num34 < 5; num34++)
					{
						List<List<int>> colors40 = _DRAM_colors.ElementAt(0).Value.colors;
						switch (num34)
						{
						case 0:
							array276[0] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(0)));
							array276[1] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(1)));
							array276[2] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(2)));
							break;
						case 1:
							array277[0] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(0)));
							array277[1] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(1)));
							array277[2] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(2)));
							break;
						case 2:
							array278[0] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(0)));
							array278[1] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(1)));
							array278[2] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(2)));
							break;
						case 3:
							array279[0] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(0)));
							array279[1] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(1)));
							array279[2] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(2)));
							break;
						case 4:
							array280[0] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(0)));
							array280[1] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(1)));
							array280[2] = Convert.ToByte(Convert.ToByte(colors40[num34].ElementAt(2)));
							break;
						}
					}
					Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array276, array277, array278, array279, array280);
				}
				createdCount++;
				break;
			case 2:
				switch (createdCount)
				{
				case 0:
					if (Panel_DRAM_Fury0 == null)
					{
						Panel_DRAM_Fury0 = new UC_Pannel_DRAM_Fury();
						Panel_DRAM_Fury0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Panel_DRAM_Fury0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin39 = default(Thickness);
						margin39.Top = 84.0;
						Panel_DRAM_Fury0.Margin = margin39;
						gr_DevicePanel_Childs.Children.Add(Panel_DRAM_Fury0);
					}
					if (sMode == "all_off")
					{
						byte[] array267 = new byte[1] { 125 };
						Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array267, array267, array267, array267, array267);
					}
					else if (sOption == "DefaultColor")
					{
						if (Panel_DRAM_Fury0 != null)
						{
							byte[] in_ColorValue343 = new byte[3];
							byte[] in_ColorValue344 = new byte[3];
							byte[] in_ColorValue345 = new byte[3];
							byte[] in_ColorValue346 = new byte[3];
							byte[] in_ColorValue347 = new byte[3];
							Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, in_ColorValue343, in_ColorValue344, in_ColorValue345, in_ColorValue346, in_ColorValue347);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Panel_DRAM_Fury0 != null)
						{
							byte[] array268 = new byte[3];
							byte[] in_ColorValue348 = new byte[3];
							byte[] in_ColorValue349 = new byte[3];
							byte[] in_ColorValue350 = new byte[3];
							byte[] in_ColorValue351 = new byte[3];
							array268[0] = ColorPalette.GetCurrentRGB[0];
							array268[1] = ColorPalette.GetCurrentRGB[1];
							array268[2] = ColorPalette.GetCurrentRGB[2];
							Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array268, in_ColorValue348, in_ColorValue349, in_ColorValue350, in_ColorValue351);
						}
					}
					else if (sOption == "FiveColor" && Panel_DRAM_Fury0 != null)
					{
						byte[] array269 = new byte[3];
						byte[] array270 = new byte[3];
						byte[] array271 = new byte[3];
						byte[] array272 = new byte[3];
						byte[] array273 = new byte[3];
						for (int num33 = 0; num33 < 5; num33++)
						{
							List<List<int>> colors39 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num33)
							{
							case 0:
								array269[0] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(0)));
								array269[1] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(1)));
								array269[2] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(2)));
								break;
							case 1:
								array270[0] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(0)));
								array270[1] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(1)));
								array270[2] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(2)));
								break;
							case 2:
								array271[0] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(0)));
								array271[1] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(1)));
								array271[2] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(2)));
								break;
							case 3:
								array272[0] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(0)));
								array272[1] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(1)));
								array272[2] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(2)));
								break;
							case 4:
								array273[0] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(0)));
								array273[1] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(1)));
								array273[2] = Convert.ToByte(Convert.ToByte(colors39[num33].ElementAt(2)));
								break;
							}
						}
						Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array269, array270, array271, array272, array273);
					}
					createdCount++;
					break;
				case 1:
					if (Panel_DRAM_Fury1 == null)
					{
						Panel_DRAM_Fury1 = new UC_Pannel_DRAM_Fury();
						Panel_DRAM_Fury1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Panel_DRAM_Fury1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin38 = default(Thickness);
						margin38.Top = 225.0;
						Panel_DRAM_Fury1.Margin = margin38;
						gr_DevicePanel_Childs.Children.Add(Panel_DRAM_Fury1);
					}
					if (sMode == "all_off")
					{
						byte[] array260 = new byte[1] { 125 };
						Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, array260, array260, array260, array260, array260);
					}
					else if (sOption == "DefaultColor")
					{
						if (Panel_DRAM_Fury1 != null)
						{
							byte[] in_ColorValue334 = new byte[3];
							byte[] in_ColorValue335 = new byte[3];
							byte[] in_ColorValue336 = new byte[3];
							byte[] in_ColorValue337 = new byte[3];
							byte[] in_ColorValue338 = new byte[3];
							Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, in_ColorValue334, in_ColorValue335, in_ColorValue336, in_ColorValue337, in_ColorValue338);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Panel_DRAM_Fury1 != null)
						{
							byte[] array261 = new byte[3];
							byte[] in_ColorValue339 = new byte[3];
							byte[] in_ColorValue340 = new byte[3];
							byte[] in_ColorValue341 = new byte[3];
							byte[] in_ColorValue342 = new byte[3];
							array261[0] = ColorPalette.GetCurrentRGB[0];
							array261[1] = ColorPalette.GetCurrentRGB[1];
							array261[2] = ColorPalette.GetCurrentRGB[2];
							Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, array261, in_ColorValue339, in_ColorValue340, in_ColorValue341, in_ColorValue342);
						}
					}
					else if (sOption == "FiveColor" && Panel_DRAM_Fury1 != null)
					{
						byte[] array262 = new byte[3];
						byte[] array263 = new byte[3];
						byte[] array264 = new byte[3];
						byte[] array265 = new byte[3];
						byte[] array266 = new byte[3];
						for (int num32 = 0; num32 < 5; num32++)
						{
							List<List<int>> colors38 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (num32)
							{
							case 0:
								array262[0] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(0)));
								array262[1] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(1)));
								array262[2] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(2)));
								break;
							case 1:
								array263[0] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(0)));
								array263[1] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(1)));
								array263[2] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(2)));
								break;
							case 2:
								array264[0] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(0)));
								array264[1] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(1)));
								array264[2] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(2)));
								break;
							case 3:
								array265[0] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(0)));
								array265[1] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(1)));
								array265[2] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(2)));
								break;
							case 4:
								array266[0] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(0)));
								array266[1] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(1)));
								array266[2] = Convert.ToByte(Convert.ToByte(colors38[num32].ElementAt(2)));
								break;
							}
						}
						Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, array262, array263, array264, array265, array266);
					}
					createdCount++;
					break;
				}
				break;
			case 3:
				switch (createdCount)
				{
				case 0:
					if (Panel_DRAM_Fury0 == null)
					{
						Panel_DRAM_Fury0 = new UC_Pannel_DRAM_Fury();
						Panel_DRAM_Fury0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Panel_DRAM_Fury0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin37 = default(Thickness);
						margin37.Top = 52.0;
						Panel_DRAM_Fury0.Margin = margin37;
						gr_DevicePanel_Childs.Children.Add(Panel_DRAM_Fury0);
					}
					if (sMode == "all_off")
					{
						byte[] array253 = new byte[1] { 125 };
						Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array253, array253, array253, array253, array253);
					}
					else if (sOption == "DefaultColor")
					{
						if (Panel_DRAM_Fury0 != null)
						{
							byte[] in_ColorValue325 = new byte[3];
							byte[] in_ColorValue326 = new byte[3];
							byte[] in_ColorValue327 = new byte[3];
							byte[] in_ColorValue328 = new byte[3];
							byte[] in_ColorValue329 = new byte[3];
							Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, in_ColorValue325, in_ColorValue326, in_ColorValue327, in_ColorValue328, in_ColorValue329);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Panel_DRAM_Fury0 != null)
						{
							byte[] array254 = new byte[3];
							byte[] in_ColorValue330 = new byte[3];
							byte[] in_ColorValue331 = new byte[3];
							byte[] in_ColorValue332 = new byte[3];
							byte[] in_ColorValue333 = new byte[3];
							array254[0] = ColorPalette.GetCurrentRGB[0];
							array254[1] = ColorPalette.GetCurrentRGB[1];
							array254[2] = ColorPalette.GetCurrentRGB[2];
							Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array254, in_ColorValue330, in_ColorValue331, in_ColorValue332, in_ColorValue333);
						}
					}
					else if (sOption == "FiveColor" && Panel_DRAM_Fury0 != null)
					{
						byte[] array255 = new byte[3];
						byte[] array256 = new byte[3];
						byte[] array257 = new byte[3];
						byte[] array258 = new byte[3];
						byte[] array259 = new byte[3];
						for (int num31 = 0; num31 < 5; num31++)
						{
							List<List<int>> colors37 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num31)
							{
							case 0:
								array255[0] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(0)));
								array255[1] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(1)));
								array255[2] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(2)));
								break;
							case 1:
								array256[0] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(0)));
								array256[1] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(1)));
								array256[2] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(2)));
								break;
							case 2:
								array257[0] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(0)));
								array257[1] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(1)));
								array257[2] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(2)));
								break;
							case 3:
								array258[0] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(0)));
								array258[1] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(1)));
								array258[2] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(2)));
								break;
							case 4:
								array259[0] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(0)));
								array259[1] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(1)));
								array259[2] = Convert.ToByte(Convert.ToByte(colors37[num31].ElementAt(2)));
								break;
							}
						}
						Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array255, array256, array257, array258, array259);
					}
					createdCount++;
					break;
				case 1:
					if (Panel_DRAM_Fury1 == null)
					{
						Panel_DRAM_Fury1 = new UC_Pannel_DRAM_Fury();
						Panel_DRAM_Fury1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Panel_DRAM_Fury1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin36 = default(Thickness);
						margin36.Top = 155.0;
						Panel_DRAM_Fury1.Margin = margin36;
						gr_DevicePanel_Childs.Children.Add(Panel_DRAM_Fury1);
					}
					if (sMode == "all_off")
					{
						byte[] array246 = new byte[1] { 125 };
						Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, array246, array246, array246, array246, array246);
					}
					else if (sOption == "DefaultColor")
					{
						if (Panel_DRAM_Fury1 != null)
						{
							byte[] in_ColorValue316 = new byte[3];
							byte[] in_ColorValue317 = new byte[3];
							byte[] in_ColorValue318 = new byte[3];
							byte[] in_ColorValue319 = new byte[3];
							byte[] in_ColorValue320 = new byte[3];
							Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, in_ColorValue316, in_ColorValue317, in_ColorValue318, in_ColorValue319, in_ColorValue320);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Panel_DRAM_Fury1 != null)
						{
							byte[] array247 = new byte[3];
							byte[] in_ColorValue321 = new byte[3];
							byte[] in_ColorValue322 = new byte[3];
							byte[] in_ColorValue323 = new byte[3];
							byte[] in_ColorValue324 = new byte[3];
							array247[0] = ColorPalette.GetCurrentRGB[0];
							array247[1] = ColorPalette.GetCurrentRGB[1];
							array247[2] = ColorPalette.GetCurrentRGB[2];
							Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, array247, in_ColorValue321, in_ColorValue322, in_ColorValue323, in_ColorValue324);
						}
					}
					else if (sOption == "FiveColor" && Panel_DRAM_Fury1 != null)
					{
						byte[] array248 = new byte[3];
						byte[] array249 = new byte[3];
						byte[] array250 = new byte[3];
						byte[] array251 = new byte[3];
						byte[] array252 = new byte[3];
						for (int num30 = 0; num30 < 5; num30++)
						{
							List<List<int>> colors36 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (num30)
							{
							case 0:
								array248[0] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(0)));
								array248[1] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(1)));
								array248[2] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(2)));
								break;
							case 1:
								array249[0] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(0)));
								array249[1] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(1)));
								array249[2] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(2)));
								break;
							case 2:
								array250[0] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(0)));
								array250[1] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(1)));
								array250[2] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(2)));
								break;
							case 3:
								array251[0] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(0)));
								array251[1] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(1)));
								array251[2] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(2)));
								break;
							case 4:
								array252[0] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(0)));
								array252[1] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(1)));
								array252[2] = Convert.ToByte(Convert.ToByte(colors36[num30].ElementAt(2)));
								break;
							}
						}
						Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, array248, array249, array250, array251, array252);
					}
					createdCount++;
					break;
				case 2:
					if (Panel_DRAM_Fury2 == null)
					{
						Panel_DRAM_Fury2 = new UC_Pannel_DRAM_Fury();
						Panel_DRAM_Fury2.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Panel_DRAM_Fury2.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin35 = default(Thickness);
						margin35.Top = 257.0;
						Panel_DRAM_Fury2.Margin = margin35;
						gr_DevicePanel_Childs.Children.Add(Panel_DRAM_Fury2);
					}
					if (sMode == "all_off")
					{
						byte[] array239 = new byte[1] { 125 };
						Panel_DRAM_Fury2.SetStyleEffect(sMode, sOption, array239, array239, array239, array239, array239);
					}
					else if (sOption == "DefaultColor")
					{
						if (Panel_DRAM_Fury2 != null)
						{
							byte[] in_ColorValue307 = new byte[3];
							byte[] in_ColorValue308 = new byte[3];
							byte[] in_ColorValue309 = new byte[3];
							byte[] in_ColorValue310 = new byte[3];
							byte[] in_ColorValue311 = new byte[3];
							Panel_DRAM_Fury2.SetStyleEffect(sMode, sOption, in_ColorValue307, in_ColorValue308, in_ColorValue309, in_ColorValue310, in_ColorValue311);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Panel_DRAM_Fury2 != null)
						{
							byte[] array240 = new byte[3];
							byte[] in_ColorValue312 = new byte[3];
							byte[] in_ColorValue313 = new byte[3];
							byte[] in_ColorValue314 = new byte[3];
							byte[] in_ColorValue315 = new byte[3];
							array240[0] = ColorPalette.GetCurrentRGB[0];
							array240[1] = ColorPalette.GetCurrentRGB[1];
							array240[2] = ColorPalette.GetCurrentRGB[2];
							Panel_DRAM_Fury2.SetStyleEffect(sMode, sOption, array240, in_ColorValue312, in_ColorValue313, in_ColorValue314, in_ColorValue315);
						}
					}
					else if (sOption == "FiveColor" && Panel_DRAM_Fury2 != null)
					{
						byte[] array241 = new byte[3];
						byte[] array242 = new byte[3];
						byte[] array243 = new byte[3];
						byte[] array244 = new byte[3];
						byte[] array245 = new byte[3];
						for (int num29 = 0; num29 < 5; num29++)
						{
							List<List<int>> colors35 = _DRAM_colors.ElementAt(2).Value.colors;
							switch (num29)
							{
							case 0:
								array241[0] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(0)));
								array241[1] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(1)));
								array241[2] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(2)));
								break;
							case 1:
								array242[0] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(0)));
								array242[1] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(1)));
								array242[2] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(2)));
								break;
							case 2:
								array243[0] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(0)));
								array243[1] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(1)));
								array243[2] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(2)));
								break;
							case 3:
								array244[0] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(0)));
								array244[1] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(1)));
								array244[2] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(2)));
								break;
							case 4:
								array245[0] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(0)));
								array245[1] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(1)));
								array245[2] = Convert.ToByte(Convert.ToByte(colors35[num29].ElementAt(2)));
								break;
							}
						}
						Panel_DRAM_Fury2.SetStyleEffect(sMode, sOption, array241, array242, array243, array244, array245);
					}
					createdCount++;
					break;
				}
				break;
			case 4:
				switch (createdCount)
				{
				case 0:
					if (Panel_DRAM_Fury0 == null)
					{
						Panel_DRAM_Fury0 = new UC_Pannel_DRAM_Fury();
						Panel_DRAM_Fury0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Panel_DRAM_Fury0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin32 = default(Thickness);
						margin32.Top = 0.0;
						Panel_DRAM_Fury0.Margin = margin32;
						gr_DevicePanel_Childs.Children.Add(Panel_DRAM_Fury0);
					}
					if (sMode == "all_off")
					{
						byte[] array218 = new byte[1] { 125 };
						Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array218, array218, array218, array218, array218);
					}
					else if (sOption == "DefaultColor")
					{
						if (Panel_DRAM_Fury0 != null)
						{
							byte[] in_ColorValue280 = new byte[3];
							byte[] in_ColorValue281 = new byte[3];
							byte[] in_ColorValue282 = new byte[3];
							byte[] in_ColorValue283 = new byte[3];
							byte[] in_ColorValue284 = new byte[3];
							Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, in_ColorValue280, in_ColorValue281, in_ColorValue282, in_ColorValue283, in_ColorValue284);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Panel_DRAM_Fury0 != null)
						{
							byte[] array219 = new byte[3];
							byte[] in_ColorValue285 = new byte[3];
							byte[] in_ColorValue286 = new byte[3];
							byte[] in_ColorValue287 = new byte[3];
							byte[] in_ColorValue288 = new byte[3];
							array219[0] = ColorPalette.GetCurrentRGB[0];
							array219[1] = ColorPalette.GetCurrentRGB[1];
							array219[2] = ColorPalette.GetCurrentRGB[2];
							Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array219, in_ColorValue285, in_ColorValue286, in_ColorValue287, in_ColorValue288);
						}
					}
					else if (sOption == "FiveColor" && Panel_DRAM_Fury0 != null)
					{
						byte[] array220 = new byte[3];
						byte[] array221 = new byte[3];
						byte[] array222 = new byte[3];
						byte[] array223 = new byte[3];
						byte[] array224 = new byte[3];
						for (int num26 = 0; num26 < 5; num26++)
						{
							List<List<int>> colors32 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num26)
							{
							case 0:
								array220[0] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(0)));
								array220[1] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(1)));
								array220[2] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(2)));
								break;
							case 1:
								array221[0] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(0)));
								array221[1] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(1)));
								array221[2] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(2)));
								break;
							case 2:
								array222[0] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(0)));
								array222[1] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(1)));
								array222[2] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(2)));
								break;
							case 3:
								array223[0] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(0)));
								array223[1] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(1)));
								array223[2] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(2)));
								break;
							case 4:
								array224[0] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(0)));
								array224[1] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(1)));
								array224[2] = Convert.ToByte(Convert.ToByte(colors32[num26].ElementAt(2)));
								break;
							}
						}
						Panel_DRAM_Fury0.SetStyleEffect(sMode, sOption, array220, array221, array222, array223, array224);
					}
					createdCount++;
					break;
				case 1:
					if (Panel_DRAM_Fury1 == null)
					{
						Panel_DRAM_Fury1 = new UC_Pannel_DRAM_Fury();
						Panel_DRAM_Fury1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Panel_DRAM_Fury1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin34 = default(Thickness);
						margin34.Top = 103.0;
						Panel_DRAM_Fury1.Margin = margin34;
						gr_DevicePanel_Childs.Children.Add(Panel_DRAM_Fury1);
					}
					if (sMode == "all_off")
					{
						byte[] array232 = new byte[1] { 125 };
						Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, array232, array232, array232, array232, array232);
					}
					else if (sOption == "DefaultColor")
					{
						if (Panel_DRAM_Fury1 != null)
						{
							byte[] in_ColorValue298 = new byte[3];
							byte[] in_ColorValue299 = new byte[3];
							byte[] in_ColorValue300 = new byte[3];
							byte[] in_ColorValue301 = new byte[3];
							byte[] in_ColorValue302 = new byte[3];
							Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, in_ColorValue298, in_ColorValue299, in_ColorValue300, in_ColorValue301, in_ColorValue302);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Panel_DRAM_Fury1 != null)
						{
							byte[] array233 = new byte[3];
							byte[] in_ColorValue303 = new byte[3];
							byte[] in_ColorValue304 = new byte[3];
							byte[] in_ColorValue305 = new byte[3];
							byte[] in_ColorValue306 = new byte[3];
							array233[0] = ColorPalette.GetCurrentRGB[0];
							array233[1] = ColorPalette.GetCurrentRGB[1];
							array233[2] = ColorPalette.GetCurrentRGB[2];
							Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, array233, in_ColorValue303, in_ColorValue304, in_ColorValue305, in_ColorValue306);
						}
					}
					else if (sOption == "FiveColor" && Panel_DRAM_Fury1 != null)
					{
						byte[] array234 = new byte[3];
						byte[] array235 = new byte[3];
						byte[] array236 = new byte[3];
						byte[] array237 = new byte[3];
						byte[] array238 = new byte[3];
						for (int num28 = 0; num28 < 5; num28++)
						{
							List<List<int>> colors34 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (num28)
							{
							case 0:
								array234[0] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(0)));
								array234[1] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(1)));
								array234[2] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(2)));
								break;
							case 1:
								array235[0] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(0)));
								array235[1] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(1)));
								array235[2] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(2)));
								break;
							case 2:
								array236[0] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(0)));
								array236[1] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(1)));
								array236[2] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(2)));
								break;
							case 3:
								array237[0] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(0)));
								array237[1] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(1)));
								array237[2] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(2)));
								break;
							case 4:
								array238[0] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(0)));
								array238[1] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(1)));
								array238[2] = Convert.ToByte(Convert.ToByte(colors34[num28].ElementAt(2)));
								break;
							}
						}
						Panel_DRAM_Fury1.SetStyleEffect(sMode, sOption, array234, array235, array236, array237, array238);
					}
					createdCount++;
					break;
				case 2:
					if (Panel_DRAM_Fury2 == null)
					{
						Panel_DRAM_Fury2 = new UC_Pannel_DRAM_Fury();
						Panel_DRAM_Fury2.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Panel_DRAM_Fury2.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin33 = default(Thickness);
						margin33.Top = 206.0;
						Panel_DRAM_Fury2.Margin = margin33;
						gr_DevicePanel_Childs.Children.Add(Panel_DRAM_Fury2);
					}
					if (sMode == "all_off")
					{
						byte[] array225 = new byte[1] { 125 };
						Panel_DRAM_Fury2.SetStyleEffect(sMode, sOption, array225, array225, array225, array225, array225);
					}
					else if (sOption == "DefaultColor")
					{
						if (Panel_DRAM_Fury2 != null)
						{
							byte[] in_ColorValue289 = new byte[3];
							byte[] in_ColorValue290 = new byte[3];
							byte[] in_ColorValue291 = new byte[3];
							byte[] in_ColorValue292 = new byte[3];
							byte[] in_ColorValue293 = new byte[3];
							Panel_DRAM_Fury2.SetStyleEffect(sMode, sOption, in_ColorValue289, in_ColorValue290, in_ColorValue291, in_ColorValue292, in_ColorValue293);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Panel_DRAM_Fury2 != null)
						{
							byte[] array226 = new byte[3];
							byte[] in_ColorValue294 = new byte[3];
							byte[] in_ColorValue295 = new byte[3];
							byte[] in_ColorValue296 = new byte[3];
							byte[] in_ColorValue297 = new byte[3];
							array226[0] = ColorPalette.GetCurrentRGB[0];
							array226[1] = ColorPalette.GetCurrentRGB[1];
							array226[2] = ColorPalette.GetCurrentRGB[2];
							Panel_DRAM_Fury2.SetStyleEffect(sMode, sOption, array226, in_ColorValue294, in_ColorValue295, in_ColorValue296, in_ColorValue297);
						}
					}
					else if (sOption == "FiveColor" && Panel_DRAM_Fury2 != null)
					{
						byte[] array227 = new byte[3];
						byte[] array228 = new byte[3];
						byte[] array229 = new byte[3];
						byte[] array230 = new byte[3];
						byte[] array231 = new byte[3];
						for (int num27 = 0; num27 < 5; num27++)
						{
							List<List<int>> colors33 = _DRAM_colors.ElementAt(2).Value.colors;
							switch (num27)
							{
							case 0:
								array227[0] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(0)));
								array227[1] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(1)));
								array227[2] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(2)));
								break;
							case 1:
								array228[0] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(0)));
								array228[1] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(1)));
								array228[2] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(2)));
								break;
							case 2:
								array229[0] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(0)));
								array229[1] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(1)));
								array229[2] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(2)));
								break;
							case 3:
								array230[0] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(0)));
								array230[1] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(1)));
								array230[2] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(2)));
								break;
							case 4:
								array231[0] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(0)));
								array231[1] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(1)));
								array231[2] = Convert.ToByte(Convert.ToByte(colors33[num27].ElementAt(2)));
								break;
							}
						}
						Panel_DRAM_Fury2.SetStyleEffect(sMode, sOption, array227, array228, array229, array230, array231);
					}
					createdCount++;
					break;
				case 3:
					if (Panel_DRAM_Fury3 == null)
					{
						Panel_DRAM_Fury3 = new UC_Pannel_DRAM_Fury();
						Panel_DRAM_Fury3.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Panel_DRAM_Fury3.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin31 = default(Thickness);
						margin31.Top = 309.0;
						Panel_DRAM_Fury3.Margin = margin31;
						gr_DevicePanel_Childs.Children.Add(Panel_DRAM_Fury3);
					}
					if (sMode == "all_off")
					{
						byte[] array211 = new byte[1] { 125 };
						Panel_DRAM_Fury3.SetStyleEffect(sMode, sOption, array211, array211, array211, array211, array211);
					}
					else if (sOption == "DefaultColor")
					{
						if (Panel_DRAM_Fury3 != null)
						{
							byte[] in_ColorValue271 = new byte[3];
							byte[] in_ColorValue272 = new byte[3];
							byte[] in_ColorValue273 = new byte[3];
							byte[] in_ColorValue274 = new byte[3];
							byte[] in_ColorValue275 = new byte[3];
							Panel_DRAM_Fury3.SetStyleEffect(sMode, sOption, in_ColorValue271, in_ColorValue272, in_ColorValue273, in_ColorValue274, in_ColorValue275);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Panel_DRAM_Fury3 != null)
						{
							byte[] array212 = new byte[3];
							byte[] in_ColorValue276 = new byte[3];
							byte[] in_ColorValue277 = new byte[3];
							byte[] in_ColorValue278 = new byte[3];
							byte[] in_ColorValue279 = new byte[3];
							array212[0] = ColorPalette.GetCurrentRGB[0];
							array212[1] = ColorPalette.GetCurrentRGB[1];
							array212[2] = ColorPalette.GetCurrentRGB[2];
							Panel_DRAM_Fury3.SetStyleEffect(sMode, sOption, array212, in_ColorValue276, in_ColorValue277, in_ColorValue278, in_ColorValue279);
						}
					}
					else if (sOption == "FiveColor" && Panel_DRAM_Fury3 != null)
					{
						byte[] array213 = new byte[3];
						byte[] array214 = new byte[3];
						byte[] array215 = new byte[3];
						byte[] array216 = new byte[3];
						byte[] array217 = new byte[3];
						for (int num25 = 0; num25 < 5; num25++)
						{
							List<List<int>> colors31 = _DRAM_colors.ElementAt(3).Value.colors;
							switch (num25)
							{
							case 0:
								array213[0] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(0)));
								array213[1] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(1)));
								array213[2] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(2)));
								break;
							case 1:
								array214[0] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(0)));
								array214[1] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(1)));
								array214[2] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(2)));
								break;
							case 2:
								array215[0] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(0)));
								array215[1] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(1)));
								array215[2] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(2)));
								break;
							case 3:
								array216[0] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(0)));
								array216[1] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(1)));
								array216[2] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(2)));
								break;
							case 4:
								array217[0] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(0)));
								array217[1] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(1)));
								array217[2] = Convert.ToByte(Convert.ToByte(colors31[num25].ElementAt(2)));
								break;
							}
						}
						Panel_DRAM_Fury3.SetStyleEffect(sMode, sOption, array213, array214, array215, array216, array217);
					}
					createdCount++;
					break;
				}
				break;
			}
			break;
		case "Beast":
			switch (deviceCount)
			{
			case 1:
				if (Pannel_DRAM_Beast0 == null)
				{
					Pannel_DRAM_Beast0 = new UC_Pannel_DRAM_Beast();
					Pannel_DRAM_Beast0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
					Pannel_DRAM_Beast0.VerticalAlignment = VerticalAlignment.Top;
					Thickness margin30 = default(Thickness);
					margin30.Top = 154.0;
					Pannel_DRAM_Beast0.Margin = margin30;
					gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Beast0);
				}
				if (sMode == "all_off")
				{
					byte[] array204 = new byte[1] { 125 };
					Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array204, array204, array204, array204, array204);
				}
				else if (sOption == "DefaultColor")
				{
					if (Pannel_DRAM_Beast0 != null)
					{
						byte[] in_ColorValue262 = new byte[3];
						byte[] in_ColorValue263 = new byte[3];
						byte[] in_ColorValue264 = new byte[3];
						byte[] in_ColorValue265 = new byte[3];
						byte[] in_ColorValue266 = new byte[3];
						Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, in_ColorValue262, in_ColorValue263, in_ColorValue264, in_ColorValue265, in_ColorValue266);
					}
				}
				else if (sOption == "OneColor")
				{
					if (Pannel_DRAM_Beast0 != null)
					{
						byte[] array205 = new byte[3];
						byte[] in_ColorValue267 = new byte[3];
						byte[] in_ColorValue268 = new byte[3];
						byte[] in_ColorValue269 = new byte[3];
						byte[] in_ColorValue270 = new byte[3];
						array205[0] = ColorPalette.GetCurrentRGB[0];
						array205[1] = ColorPalette.GetCurrentRGB[1];
						array205[2] = ColorPalette.GetCurrentRGB[2];
						Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array205, in_ColorValue267, in_ColorValue268, in_ColorValue269, in_ColorValue270);
					}
				}
				else if (sOption == "FiveColor" && Pannel_DRAM_Beast0 != null)
				{
					byte[] array206 = new byte[3];
					byte[] array207 = new byte[3];
					byte[] array208 = new byte[3];
					byte[] array209 = new byte[3];
					byte[] array210 = new byte[3];
					for (int num24 = 0; num24 < 5; num24++)
					{
						List<List<int>> colors30 = _DRAM_colors.ElementAt(0).Value.colors;
						switch (num24)
						{
						case 0:
							array206[0] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(0)));
							array206[1] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(1)));
							array206[2] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(2)));
							break;
						case 1:
							array207[0] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(0)));
							array207[1] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(1)));
							array207[2] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(2)));
							break;
						case 2:
							array208[0] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(0)));
							array208[1] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(1)));
							array208[2] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(2)));
							break;
						case 3:
							array209[0] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(0)));
							array209[1] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(1)));
							array209[2] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(2)));
							break;
						case 4:
							array210[0] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(0)));
							array210[1] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(1)));
							array210[2] = Convert.ToByte(Convert.ToByte(colors30[num24].ElementAt(2)));
							break;
						}
					}
					Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array206, array207, array208, array209, array210);
				}
				createdCount++;
				break;
			case 2:
				switch (createdCount)
				{
				case 0:
					if (Pannel_DRAM_Beast0 == null)
					{
						Pannel_DRAM_Beast0 = new UC_Pannel_DRAM_Beast();
						Pannel_DRAM_Beast0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Beast0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin29 = default(Thickness);
						margin29.Top = 84.0;
						Pannel_DRAM_Beast0.Margin = margin29;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Beast0);
					}
					if (sMode == "all_off")
					{
						byte[] array197 = new byte[1] { 125 };
						Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array197, array197, array197, array197, array197);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Beast0 != null)
						{
							byte[] in_ColorValue253 = new byte[3];
							byte[] in_ColorValue254 = new byte[3];
							byte[] in_ColorValue255 = new byte[3];
							byte[] in_ColorValue256 = new byte[3];
							byte[] in_ColorValue257 = new byte[3];
							Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, in_ColorValue253, in_ColorValue254, in_ColorValue255, in_ColorValue256, in_ColorValue257);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Beast0 != null)
						{
							byte[] array198 = new byte[3];
							byte[] in_ColorValue258 = new byte[3];
							byte[] in_ColorValue259 = new byte[3];
							byte[] in_ColorValue260 = new byte[3];
							byte[] in_ColorValue261 = new byte[3];
							array198[0] = ColorPalette.GetCurrentRGB[0];
							array198[1] = ColorPalette.GetCurrentRGB[1];
							array198[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array198, in_ColorValue258, in_ColorValue259, in_ColorValue260, in_ColorValue261);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Beast0 != null)
					{
						byte[] array199 = new byte[3];
						byte[] array200 = new byte[3];
						byte[] array201 = new byte[3];
						byte[] array202 = new byte[3];
						byte[] array203 = new byte[3];
						for (int num23 = 0; num23 < 5; num23++)
						{
							List<List<int>> colors29 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num23)
							{
							case 0:
								array199[0] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(0)));
								array199[1] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(1)));
								array199[2] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(2)));
								break;
							case 1:
								array200[0] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(0)));
								array200[1] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(1)));
								array200[2] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(2)));
								break;
							case 2:
								array201[0] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(0)));
								array201[1] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(1)));
								array201[2] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(2)));
								break;
							case 3:
								array202[0] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(0)));
								array202[1] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(1)));
								array202[2] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(2)));
								break;
							case 4:
								array203[0] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(0)));
								array203[1] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(1)));
								array203[2] = Convert.ToByte(Convert.ToByte(colors29[num23].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array199, array200, array201, array202, array203);
					}
					createdCount++;
					break;
				case 1:
					if (Pannel_DRAM_Beast1 == null)
					{
						Pannel_DRAM_Beast1 = new UC_Pannel_DRAM_Beast();
						Pannel_DRAM_Beast1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Beast1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin28 = default(Thickness);
						margin28.Top = 225.0;
						Pannel_DRAM_Beast1.Margin = margin28;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Beast1);
					}
					if (sMode == "all_off")
					{
						byte[] array190 = new byte[1] { 125 };
						Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, array190, array190, array190, array190, array190);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Beast1 != null)
						{
							byte[] in_ColorValue244 = new byte[3];
							byte[] in_ColorValue245 = new byte[3];
							byte[] in_ColorValue246 = new byte[3];
							byte[] in_ColorValue247 = new byte[3];
							byte[] in_ColorValue248 = new byte[3];
							Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, in_ColorValue244, in_ColorValue245, in_ColorValue246, in_ColorValue247, in_ColorValue248);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Beast1 != null)
						{
							byte[] array191 = new byte[3];
							byte[] in_ColorValue249 = new byte[3];
							byte[] in_ColorValue250 = new byte[3];
							byte[] in_ColorValue251 = new byte[3];
							byte[] in_ColorValue252 = new byte[3];
							array191[0] = ColorPalette.GetCurrentRGB[0];
							array191[1] = ColorPalette.GetCurrentRGB[1];
							array191[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, array191, in_ColorValue249, in_ColorValue250, in_ColorValue251, in_ColorValue252);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Beast1 != null)
					{
						byte[] array192 = new byte[3];
						byte[] array193 = new byte[3];
						byte[] array194 = new byte[3];
						byte[] array195 = new byte[3];
						byte[] array196 = new byte[3];
						for (int num22 = 0; num22 < 5; num22++)
						{
							List<List<int>> colors28 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (num22)
							{
							case 0:
								array192[0] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(0)));
								array192[1] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(1)));
								array192[2] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(2)));
								break;
							case 1:
								array193[0] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(0)));
								array193[1] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(1)));
								array193[2] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(2)));
								break;
							case 2:
								array194[0] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(0)));
								array194[1] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(1)));
								array194[2] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(2)));
								break;
							case 3:
								array195[0] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(0)));
								array195[1] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(1)));
								array195[2] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(2)));
								break;
							case 4:
								array196[0] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(0)));
								array196[1] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(1)));
								array196[2] = Convert.ToByte(Convert.ToByte(colors28[num22].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, array192, array193, array194, array195, array196);
					}
					createdCount++;
					break;
				}
				break;
			case 3:
				switch (createdCount)
				{
				case 0:
					if (Pannel_DRAM_Beast0 == null)
					{
						Pannel_DRAM_Beast0 = new UC_Pannel_DRAM_Beast();
						Pannel_DRAM_Beast0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Beast0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin27 = default(Thickness);
						margin27.Top = 52.0;
						Pannel_DRAM_Beast0.Margin = margin27;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Beast0);
					}
					if (sMode == "all_off")
					{
						byte[] array183 = new byte[1] { 125 };
						Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array183, array183, array183, array183, array183);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Beast0 != null)
						{
							byte[] in_ColorValue235 = new byte[3];
							byte[] in_ColorValue236 = new byte[3];
							byte[] in_ColorValue237 = new byte[3];
							byte[] in_ColorValue238 = new byte[3];
							byte[] in_ColorValue239 = new byte[3];
							Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, in_ColorValue235, in_ColorValue236, in_ColorValue237, in_ColorValue238, in_ColorValue239);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Beast0 != null)
						{
							byte[] array184 = new byte[3];
							byte[] in_ColorValue240 = new byte[3];
							byte[] in_ColorValue241 = new byte[3];
							byte[] in_ColorValue242 = new byte[3];
							byte[] in_ColorValue243 = new byte[3];
							array184[0] = ColorPalette.GetCurrentRGB[0];
							array184[1] = ColorPalette.GetCurrentRGB[1];
							array184[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array184, in_ColorValue240, in_ColorValue241, in_ColorValue242, in_ColorValue243);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Beast0 != null)
					{
						byte[] array185 = new byte[3];
						byte[] array186 = new byte[3];
						byte[] array187 = new byte[3];
						byte[] array188 = new byte[3];
						byte[] array189 = new byte[3];
						for (int num21 = 0; num21 < 5; num21++)
						{
							List<List<int>> colors27 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num21)
							{
							case 0:
								array185[0] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(0)));
								array185[1] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(1)));
								array185[2] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(2)));
								break;
							case 1:
								array186[0] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(0)));
								array186[1] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(1)));
								array186[2] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(2)));
								break;
							case 2:
								array187[0] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(0)));
								array187[1] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(1)));
								array187[2] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(2)));
								break;
							case 3:
								array188[0] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(0)));
								array188[1] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(1)));
								array188[2] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(2)));
								break;
							case 4:
								array189[0] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(0)));
								array189[1] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(1)));
								array189[2] = Convert.ToByte(Convert.ToByte(colors27[num21].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array185, array186, array187, array188, array189);
					}
					createdCount++;
					break;
				case 1:
					if (Pannel_DRAM_Beast1 == null)
					{
						Pannel_DRAM_Beast1 = new UC_Pannel_DRAM_Beast();
						Pannel_DRAM_Beast1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Beast1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin26 = default(Thickness);
						margin26.Top = 155.0;
						Pannel_DRAM_Beast1.Margin = margin26;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Beast1);
					}
					if (sMode == "all_off")
					{
						byte[] array176 = new byte[1] { 125 };
						Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, array176, array176, array176, array176, array176);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Beast1 != null)
						{
							byte[] in_ColorValue226 = new byte[3];
							byte[] in_ColorValue227 = new byte[3];
							byte[] in_ColorValue228 = new byte[3];
							byte[] in_ColorValue229 = new byte[3];
							byte[] in_ColorValue230 = new byte[3];
							Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, in_ColorValue226, in_ColorValue227, in_ColorValue228, in_ColorValue229, in_ColorValue230);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Beast1 != null)
						{
							byte[] array177 = new byte[3];
							byte[] in_ColorValue231 = new byte[3];
							byte[] in_ColorValue232 = new byte[3];
							byte[] in_ColorValue233 = new byte[3];
							byte[] in_ColorValue234 = new byte[3];
							array177[0] = ColorPalette.GetCurrentRGB[0];
							array177[1] = ColorPalette.GetCurrentRGB[1];
							array177[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, array177, in_ColorValue231, in_ColorValue232, in_ColorValue233, in_ColorValue234);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Beast1 != null)
					{
						byte[] array178 = new byte[3];
						byte[] array179 = new byte[3];
						byte[] array180 = new byte[3];
						byte[] array181 = new byte[3];
						byte[] array182 = new byte[3];
						for (int num20 = 0; num20 < 5; num20++)
						{
							List<List<int>> colors26 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (num20)
							{
							case 0:
								array178[0] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(0)));
								array178[1] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(1)));
								array178[2] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(2)));
								break;
							case 1:
								array179[0] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(0)));
								array179[1] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(1)));
								array179[2] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(2)));
								break;
							case 2:
								array180[0] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(0)));
								array180[1] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(1)));
								array180[2] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(2)));
								break;
							case 3:
								array181[0] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(0)));
								array181[1] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(1)));
								array181[2] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(2)));
								break;
							case 4:
								array182[0] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(0)));
								array182[1] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(1)));
								array182[2] = Convert.ToByte(Convert.ToByte(colors26[num20].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, array178, array179, array180, array181, array182);
					}
					createdCount++;
					break;
				case 2:
					if (Pannel_DRAM_Beast2 == null)
					{
						Pannel_DRAM_Beast2 = new UC_Pannel_DRAM_Beast();
						Pannel_DRAM_Beast2.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Beast2.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin25 = default(Thickness);
						margin25.Top = 257.0;
						Pannel_DRAM_Beast2.Margin = margin25;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Beast2);
					}
					if (sMode == "all_off")
					{
						byte[] array169 = new byte[1] { 125 };
						Pannel_DRAM_Beast2.SetStyleEffect(sMode, sOption, array169, array169, array169, array169, array169);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Beast2 != null)
						{
							byte[] in_ColorValue217 = new byte[3];
							byte[] in_ColorValue218 = new byte[3];
							byte[] in_ColorValue219 = new byte[3];
							byte[] in_ColorValue220 = new byte[3];
							byte[] in_ColorValue221 = new byte[3];
							Pannel_DRAM_Beast2.SetStyleEffect(sMode, sOption, in_ColorValue217, in_ColorValue218, in_ColorValue219, in_ColorValue220, in_ColorValue221);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Beast2 != null)
						{
							byte[] array170 = new byte[3];
							byte[] in_ColorValue222 = new byte[3];
							byte[] in_ColorValue223 = new byte[3];
							byte[] in_ColorValue224 = new byte[3];
							byte[] in_ColorValue225 = new byte[3];
							array170[0] = ColorPalette.GetCurrentRGB[0];
							array170[1] = ColorPalette.GetCurrentRGB[1];
							array170[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Beast2.SetStyleEffect(sMode, sOption, array170, in_ColorValue222, in_ColorValue223, in_ColorValue224, in_ColorValue225);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Beast2 != null)
					{
						byte[] array171 = new byte[3];
						byte[] array172 = new byte[3];
						byte[] array173 = new byte[3];
						byte[] array174 = new byte[3];
						byte[] array175 = new byte[3];
						for (int num19 = 0; num19 < 5; num19++)
						{
							List<List<int>> colors25 = _DRAM_colors.ElementAt(2).Value.colors;
							switch (num19)
							{
							case 0:
								array171[0] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(0)));
								array171[1] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(1)));
								array171[2] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(2)));
								break;
							case 1:
								array172[0] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(0)));
								array172[1] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(1)));
								array172[2] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(2)));
								break;
							case 2:
								array173[0] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(0)));
								array173[1] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(1)));
								array173[2] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(2)));
								break;
							case 3:
								array174[0] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(0)));
								array174[1] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(1)));
								array174[2] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(2)));
								break;
							case 4:
								array175[0] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(0)));
								array175[1] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(1)));
								array175[2] = Convert.ToByte(Convert.ToByte(colors25[num19].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Beast2.SetStyleEffect(sMode, sOption, array171, array172, array173, array174, array175);
					}
					createdCount++;
					break;
				}
				break;
			case 4:
				switch (createdCount)
				{
				case 0:
					if (Pannel_DRAM_Beast0 == null)
					{
						Pannel_DRAM_Beast0 = new UC_Pannel_DRAM_Beast();
						Pannel_DRAM_Beast0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Beast0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin22 = default(Thickness);
						margin22.Top = 0.0;
						Pannel_DRAM_Beast0.Margin = margin22;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Beast0);
					}
					if (sMode == "all_off")
					{
						byte[] array148 = new byte[1] { 125 };
						Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array148, array148, array148, array148, array148);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Beast0 != null)
						{
							byte[] in_ColorValue190 = new byte[3];
							byte[] in_ColorValue191 = new byte[3];
							byte[] in_ColorValue192 = new byte[3];
							byte[] in_ColorValue193 = new byte[3];
							byte[] in_ColorValue194 = new byte[3];
							Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, in_ColorValue190, in_ColorValue191, in_ColorValue192, in_ColorValue193, in_ColorValue194);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Beast0 != null)
						{
							byte[] array149 = new byte[3];
							byte[] in_ColorValue195 = new byte[3];
							byte[] in_ColorValue196 = new byte[3];
							byte[] in_ColorValue197 = new byte[3];
							byte[] in_ColorValue198 = new byte[3];
							array149[0] = ColorPalette.GetCurrentRGB[0];
							array149[1] = ColorPalette.GetCurrentRGB[1];
							array149[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array149, in_ColorValue195, in_ColorValue196, in_ColorValue197, in_ColorValue198);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Beast0 != null)
					{
						byte[] array150 = new byte[3];
						byte[] array151 = new byte[3];
						byte[] array152 = new byte[3];
						byte[] array153 = new byte[3];
						byte[] array154 = new byte[3];
						for (int num16 = 0; num16 < 5; num16++)
						{
							List<List<int>> colors22 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num16)
							{
							case 0:
								array150[0] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(0)));
								array150[1] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(1)));
								array150[2] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(2)));
								break;
							case 1:
								array151[0] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(0)));
								array151[1] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(1)));
								array151[2] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(2)));
								break;
							case 2:
								array152[0] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(0)));
								array152[1] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(1)));
								array152[2] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(2)));
								break;
							case 3:
								array153[0] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(0)));
								array153[1] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(1)));
								array153[2] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(2)));
								break;
							case 4:
								array154[0] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(0)));
								array154[1] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(1)));
								array154[2] = Convert.ToByte(Convert.ToByte(colors22[num16].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Beast0.SetStyleEffect(sMode, sOption, array150, array151, array152, array153, array154);
					}
					createdCount++;
					break;
				case 1:
					if (Pannel_DRAM_Beast1 == null)
					{
						Pannel_DRAM_Beast1 = new UC_Pannel_DRAM_Beast();
						Pannel_DRAM_Beast1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Beast1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin24 = default(Thickness);
						margin24.Top = 103.0;
						Pannel_DRAM_Beast1.Margin = margin24;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Beast1);
					}
					if (sMode == "all_off")
					{
						byte[] array162 = new byte[1] { 125 };
						Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, array162, array162, array162, array162, array162);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Beast1 != null)
						{
							byte[] in_ColorValue208 = new byte[3];
							byte[] in_ColorValue209 = new byte[3];
							byte[] in_ColorValue210 = new byte[3];
							byte[] in_ColorValue211 = new byte[3];
							byte[] in_ColorValue212 = new byte[3];
							Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, in_ColorValue208, in_ColorValue209, in_ColorValue210, in_ColorValue211, in_ColorValue212);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Beast1 != null)
						{
							byte[] array163 = new byte[3];
							byte[] in_ColorValue213 = new byte[3];
							byte[] in_ColorValue214 = new byte[3];
							byte[] in_ColorValue215 = new byte[3];
							byte[] in_ColorValue216 = new byte[3];
							array163[0] = ColorPalette.GetCurrentRGB[0];
							array163[1] = ColorPalette.GetCurrentRGB[1];
							array163[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, array163, in_ColorValue213, in_ColorValue214, in_ColorValue215, in_ColorValue216);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Beast1 != null)
					{
						byte[] array164 = new byte[3];
						byte[] array165 = new byte[3];
						byte[] array166 = new byte[3];
						byte[] array167 = new byte[3];
						byte[] array168 = new byte[3];
						for (int num18 = 0; num18 < 5; num18++)
						{
							List<List<int>> colors24 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (num18)
							{
							case 0:
								array164[0] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(0)));
								array164[1] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(1)));
								array164[2] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(2)));
								break;
							case 1:
								array165[0] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(0)));
								array165[1] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(1)));
								array165[2] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(2)));
								break;
							case 2:
								array166[0] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(0)));
								array166[1] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(1)));
								array166[2] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(2)));
								break;
							case 3:
								array167[0] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(0)));
								array167[1] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(1)));
								array167[2] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(2)));
								break;
							case 4:
								array168[0] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(0)));
								array168[1] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(1)));
								array168[2] = Convert.ToByte(Convert.ToByte(colors24[num18].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Beast1.SetStyleEffect(sMode, sOption, array164, array165, array166, array167, array168);
					}
					createdCount++;
					break;
				case 2:
					if (Pannel_DRAM_Beast2 == null)
					{
						Pannel_DRAM_Beast2 = new UC_Pannel_DRAM_Beast();
						Pannel_DRAM_Beast2.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Beast2.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin23 = default(Thickness);
						margin23.Top = 206.0;
						Pannel_DRAM_Beast2.Margin = margin23;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Beast2);
					}
					if (sMode == "all_off")
					{
						byte[] array155 = new byte[1] { 125 };
						Pannel_DRAM_Beast2.SetStyleEffect(sMode, sOption, array155, array155, array155, array155, array155);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Beast2 != null)
						{
							byte[] in_ColorValue199 = new byte[3];
							byte[] in_ColorValue200 = new byte[3];
							byte[] in_ColorValue201 = new byte[3];
							byte[] in_ColorValue202 = new byte[3];
							byte[] in_ColorValue203 = new byte[3];
							Pannel_DRAM_Beast2.SetStyleEffect(sMode, sOption, in_ColorValue199, in_ColorValue200, in_ColorValue201, in_ColorValue202, in_ColorValue203);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Beast2 != null)
						{
							byte[] array156 = new byte[3];
							byte[] in_ColorValue204 = new byte[3];
							byte[] in_ColorValue205 = new byte[3];
							byte[] in_ColorValue206 = new byte[3];
							byte[] in_ColorValue207 = new byte[3];
							array156[0] = ColorPalette.GetCurrentRGB[0];
							array156[1] = ColorPalette.GetCurrentRGB[1];
							array156[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Beast2.SetStyleEffect(sMode, sOption, array156, in_ColorValue204, in_ColorValue205, in_ColorValue206, in_ColorValue207);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Beast2 != null)
					{
						byte[] array157 = new byte[3];
						byte[] array158 = new byte[3];
						byte[] array159 = new byte[3];
						byte[] array160 = new byte[3];
						byte[] array161 = new byte[3];
						for (int num17 = 0; num17 < 5; num17++)
						{
							List<List<int>> colors23 = _DRAM_colors.ElementAt(2).Value.colors;
							switch (num17)
							{
							case 0:
								array157[0] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(0)));
								array157[1] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(1)));
								array157[2] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(2)));
								break;
							case 1:
								array158[0] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(0)));
								array158[1] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(1)));
								array158[2] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(2)));
								break;
							case 2:
								array159[0] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(0)));
								array159[1] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(1)));
								array159[2] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(2)));
								break;
							case 3:
								array160[0] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(0)));
								array160[1] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(1)));
								array160[2] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(2)));
								break;
							case 4:
								array161[0] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(0)));
								array161[1] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(1)));
								array161[2] = Convert.ToByte(Convert.ToByte(colors23[num17].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Beast2.SetStyleEffect(sMode, sOption, array157, array158, array159, array160, array161);
					}
					createdCount++;
					break;
				case 3:
					if (Pannel_DRAM_Beast3 == null)
					{
						Pannel_DRAM_Beast3 = new UC_Pannel_DRAM_Beast();
						Pannel_DRAM_Beast3.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Beast3.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin21 = default(Thickness);
						margin21.Top = 309.0;
						Pannel_DRAM_Beast3.Margin = margin21;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Beast3);
					}
					if (sMode == "all_off")
					{
						byte[] array141 = new byte[1] { 125 };
						Pannel_DRAM_Beast3.SetStyleEffect(sMode, sOption, array141, array141, array141, array141, array141);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Beast3 != null)
						{
							byte[] in_ColorValue181 = new byte[3];
							byte[] in_ColorValue182 = new byte[3];
							byte[] in_ColorValue183 = new byte[3];
							byte[] in_ColorValue184 = new byte[3];
							byte[] in_ColorValue185 = new byte[3];
							Pannel_DRAM_Beast3.SetStyleEffect(sMode, sOption, in_ColorValue181, in_ColorValue182, in_ColorValue183, in_ColorValue184, in_ColorValue185);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Beast3 != null)
						{
							byte[] array142 = new byte[3];
							byte[] in_ColorValue186 = new byte[3];
							byte[] in_ColorValue187 = new byte[3];
							byte[] in_ColorValue188 = new byte[3];
							byte[] in_ColorValue189 = new byte[3];
							array142[0] = ColorPalette.GetCurrentRGB[0];
							array142[1] = ColorPalette.GetCurrentRGB[1];
							array142[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Beast3.SetStyleEffect(sMode, sOption, array142, in_ColorValue186, in_ColorValue187, in_ColorValue188, in_ColorValue189);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Beast3 != null)
					{
						byte[] array143 = new byte[3];
						byte[] array144 = new byte[3];
						byte[] array145 = new byte[3];
						byte[] array146 = new byte[3];
						byte[] array147 = new byte[3];
						for (int num15 = 0; num15 < 5; num15++)
						{
							List<List<int>> colors21 = _DRAM_colors.ElementAt(3).Value.colors;
							switch (num15)
							{
							case 0:
								array143[0] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(0)));
								array143[1] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(1)));
								array143[2] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(2)));
								break;
							case 1:
								array144[0] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(0)));
								array144[1] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(1)));
								array144[2] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(2)));
								break;
							case 2:
								array145[0] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(0)));
								array145[1] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(1)));
								array145[2] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(2)));
								break;
							case 3:
								array146[0] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(0)));
								array146[1] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(1)));
								array146[2] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(2)));
								break;
							case 4:
								array147[0] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(0)));
								array147[1] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(1)));
								array147[2] = Convert.ToByte(Convert.ToByte(colors21[num15].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Beast3.SetStyleEffect(sMode, sOption, array143, array144, array145, array146, array147);
					}
					createdCount++;
					break;
				}
				break;
			}
			break;
		case "Predator":
			switch (deviceCount)
			{
			case 1:
				if (Pannel_DRAM_Predator0 == null)
				{
					Pannel_DRAM_Predator0 = new UC_Pannel_DRAM_Predator();
					Pannel_DRAM_Predator0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
					Pannel_DRAM_Predator0.VerticalAlignment = VerticalAlignment.Top;
					Thickness margin20 = default(Thickness);
					margin20.Top = 154.0;
					Pannel_DRAM_Predator0.Margin = margin20;
					gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Predator0);
				}
				if (sMode == "all_off")
				{
					byte[] array134 = new byte[1] { 125 };
					Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array134, array134, array134, array134, array134);
				}
				else if (sOption == "DefaultColor")
				{
					if (Pannel_DRAM_Predator0 != null)
					{
						byte[] in_ColorValue172 = new byte[3];
						byte[] in_ColorValue173 = new byte[3];
						byte[] in_ColorValue174 = new byte[3];
						byte[] in_ColorValue175 = new byte[3];
						byte[] in_ColorValue176 = new byte[3];
						Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, in_ColorValue172, in_ColorValue173, in_ColorValue174, in_ColorValue175, in_ColorValue176);
					}
				}
				else if (sOption == "OneColor")
				{
					if (Pannel_DRAM_Predator0 != null)
					{
						byte[] array135 = new byte[3];
						byte[] in_ColorValue177 = new byte[3];
						byte[] in_ColorValue178 = new byte[3];
						byte[] in_ColorValue179 = new byte[3];
						byte[] in_ColorValue180 = new byte[3];
						array135[0] = ColorPalette.GetCurrentRGB[0];
						array135[1] = ColorPalette.GetCurrentRGB[1];
						array135[2] = ColorPalette.GetCurrentRGB[2];
						Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array135, in_ColorValue177, in_ColorValue178, in_ColorValue179, in_ColorValue180);
					}
				}
				else if (sOption == "FiveColor" && Pannel_DRAM_Predator0 != null)
				{
					byte[] array136 = new byte[3];
					byte[] array137 = new byte[3];
					byte[] array138 = new byte[3];
					byte[] array139 = new byte[3];
					byte[] array140 = new byte[3];
					for (int num14 = 0; num14 < 5; num14++)
					{
						List<List<int>> colors20 = _DRAM_colors.ElementAt(0).Value.colors;
						switch (num14)
						{
						case 0:
							array136[0] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(0)));
							array136[1] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(1)));
							array136[2] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(2)));
							break;
						case 1:
							array137[0] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(0)));
							array137[1] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(1)));
							array137[2] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(2)));
							break;
						case 2:
							array138[0] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(0)));
							array138[1] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(1)));
							array138[2] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(2)));
							break;
						case 3:
							array139[0] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(0)));
							array139[1] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(1)));
							array139[2] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(2)));
							break;
						case 4:
							array140[0] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(0)));
							array140[1] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(1)));
							array140[2] = Convert.ToByte(Convert.ToByte(colors20[num14].ElementAt(2)));
							break;
						}
					}
					Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array136, array137, array138, array139, array140);
				}
				createdCount++;
				break;
			case 2:
				switch (createdCount)
				{
				case 0:
					if (Pannel_DRAM_Predator0 == null)
					{
						Pannel_DRAM_Predator0 = new UC_Pannel_DRAM_Predator();
						Pannel_DRAM_Predator0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Predator0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin19 = default(Thickness);
						margin19.Top = 84.0;
						Pannel_DRAM_Predator0.Margin = margin19;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Predator0);
					}
					if (sMode == "all_off")
					{
						byte[] array127 = new byte[1] { 125 };
						Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array127, array127, array127, array127, array127);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Predator0 != null)
						{
							byte[] in_ColorValue163 = new byte[3];
							byte[] in_ColorValue164 = new byte[3];
							byte[] in_ColorValue165 = new byte[3];
							byte[] in_ColorValue166 = new byte[3];
							byte[] in_ColorValue167 = new byte[3];
							Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, in_ColorValue163, in_ColorValue164, in_ColorValue165, in_ColorValue166, in_ColorValue167);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Predator0 != null)
						{
							byte[] array128 = new byte[3];
							byte[] in_ColorValue168 = new byte[3];
							byte[] in_ColorValue169 = new byte[3];
							byte[] in_ColorValue170 = new byte[3];
							byte[] in_ColorValue171 = new byte[3];
							array128[0] = ColorPalette.GetCurrentRGB[0];
							array128[1] = ColorPalette.GetCurrentRGB[1];
							array128[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array128, in_ColorValue168, in_ColorValue169, in_ColorValue170, in_ColorValue171);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Predator0 != null)
					{
						byte[] array129 = new byte[3];
						byte[] array130 = new byte[3];
						byte[] array131 = new byte[3];
						byte[] array132 = new byte[3];
						byte[] array133 = new byte[3];
						for (int num13 = 0; num13 < 5; num13++)
						{
							List<List<int>> colors19 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num13)
							{
							case 0:
								array129[0] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(0)));
								array129[1] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(1)));
								array129[2] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(2)));
								break;
							case 1:
								array130[0] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(0)));
								array130[1] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(1)));
								array130[2] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(2)));
								break;
							case 2:
								array131[0] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(0)));
								array131[1] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(1)));
								array131[2] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(2)));
								break;
							case 3:
								array132[0] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(0)));
								array132[1] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(1)));
								array132[2] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(2)));
								break;
							case 4:
								array133[0] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(0)));
								array133[1] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(1)));
								array133[2] = Convert.ToByte(Convert.ToByte(colors19[num13].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array129, array130, array131, array132, array133);
					}
					createdCount++;
					break;
				case 1:
					if (Pannel_DRAM_Predator1 == null)
					{
						Pannel_DRAM_Predator1 = new UC_Pannel_DRAM_Predator();
						Pannel_DRAM_Predator1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Predator1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin18 = default(Thickness);
						margin18.Top = 225.0;
						Pannel_DRAM_Predator1.Margin = margin18;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Predator1);
					}
					if (sMode == "all_off")
					{
						byte[] array120 = new byte[1] { 125 };
						Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, array120, array120, array120, array120, array120);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Predator1 != null)
						{
							byte[] in_ColorValue154 = new byte[3];
							byte[] in_ColorValue155 = new byte[3];
							byte[] in_ColorValue156 = new byte[3];
							byte[] in_ColorValue157 = new byte[3];
							byte[] in_ColorValue158 = new byte[3];
							Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, in_ColorValue154, in_ColorValue155, in_ColorValue156, in_ColorValue157, in_ColorValue158);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Predator1 != null)
						{
							byte[] array121 = new byte[3];
							byte[] in_ColorValue159 = new byte[3];
							byte[] in_ColorValue160 = new byte[3];
							byte[] in_ColorValue161 = new byte[3];
							byte[] in_ColorValue162 = new byte[3];
							array121[0] = ColorPalette.GetCurrentRGB[0];
							array121[1] = ColorPalette.GetCurrentRGB[1];
							array121[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, array121, in_ColorValue159, in_ColorValue160, in_ColorValue161, in_ColorValue162);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Predator1 != null)
					{
						byte[] array122 = new byte[3];
						byte[] array123 = new byte[3];
						byte[] array124 = new byte[3];
						byte[] array125 = new byte[3];
						byte[] array126 = new byte[3];
						for (int num12 = 0; num12 < 5; num12++)
						{
							List<List<int>> colors18 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (num12)
							{
							case 0:
								array122[0] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(0)));
								array122[1] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(1)));
								array122[2] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(2)));
								break;
							case 1:
								array123[0] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(0)));
								array123[1] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(1)));
								array123[2] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(2)));
								break;
							case 2:
								array124[0] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(0)));
								array124[1] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(1)));
								array124[2] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(2)));
								break;
							case 3:
								array125[0] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(0)));
								array125[1] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(1)));
								array125[2] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(2)));
								break;
							case 4:
								array126[0] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(0)));
								array126[1] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(1)));
								array126[2] = Convert.ToByte(Convert.ToByte(colors18[num12].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, array122, array123, array124, array125, array126);
					}
					createdCount++;
					break;
				}
				break;
			case 3:
				switch (createdCount)
				{
				case 0:
					if (Pannel_DRAM_Predator0 == null)
					{
						Pannel_DRAM_Predator0 = new UC_Pannel_DRAM_Predator();
						Pannel_DRAM_Predator0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Predator0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin17 = default(Thickness);
						margin17.Top = 52.0;
						Pannel_DRAM_Predator0.Margin = margin17;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Predator0);
					}
					if (sMode == "all_off")
					{
						byte[] array113 = new byte[1] { 125 };
						Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array113, array113, array113, array113, array113);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Predator0 != null)
						{
							byte[] in_ColorValue145 = new byte[3];
							byte[] in_ColorValue146 = new byte[3];
							byte[] in_ColorValue147 = new byte[3];
							byte[] in_ColorValue148 = new byte[3];
							byte[] in_ColorValue149 = new byte[3];
							Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, in_ColorValue145, in_ColorValue146, in_ColorValue147, in_ColorValue148, in_ColorValue149);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Predator0 != null)
						{
							byte[] array114 = new byte[3];
							byte[] in_ColorValue150 = new byte[3];
							byte[] in_ColorValue151 = new byte[3];
							byte[] in_ColorValue152 = new byte[3];
							byte[] in_ColorValue153 = new byte[3];
							array114[0] = ColorPalette.GetCurrentRGB[0];
							array114[1] = ColorPalette.GetCurrentRGB[1];
							array114[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array114, in_ColorValue150, in_ColorValue151, in_ColorValue152, in_ColorValue153);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Predator0 != null)
					{
						byte[] array115 = new byte[3];
						byte[] array116 = new byte[3];
						byte[] array117 = new byte[3];
						byte[] array118 = new byte[3];
						byte[] array119 = new byte[3];
						for (int num11 = 0; num11 < 5; num11++)
						{
							List<List<int>> colors17 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num11)
							{
							case 0:
								array115[0] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(0)));
								array115[1] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(1)));
								array115[2] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(2)));
								break;
							case 1:
								array116[0] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(0)));
								array116[1] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(1)));
								array116[2] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(2)));
								break;
							case 2:
								array117[0] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(0)));
								array117[1] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(1)));
								array117[2] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(2)));
								break;
							case 3:
								array118[0] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(0)));
								array118[1] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(1)));
								array118[2] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(2)));
								break;
							case 4:
								array119[0] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(0)));
								array119[1] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(1)));
								array119[2] = Convert.ToByte(Convert.ToByte(colors17[num11].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array115, array116, array117, array118, array119);
					}
					createdCount++;
					break;
				case 1:
					if (Pannel_DRAM_Predator1 == null)
					{
						Pannel_DRAM_Predator1 = new UC_Pannel_DRAM_Predator();
						Pannel_DRAM_Predator1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Predator1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin16 = default(Thickness);
						margin16.Top = 155.0;
						Pannel_DRAM_Predator1.Margin = margin16;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Predator1);
					}
					if (sMode == "all_off")
					{
						byte[] array106 = new byte[1] { 125 };
						Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, array106, array106, array106, array106, array106);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Predator1 != null)
						{
							byte[] in_ColorValue136 = new byte[3];
							byte[] in_ColorValue137 = new byte[3];
							byte[] in_ColorValue138 = new byte[3];
							byte[] in_ColorValue139 = new byte[3];
							byte[] in_ColorValue140 = new byte[3];
							Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, in_ColorValue136, in_ColorValue137, in_ColorValue138, in_ColorValue139, in_ColorValue140);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Predator1 != null)
						{
							byte[] array107 = new byte[3];
							byte[] in_ColorValue141 = new byte[3];
							byte[] in_ColorValue142 = new byte[3];
							byte[] in_ColorValue143 = new byte[3];
							byte[] in_ColorValue144 = new byte[3];
							array107[0] = ColorPalette.GetCurrentRGB[0];
							array107[1] = ColorPalette.GetCurrentRGB[1];
							array107[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, array107, in_ColorValue141, in_ColorValue142, in_ColorValue143, in_ColorValue144);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Predator1 != null)
					{
						byte[] array108 = new byte[3];
						byte[] array109 = new byte[3];
						byte[] array110 = new byte[3];
						byte[] array111 = new byte[3];
						byte[] array112 = new byte[3];
						for (int num10 = 0; num10 < 5; num10++)
						{
							List<List<int>> colors16 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (num10)
							{
							case 0:
								array108[0] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(0)));
								array108[1] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(1)));
								array108[2] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(2)));
								break;
							case 1:
								array109[0] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(0)));
								array109[1] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(1)));
								array109[2] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(2)));
								break;
							case 2:
								array110[0] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(0)));
								array110[1] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(1)));
								array110[2] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(2)));
								break;
							case 3:
								array111[0] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(0)));
								array111[1] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(1)));
								array111[2] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(2)));
								break;
							case 4:
								array112[0] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(0)));
								array112[1] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(1)));
								array112[2] = Convert.ToByte(Convert.ToByte(colors16[num10].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, array108, array109, array110, array111, array112);
					}
					createdCount++;
					break;
				case 2:
					if (Pannel_DRAM_Predator2 == null)
					{
						Pannel_DRAM_Predator2 = new UC_Pannel_DRAM_Predator();
						Pannel_DRAM_Predator2.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Predator2.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin15 = default(Thickness);
						margin15.Top = 257.0;
						Pannel_DRAM_Predator2.Margin = margin15;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Predator2);
					}
					if (sMode == "all_off")
					{
						byte[] array99 = new byte[1] { 125 };
						Pannel_DRAM_Predator2.SetStyleEffect(sMode, sOption, array99, array99, array99, array99, array99);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Predator2 != null)
						{
							byte[] in_ColorValue127 = new byte[3];
							byte[] in_ColorValue128 = new byte[3];
							byte[] in_ColorValue129 = new byte[3];
							byte[] in_ColorValue130 = new byte[3];
							byte[] in_ColorValue131 = new byte[3];
							Pannel_DRAM_Predator2.SetStyleEffect(sMode, sOption, in_ColorValue127, in_ColorValue128, in_ColorValue129, in_ColorValue130, in_ColorValue131);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Predator2 != null)
						{
							byte[] array100 = new byte[3];
							byte[] in_ColorValue132 = new byte[3];
							byte[] in_ColorValue133 = new byte[3];
							byte[] in_ColorValue134 = new byte[3];
							byte[] in_ColorValue135 = new byte[3];
							array100[0] = ColorPalette.GetCurrentRGB[0];
							array100[1] = ColorPalette.GetCurrentRGB[1];
							array100[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Predator2.SetStyleEffect(sMode, sOption, array100, in_ColorValue132, in_ColorValue133, in_ColorValue134, in_ColorValue135);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Predator2 != null)
					{
						byte[] array101 = new byte[3];
						byte[] array102 = new byte[3];
						byte[] array103 = new byte[3];
						byte[] array104 = new byte[3];
						byte[] array105 = new byte[3];
						for (int num9 = 0; num9 < 5; num9++)
						{
							List<List<int>> colors15 = _DRAM_colors.ElementAt(2).Value.colors;
							switch (num9)
							{
							case 0:
								array101[0] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(0)));
								array101[1] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(1)));
								array101[2] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(2)));
								break;
							case 1:
								array102[0] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(0)));
								array102[1] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(1)));
								array102[2] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(2)));
								break;
							case 2:
								array103[0] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(0)));
								array103[1] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(1)));
								array103[2] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(2)));
								break;
							case 3:
								array104[0] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(0)));
								array104[1] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(1)));
								array104[2] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(2)));
								break;
							case 4:
								array105[0] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(0)));
								array105[1] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(1)));
								array105[2] = Convert.ToByte(Convert.ToByte(colors15[num9].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Predator2.SetStyleEffect(sMode, sOption, array101, array102, array103, array104, array105);
					}
					createdCount++;
					break;
				}
				break;
			case 4:
				switch (createdCount)
				{
				case 0:
					if (Pannel_DRAM_Predator0 == null)
					{
						Pannel_DRAM_Predator0 = new UC_Pannel_DRAM_Predator();
						Pannel_DRAM_Predator0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Predator0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin12 = default(Thickness);
						margin12.Top = 0.0;
						Pannel_DRAM_Predator0.Margin = margin12;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Predator0);
					}
					if (sMode == "all_off")
					{
						byte[] array78 = new byte[1] { 125 };
						Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array78, array78, array78, array78, array78);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Predator0 != null)
						{
							byte[] in_ColorValue100 = new byte[3];
							byte[] in_ColorValue101 = new byte[3];
							byte[] in_ColorValue102 = new byte[3];
							byte[] in_ColorValue103 = new byte[3];
							byte[] in_ColorValue104 = new byte[3];
							Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, in_ColorValue100, in_ColorValue101, in_ColorValue102, in_ColorValue103, in_ColorValue104);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Predator0 != null)
						{
							byte[] array79 = new byte[3];
							byte[] in_ColorValue105 = new byte[3];
							byte[] in_ColorValue106 = new byte[3];
							byte[] in_ColorValue107 = new byte[3];
							byte[] in_ColorValue108 = new byte[3];
							array79[0] = ColorPalette.GetCurrentRGB[0];
							array79[1] = ColorPalette.GetCurrentRGB[1];
							array79[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array79, in_ColorValue105, in_ColorValue106, in_ColorValue107, in_ColorValue108);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Predator0 != null)
					{
						byte[] array80 = new byte[3];
						byte[] array81 = new byte[3];
						byte[] array82 = new byte[3];
						byte[] array83 = new byte[3];
						byte[] array84 = new byte[3];
						for (int num6 = 0; num6 < 5; num6++)
						{
							List<List<int>> colors12 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num6)
							{
							case 0:
								array80[0] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(0)));
								array80[1] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(1)));
								array80[2] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(2)));
								break;
							case 1:
								array81[0] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(0)));
								array81[1] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(1)));
								array81[2] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(2)));
								break;
							case 2:
								array82[0] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(0)));
								array82[1] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(1)));
								array82[2] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(2)));
								break;
							case 3:
								array83[0] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(0)));
								array83[1] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(1)));
								array83[2] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(2)));
								break;
							case 4:
								array84[0] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(0)));
								array84[1] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(1)));
								array84[2] = Convert.ToByte(Convert.ToByte(colors12[num6].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Predator0.SetStyleEffect(sMode, sOption, array80, array81, array82, array83, array84);
					}
					createdCount++;
					break;
				case 1:
					if (Pannel_DRAM_Predator1 == null)
					{
						Pannel_DRAM_Predator1 = new UC_Pannel_DRAM_Predator();
						Pannel_DRAM_Predator1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Predator1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin14 = default(Thickness);
						margin14.Top = 103.0;
						Pannel_DRAM_Predator1.Margin = margin14;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Predator1);
					}
					if (sMode == "all_off")
					{
						byte[] array92 = new byte[1] { 125 };
						Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, array92, array92, array92, array92, array92);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Predator1 != null)
						{
							byte[] in_ColorValue118 = new byte[3];
							byte[] in_ColorValue119 = new byte[3];
							byte[] in_ColorValue120 = new byte[3];
							byte[] in_ColorValue121 = new byte[3];
							byte[] in_ColorValue122 = new byte[3];
							Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, in_ColorValue118, in_ColorValue119, in_ColorValue120, in_ColorValue121, in_ColorValue122);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Predator1 != null)
						{
							byte[] array93 = new byte[3];
							byte[] in_ColorValue123 = new byte[3];
							byte[] in_ColorValue124 = new byte[3];
							byte[] in_ColorValue125 = new byte[3];
							byte[] in_ColorValue126 = new byte[3];
							array93[0] = ColorPalette.GetCurrentRGB[0];
							array93[1] = ColorPalette.GetCurrentRGB[1];
							array93[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, array93, in_ColorValue123, in_ColorValue124, in_ColorValue125, in_ColorValue126);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Predator1 != null)
					{
						byte[] array94 = new byte[3];
						byte[] array95 = new byte[3];
						byte[] array96 = new byte[3];
						byte[] array97 = new byte[3];
						byte[] array98 = new byte[3];
						for (int num8 = 0; num8 < 5; num8++)
						{
							List<List<int>> colors14 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (num8)
							{
							case 0:
								array94[0] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(0)));
								array94[1] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(1)));
								array94[2] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(2)));
								break;
							case 1:
								array95[0] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(0)));
								array95[1] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(1)));
								array95[2] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(2)));
								break;
							case 2:
								array96[0] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(0)));
								array96[1] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(1)));
								array96[2] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(2)));
								break;
							case 3:
								array97[0] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(0)));
								array97[1] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(1)));
								array97[2] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(2)));
								break;
							case 4:
								array98[0] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(0)));
								array98[1] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(1)));
								array98[2] = Convert.ToByte(Convert.ToByte(colors14[num8].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Predator1.SetStyleEffect(sMode, sOption, array94, array95, array96, array97, array98);
					}
					createdCount++;
					break;
				case 2:
					if (Pannel_DRAM_Predator2 == null)
					{
						Pannel_DRAM_Predator2 = new UC_Pannel_DRAM_Predator();
						Pannel_DRAM_Predator2.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Predator2.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin13 = default(Thickness);
						margin13.Top = 206.0;
						Pannel_DRAM_Predator2.Margin = margin13;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Predator2);
					}
					if (sMode == "all_off")
					{
						byte[] array85 = new byte[1] { 125 };
						Pannel_DRAM_Predator2.SetStyleEffect(sMode, sOption, array85, array85, array85, array85, array85);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Predator2 != null)
						{
							byte[] in_ColorValue109 = new byte[3];
							byte[] in_ColorValue110 = new byte[3];
							byte[] in_ColorValue111 = new byte[3];
							byte[] in_ColorValue112 = new byte[3];
							byte[] in_ColorValue113 = new byte[3];
							Pannel_DRAM_Predator2.SetStyleEffect(sMode, sOption, in_ColorValue109, in_ColorValue110, in_ColorValue111, in_ColorValue112, in_ColorValue113);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Predator2 != null)
						{
							byte[] array86 = new byte[3];
							byte[] in_ColorValue114 = new byte[3];
							byte[] in_ColorValue115 = new byte[3];
							byte[] in_ColorValue116 = new byte[3];
							byte[] in_ColorValue117 = new byte[3];
							array86[0] = ColorPalette.GetCurrentRGB[0];
							array86[1] = ColorPalette.GetCurrentRGB[1];
							array86[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Predator2.SetStyleEffect(sMode, sOption, array86, in_ColorValue114, in_ColorValue115, in_ColorValue116, in_ColorValue117);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Predator2 != null)
					{
						byte[] array87 = new byte[3];
						byte[] array88 = new byte[3];
						byte[] array89 = new byte[3];
						byte[] array90 = new byte[3];
						byte[] array91 = new byte[3];
						for (int num7 = 0; num7 < 5; num7++)
						{
							List<List<int>> colors13 = _DRAM_colors.ElementAt(2).Value.colors;
							switch (num7)
							{
							case 0:
								array87[0] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(0)));
								array87[1] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(1)));
								array87[2] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(2)));
								break;
							case 1:
								array88[0] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(0)));
								array88[1] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(1)));
								array88[2] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(2)));
								break;
							case 2:
								array89[0] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(0)));
								array89[1] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(1)));
								array89[2] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(2)));
								break;
							case 3:
								array90[0] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(0)));
								array90[1] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(1)));
								array90[2] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(2)));
								break;
							case 4:
								array91[0] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(0)));
								array91[1] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(1)));
								array91[2] = Convert.ToByte(Convert.ToByte(colors13[num7].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Predator2.SetStyleEffect(sMode, sOption, array87, array88, array89, array90, array91);
					}
					createdCount++;
					break;
				case 3:
					if (Pannel_DRAM_Predator3 == null)
					{
						Pannel_DRAM_Predator3 = new UC_Pannel_DRAM_Predator();
						Pannel_DRAM_Predator3.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Predator3.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin11 = default(Thickness);
						margin11.Top = 309.0;
						Pannel_DRAM_Predator3.Margin = margin11;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Predator3);
					}
					if (sMode == "all_off")
					{
						byte[] array71 = new byte[1] { 125 };
						Pannel_DRAM_Predator3.SetStyleEffect(sMode, sOption, array71, array71, array71, array71, array71);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Predator3 != null)
						{
							byte[] in_ColorValue91 = new byte[3];
							byte[] in_ColorValue92 = new byte[3];
							byte[] in_ColorValue93 = new byte[3];
							byte[] in_ColorValue94 = new byte[3];
							byte[] in_ColorValue95 = new byte[3];
							Pannel_DRAM_Predator3.SetStyleEffect(sMode, sOption, in_ColorValue91, in_ColorValue92, in_ColorValue93, in_ColorValue94, in_ColorValue95);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Predator3 != null)
						{
							byte[] array72 = new byte[3];
							byte[] in_ColorValue96 = new byte[3];
							byte[] in_ColorValue97 = new byte[3];
							byte[] in_ColorValue98 = new byte[3];
							byte[] in_ColorValue99 = new byte[3];
							array72[0] = ColorPalette.GetCurrentRGB[0];
							array72[1] = ColorPalette.GetCurrentRGB[1];
							array72[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Predator3.SetStyleEffect(sMode, sOption, array72, in_ColorValue96, in_ColorValue97, in_ColorValue98, in_ColorValue99);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Predator3 != null)
					{
						byte[] array73 = new byte[3];
						byte[] array74 = new byte[3];
						byte[] array75 = new byte[3];
						byte[] array76 = new byte[3];
						byte[] array77 = new byte[3];
						for (int num5 = 0; num5 < 5; num5++)
						{
							List<List<int>> colors11 = _DRAM_colors.ElementAt(3).Value.colors;
							switch (num5)
							{
							case 0:
								array73[0] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(0)));
								array73[1] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(1)));
								array73[2] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(2)));
								break;
							case 1:
								array74[0] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(0)));
								array74[1] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(1)));
								array74[2] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(2)));
								break;
							case 2:
								array75[0] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(0)));
								array75[1] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(1)));
								array75[2] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(2)));
								break;
							case 3:
								array76[0] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(0)));
								array76[1] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(1)));
								array76[2] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(2)));
								break;
							case 4:
								array77[0] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(0)));
								array77[1] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(1)));
								array77[2] = Convert.ToByte(Convert.ToByte(colors11[num5].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Predator3.SetStyleEffect(sMode, sOption, array73, array74, array75, array76, array77);
					}
					createdCount++;
					break;
				}
				break;
			}
			break;
		case "Renegade":
			switch (deviceCount)
			{
			case 1:
				if (Pannel_DRAM_Renegade0 == null)
				{
					Pannel_DRAM_Renegade0 = new UC_Pannel_DRAM_Renegade();
					Pannel_DRAM_Renegade0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
					Pannel_DRAM_Renegade0.VerticalAlignment = VerticalAlignment.Top;
					Thickness margin10 = default(Thickness);
					margin10.Top = 154.0;
					Pannel_DRAM_Renegade0.Margin = margin10;
					gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Renegade0);
				}
				if (sMode == "all_off")
				{
					byte[] array64 = new byte[1] { 125 };
					Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array64, array64, array64, array64, array64);
				}
				else if (sOption == "DefaultColor")
				{
					if (Pannel_DRAM_Renegade0 != null)
					{
						byte[] in_ColorValue82 = new byte[3];
						byte[] in_ColorValue83 = new byte[3];
						byte[] in_ColorValue84 = new byte[3];
						byte[] in_ColorValue85 = new byte[3];
						byte[] in_ColorValue86 = new byte[3];
						Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, in_ColorValue82, in_ColorValue83, in_ColorValue84, in_ColorValue85, in_ColorValue86);
					}
				}
				else if (sOption == "OneColor")
				{
					if (Pannel_DRAM_Renegade0 != null)
					{
						byte[] array65 = new byte[3];
						byte[] in_ColorValue87 = new byte[3];
						byte[] in_ColorValue88 = new byte[3];
						byte[] in_ColorValue89 = new byte[3];
						byte[] in_ColorValue90 = new byte[3];
						array65[0] = ColorPalette.GetCurrentRGB[0];
						array65[1] = ColorPalette.GetCurrentRGB[1];
						array65[2] = ColorPalette.GetCurrentRGB[2];
						Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array65, in_ColorValue87, in_ColorValue88, in_ColorValue89, in_ColorValue90);
					}
				}
				else if (sOption == "FiveColor" && Pannel_DRAM_Renegade0 != null)
				{
					byte[] array66 = new byte[3];
					byte[] array67 = new byte[3];
					byte[] array68 = new byte[3];
					byte[] array69 = new byte[3];
					byte[] array70 = new byte[3];
					for (int num4 = 0; num4 < 5; num4++)
					{
						List<List<int>> colors10 = _DRAM_colors.ElementAt(0).Value.colors;
						switch (num4)
						{
						case 0:
							array66[0] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(0)));
							array66[1] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(1)));
							array66[2] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(2)));
							break;
						case 1:
							array67[0] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(0)));
							array67[1] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(1)));
							array67[2] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(2)));
							break;
						case 2:
							array68[0] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(0)));
							array68[1] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(1)));
							array68[2] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(2)));
							break;
						case 3:
							array69[0] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(0)));
							array69[1] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(1)));
							array69[2] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(2)));
							break;
						case 4:
							array70[0] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(0)));
							array70[1] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(1)));
							array70[2] = Convert.ToByte(Convert.ToByte(colors10[num4].ElementAt(2)));
							break;
						}
					}
					Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array66, array67, array68, array69, array70);
				}
				createdCount++;
				break;
			case 2:
				switch (createdCount)
				{
				case 0:
					if (Pannel_DRAM_Renegade0 == null)
					{
						Pannel_DRAM_Renegade0 = new UC_Pannel_DRAM_Renegade();
						Pannel_DRAM_Renegade0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Renegade0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin9 = default(Thickness);
						margin9.Top = 84.0;
						Pannel_DRAM_Renegade0.Margin = margin9;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Renegade0);
					}
					if (sMode == "all_off")
					{
						byte[] array57 = new byte[1] { 125 };
						Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array57, array57, array57, array57, array57);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Renegade0 != null)
						{
							byte[] in_ColorValue73 = new byte[3];
							byte[] in_ColorValue74 = new byte[3];
							byte[] in_ColorValue75 = new byte[3];
							byte[] in_ColorValue76 = new byte[3];
							byte[] in_ColorValue77 = new byte[3];
							Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, in_ColorValue73, in_ColorValue74, in_ColorValue75, in_ColorValue76, in_ColorValue77);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Renegade0 != null)
						{
							byte[] array58 = new byte[3];
							byte[] in_ColorValue78 = new byte[3];
							byte[] in_ColorValue79 = new byte[3];
							byte[] in_ColorValue80 = new byte[3];
							byte[] in_ColorValue81 = new byte[3];
							array58[0] = ColorPalette.GetCurrentRGB[0];
							array58[1] = ColorPalette.GetCurrentRGB[1];
							array58[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array58, in_ColorValue78, in_ColorValue79, in_ColorValue80, in_ColorValue81);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Renegade0 != null)
					{
						byte[] array59 = new byte[3];
						byte[] array60 = new byte[3];
						byte[] array61 = new byte[3];
						byte[] array62 = new byte[3];
						byte[] array63 = new byte[3];
						for (int num3 = 0; num3 < 5; num3++)
						{
							List<List<int>> colors9 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num3)
							{
							case 0:
								array59[0] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(0)));
								array59[1] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(1)));
								array59[2] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(2)));
								break;
							case 1:
								array60[0] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(0)));
								array60[1] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(1)));
								array60[2] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(2)));
								break;
							case 2:
								array61[0] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(0)));
								array61[1] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(1)));
								array61[2] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(2)));
								break;
							case 3:
								array62[0] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(0)));
								array62[1] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(1)));
								array62[2] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(2)));
								break;
							case 4:
								array63[0] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(0)));
								array63[1] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(1)));
								array63[2] = Convert.ToByte(Convert.ToByte(colors9[num3].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array59, array60, array61, array62, array63);
					}
					createdCount++;
					break;
				case 1:
					if (Pannel_DRAM_Renegade1 == null)
					{
						Pannel_DRAM_Renegade1 = new UC_Pannel_DRAM_Renegade();
						Pannel_DRAM_Renegade1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Renegade1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin8 = default(Thickness);
						margin8.Top = 225.0;
						Pannel_DRAM_Renegade1.Margin = margin8;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Renegade1);
					}
					if (sMode == "all_off")
					{
						byte[] array50 = new byte[1] { 125 };
						Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, array50, array50, array50, array50, array50);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Renegade1 != null)
						{
							byte[] in_ColorValue64 = new byte[3];
							byte[] in_ColorValue65 = new byte[3];
							byte[] in_ColorValue66 = new byte[3];
							byte[] in_ColorValue67 = new byte[3];
							byte[] in_ColorValue68 = new byte[3];
							Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, in_ColorValue64, in_ColorValue65, in_ColorValue66, in_ColorValue67, in_ColorValue68);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Renegade1 != null)
						{
							byte[] array51 = new byte[3];
							byte[] in_ColorValue69 = new byte[3];
							byte[] in_ColorValue70 = new byte[3];
							byte[] in_ColorValue71 = new byte[3];
							byte[] in_ColorValue72 = new byte[3];
							array51[0] = ColorPalette.GetCurrentRGB[0];
							array51[1] = ColorPalette.GetCurrentRGB[1];
							array51[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, array51, in_ColorValue69, in_ColorValue70, in_ColorValue71, in_ColorValue72);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Renegade1 != null)
					{
						byte[] array52 = new byte[3];
						byte[] array53 = new byte[3];
						byte[] array54 = new byte[3];
						byte[] array55 = new byte[3];
						byte[] array56 = new byte[3];
						for (int num2 = 0; num2 < 5; num2++)
						{
							List<List<int>> colors8 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (num2)
							{
							case 0:
								array52[0] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(0)));
								array52[1] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(1)));
								array52[2] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(2)));
								break;
							case 1:
								array53[0] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(0)));
								array53[1] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(1)));
								array53[2] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(2)));
								break;
							case 2:
								array54[0] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(0)));
								array54[1] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(1)));
								array54[2] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(2)));
								break;
							case 3:
								array55[0] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(0)));
								array55[1] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(1)));
								array55[2] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(2)));
								break;
							case 4:
								array56[0] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(0)));
								array56[1] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(1)));
								array56[2] = Convert.ToByte(Convert.ToByte(colors8[num2].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, array52, array53, array54, array55, array56);
					}
					createdCount++;
					break;
				}
				break;
			case 3:
				switch (createdCount)
				{
				case 0:
					if (Pannel_DRAM_Renegade0 == null)
					{
						Pannel_DRAM_Renegade0 = new UC_Pannel_DRAM_Renegade();
						Pannel_DRAM_Renegade0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Renegade0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin7 = default(Thickness);
						margin7.Top = 52.0;
						Pannel_DRAM_Renegade0.Margin = margin7;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Renegade0);
					}
					if (sMode == "all_off")
					{
						byte[] array43 = new byte[1] { 125 };
						Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array43, array43, array43, array43, array43);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Renegade0 != null)
						{
							byte[] in_ColorValue55 = new byte[3];
							byte[] in_ColorValue56 = new byte[3];
							byte[] in_ColorValue57 = new byte[3];
							byte[] in_ColorValue58 = new byte[3];
							byte[] in_ColorValue59 = new byte[3];
							Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, in_ColorValue55, in_ColorValue56, in_ColorValue57, in_ColorValue58, in_ColorValue59);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Renegade0 != null)
						{
							byte[] array44 = new byte[3];
							byte[] in_ColorValue60 = new byte[3];
							byte[] in_ColorValue61 = new byte[3];
							byte[] in_ColorValue62 = new byte[3];
							byte[] in_ColorValue63 = new byte[3];
							array44[0] = ColorPalette.GetCurrentRGB[0];
							array44[1] = ColorPalette.GetCurrentRGB[1];
							array44[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array44, in_ColorValue60, in_ColorValue61, in_ColorValue62, in_ColorValue63);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Renegade0 != null)
					{
						byte[] array45 = new byte[3];
						byte[] array46 = new byte[3];
						byte[] array47 = new byte[3];
						byte[] array48 = new byte[3];
						byte[] array49 = new byte[3];
						for (int num = 0; num < 5; num++)
						{
							List<List<int>> colors7 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (num)
							{
							case 0:
								array45[0] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(0)));
								array45[1] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(1)));
								array45[2] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(2)));
								break;
							case 1:
								array46[0] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(0)));
								array46[1] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(1)));
								array46[2] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(2)));
								break;
							case 2:
								array47[0] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(0)));
								array47[1] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(1)));
								array47[2] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(2)));
								break;
							case 3:
								array48[0] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(0)));
								array48[1] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(1)));
								array48[2] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(2)));
								break;
							case 4:
								array49[0] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(0)));
								array49[1] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(1)));
								array49[2] = Convert.ToByte(Convert.ToByte(colors7[num].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array45, array46, array47, array48, array49);
					}
					createdCount++;
					break;
				case 1:
					if (Pannel_DRAM_Renegade1 == null)
					{
						Pannel_DRAM_Renegade1 = new UC_Pannel_DRAM_Renegade();
						Pannel_DRAM_Renegade1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Renegade1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin6 = default(Thickness);
						margin6.Top = 155.0;
						Pannel_DRAM_Renegade1.Margin = margin6;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Renegade1);
					}
					if (sMode == "all_off")
					{
						byte[] array36 = new byte[1] { 125 };
						Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, array36, array36, array36, array36, array36);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Renegade1 != null)
						{
							byte[] in_ColorValue46 = new byte[3];
							byte[] in_ColorValue47 = new byte[3];
							byte[] in_ColorValue48 = new byte[3];
							byte[] in_ColorValue49 = new byte[3];
							byte[] in_ColorValue50 = new byte[3];
							Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, in_ColorValue46, in_ColorValue47, in_ColorValue48, in_ColorValue49, in_ColorValue50);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Renegade1 != null)
						{
							byte[] array37 = new byte[3];
							byte[] in_ColorValue51 = new byte[3];
							byte[] in_ColorValue52 = new byte[3];
							byte[] in_ColorValue53 = new byte[3];
							byte[] in_ColorValue54 = new byte[3];
							array37[0] = ColorPalette.GetCurrentRGB[0];
							array37[1] = ColorPalette.GetCurrentRGB[1];
							array37[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, array37, in_ColorValue51, in_ColorValue52, in_ColorValue53, in_ColorValue54);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Renegade1 != null)
					{
						byte[] array38 = new byte[3];
						byte[] array39 = new byte[3];
						byte[] array40 = new byte[3];
						byte[] array41 = new byte[3];
						byte[] array42 = new byte[3];
						for (int n = 0; n < 5; n++)
						{
							List<List<int>> colors6 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (n)
							{
							case 0:
								array38[0] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(0)));
								array38[1] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(1)));
								array38[2] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(2)));
								break;
							case 1:
								array39[0] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(0)));
								array39[1] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(1)));
								array39[2] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(2)));
								break;
							case 2:
								array40[0] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(0)));
								array40[1] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(1)));
								array40[2] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(2)));
								break;
							case 3:
								array41[0] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(0)));
								array41[1] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(1)));
								array41[2] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(2)));
								break;
							case 4:
								array42[0] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(0)));
								array42[1] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(1)));
								array42[2] = Convert.ToByte(Convert.ToByte(colors6[n].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, array38, array39, array40, array41, array42);
						Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, array38, array39, array40, array41, array42);
					}
					createdCount++;
					break;
				case 2:
					if (Pannel_DRAM_Renegade2 == null)
					{
						Pannel_DRAM_Renegade2 = new UC_Pannel_DRAM_Renegade();
						Pannel_DRAM_Renegade2.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Renegade2.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin5 = default(Thickness);
						margin5.Top = 257.0;
						Pannel_DRAM_Renegade2.Margin = margin5;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Renegade2);
					}
					if (sMode == "all_off")
					{
						byte[] array29 = new byte[1] { 125 };
						Pannel_DRAM_Renegade2.SetStyleEffect(sMode, sOption, array29, array29, array29, array29, array29);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Renegade2 != null)
						{
							byte[] in_ColorValue37 = new byte[3];
							byte[] in_ColorValue38 = new byte[3];
							byte[] in_ColorValue39 = new byte[3];
							byte[] in_ColorValue40 = new byte[3];
							byte[] in_ColorValue41 = new byte[3];
							Pannel_DRAM_Renegade2.SetStyleEffect(sMode, sOption, in_ColorValue37, in_ColorValue38, in_ColorValue39, in_ColorValue40, in_ColorValue41);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Renegade2 != null)
						{
							byte[] array30 = new byte[3];
							byte[] in_ColorValue42 = new byte[3];
							byte[] in_ColorValue43 = new byte[3];
							byte[] in_ColorValue44 = new byte[3];
							byte[] in_ColorValue45 = new byte[3];
							array30[0] = ColorPalette.GetCurrentRGB[0];
							array30[1] = ColorPalette.GetCurrentRGB[1];
							array30[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Renegade2.SetStyleEffect(sMode, sOption, array30, in_ColorValue42, in_ColorValue43, in_ColorValue44, in_ColorValue45);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Renegade2 != null)
					{
						byte[] array31 = new byte[3];
						byte[] array32 = new byte[3];
						byte[] array33 = new byte[3];
						byte[] array34 = new byte[3];
						byte[] array35 = new byte[3];
						for (int m = 0; m < 5; m++)
						{
							List<List<int>> colors5 = _DRAM_colors.ElementAt(2).Value.colors;
							switch (m)
							{
							case 0:
								array31[0] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(0)));
								array31[1] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(1)));
								array31[2] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(2)));
								break;
							case 1:
								array32[0] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(0)));
								array32[1] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(1)));
								array32[2] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(2)));
								break;
							case 2:
								array33[0] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(0)));
								array33[1] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(1)));
								array33[2] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(2)));
								break;
							case 3:
								array34[0] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(0)));
								array34[1] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(1)));
								array34[2] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(2)));
								break;
							case 4:
								array35[0] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(0)));
								array35[1] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(1)));
								array35[2] = Convert.ToByte(Convert.ToByte(colors5[m].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Renegade2.SetStyleEffect(sMode, sOption, array31, array32, array33, array34, array35);
					}
					createdCount++;
					break;
				}
				break;
			case 4:
				switch (createdCount)
				{
				case 0:
					if (Pannel_DRAM_Renegade0 == null)
					{
						Pannel_DRAM_Renegade0 = new UC_Pannel_DRAM_Renegade();
						Pannel_DRAM_Renegade0.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Renegade0.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin2 = default(Thickness);
						margin2.Top = 0.0;
						Pannel_DRAM_Renegade0.Margin = margin2;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Renegade0);
					}
					if (sMode == "all_off")
					{
						byte[] array8 = new byte[1] { 125 };
						Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array8, array8, array8, array8, array8);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Renegade0 != null)
						{
							byte[] in_ColorValue10 = new byte[3];
							byte[] in_ColorValue11 = new byte[3];
							byte[] in_ColorValue12 = new byte[3];
							byte[] in_ColorValue13 = new byte[3];
							byte[] in_ColorValue14 = new byte[3];
							Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, in_ColorValue10, in_ColorValue11, in_ColorValue12, in_ColorValue13, in_ColorValue14);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Renegade0 != null)
						{
							byte[] array9 = new byte[3];
							byte[] in_ColorValue15 = new byte[3];
							byte[] in_ColorValue16 = new byte[3];
							byte[] in_ColorValue17 = new byte[3];
							byte[] in_ColorValue18 = new byte[3];
							array9[0] = ColorPalette.GetCurrentRGB[0];
							array9[1] = ColorPalette.GetCurrentRGB[1];
							array9[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array9, in_ColorValue15, in_ColorValue16, in_ColorValue17, in_ColorValue18);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Renegade0 != null)
					{
						byte[] array10 = new byte[3];
						byte[] array11 = new byte[3];
						byte[] array12 = new byte[3];
						byte[] array13 = new byte[3];
						byte[] array14 = new byte[3];
						for (int j = 0; j < 5; j++)
						{
							List<List<int>> colors2 = _DRAM_colors.ElementAt(0).Value.colors;
							switch (j)
							{
							case 0:
								array10[0] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(0)));
								array10[1] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(1)));
								array10[2] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(2)));
								break;
							case 1:
								array11[0] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(0)));
								array11[1] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(1)));
								array11[2] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(2)));
								break;
							case 2:
								array12[0] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(0)));
								array12[1] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(1)));
								array12[2] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(2)));
								break;
							case 3:
								array13[0] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(0)));
								array13[1] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(1)));
								array13[2] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(2)));
								break;
							case 4:
								array14[0] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(0)));
								array14[1] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(1)));
								array14[2] = Convert.ToByte(Convert.ToByte(colors2[j].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Renegade0.SetStyleEffect(sMode, sOption, array10, array11, array12, array13, array14);
					}
					createdCount++;
					break;
				case 1:
					if (Pannel_DRAM_Renegade1 == null)
					{
						Pannel_DRAM_Renegade1 = new UC_Pannel_DRAM_Renegade();
						Pannel_DRAM_Renegade1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Renegade1.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin4 = default(Thickness);
						margin4.Top = 103.0;
						Pannel_DRAM_Renegade1.Margin = margin4;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Renegade1);
					}
					if (sMode == "all_off")
					{
						byte[] array22 = new byte[1] { 125 };
						Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, array22, array22, array22, array22, array22);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Renegade1 != null)
						{
							byte[] in_ColorValue28 = new byte[3];
							byte[] in_ColorValue29 = new byte[3];
							byte[] in_ColorValue30 = new byte[3];
							byte[] in_ColorValue31 = new byte[3];
							byte[] in_ColorValue32 = new byte[3];
							Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, in_ColorValue28, in_ColorValue29, in_ColorValue30, in_ColorValue31, in_ColorValue32);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Renegade1 != null)
						{
							byte[] array23 = new byte[3];
							byte[] in_ColorValue33 = new byte[3];
							byte[] in_ColorValue34 = new byte[3];
							byte[] in_ColorValue35 = new byte[3];
							byte[] in_ColorValue36 = new byte[3];
							array23[0] = ColorPalette.GetCurrentRGB[0];
							array23[1] = ColorPalette.GetCurrentRGB[1];
							array23[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, array23, in_ColorValue33, in_ColorValue34, in_ColorValue35, in_ColorValue36);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Renegade1 != null)
					{
						byte[] array24 = new byte[3];
						byte[] array25 = new byte[3];
						byte[] array26 = new byte[3];
						byte[] array27 = new byte[3];
						byte[] array28 = new byte[3];
						for (int l = 0; l < 5; l++)
						{
							List<List<int>> colors4 = _DRAM_colors.ElementAt(1).Value.colors;
							switch (l)
							{
							case 0:
								array24[0] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(0)));
								array24[1] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(1)));
								array24[2] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(2)));
								break;
							case 1:
								array25[0] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(0)));
								array25[1] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(1)));
								array25[2] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(2)));
								break;
							case 2:
								array26[0] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(0)));
								array26[1] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(1)));
								array26[2] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(2)));
								break;
							case 3:
								array27[0] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(0)));
								array27[1] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(1)));
								array27[2] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(2)));
								break;
							case 4:
								array28[0] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(0)));
								array28[1] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(1)));
								array28[2] = Convert.ToByte(Convert.ToByte(colors4[l].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Renegade1.SetStyleEffect(sMode, sOption, array24, array25, array26, array27, array28);
					}
					createdCount++;
					break;
				case 2:
					if (Pannel_DRAM_Renegade2 == null)
					{
						Pannel_DRAM_Renegade2 = new UC_Pannel_DRAM_Renegade();
						Pannel_DRAM_Renegade2.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Renegade2.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin3 = default(Thickness);
						margin3.Top = 206.0;
						Pannel_DRAM_Renegade2.Margin = margin3;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Renegade2);
					}
					if (sMode == "all_off")
					{
						byte[] array15 = new byte[1] { 125 };
						Pannel_DRAM_Renegade2.SetStyleEffect(sMode, sOption, array15, array15, array15, array15, array15);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Renegade2 != null)
						{
							byte[] in_ColorValue19 = new byte[3];
							byte[] in_ColorValue20 = new byte[3];
							byte[] in_ColorValue21 = new byte[3];
							byte[] in_ColorValue22 = new byte[3];
							byte[] in_ColorValue23 = new byte[3];
							Pannel_DRAM_Renegade2.SetStyleEffect(sMode, sOption, in_ColorValue19, in_ColorValue20, in_ColorValue21, in_ColorValue22, in_ColorValue23);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Renegade2 != null)
						{
							byte[] array16 = new byte[3];
							byte[] in_ColorValue24 = new byte[3];
							byte[] in_ColorValue25 = new byte[3];
							byte[] in_ColorValue26 = new byte[3];
							byte[] in_ColorValue27 = new byte[3];
							array16[0] = ColorPalette.GetCurrentRGB[0];
							array16[1] = ColorPalette.GetCurrentRGB[1];
							array16[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Renegade2.SetStyleEffect(sMode, sOption, array16, in_ColorValue24, in_ColorValue25, in_ColorValue26, in_ColorValue27);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Renegade2 != null)
					{
						byte[] array17 = new byte[3];
						byte[] array18 = new byte[3];
						byte[] array19 = new byte[3];
						byte[] array20 = new byte[3];
						byte[] array21 = new byte[3];
						for (int k = 0; k < 5; k++)
						{
							List<List<int>> colors3 = _DRAM_colors.ElementAt(2).Value.colors;
							switch (k)
							{
							case 0:
								array17[0] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(0)));
								array17[1] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(1)));
								array17[2] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(2)));
								break;
							case 1:
								array18[0] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(0)));
								array18[1] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(1)));
								array18[2] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(2)));
								break;
							case 2:
								array19[0] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(0)));
								array19[1] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(1)));
								array19[2] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(2)));
								break;
							case 3:
								array20[0] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(0)));
								array20[1] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(1)));
								array20[2] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(2)));
								break;
							case 4:
								array21[0] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(0)));
								array21[1] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(1)));
								array21[2] = Convert.ToByte(Convert.ToByte(colors3[k].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Renegade2.SetStyleEffect(sMode, sOption, array17, array18, array19, array20, array21);
					}
					createdCount++;
					break;
				case 3:
					if (Pannel_DRAM_Renegade3 == null)
					{
						Pannel_DRAM_Renegade3 = new UC_Pannel_DRAM_Renegade();
						Pannel_DRAM_Renegade3.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
						Pannel_DRAM_Renegade3.VerticalAlignment = VerticalAlignment.Top;
						Thickness margin = default(Thickness);
						margin.Top = 309.0;
						Pannel_DRAM_Renegade3.Margin = margin;
						gr_DevicePanel_Childs.Children.Add(Pannel_DRAM_Renegade3);
					}
					if (sMode == "all_off")
					{
						byte[] array = new byte[1] { 125 };
						Pannel_DRAM_Renegade3.SetStyleEffect(sMode, sOption, array, array, array, array, array);
					}
					else if (sOption == "DefaultColor")
					{
						if (Pannel_DRAM_Renegade3 != null)
						{
							byte[] in_ColorValue = new byte[3];
							byte[] in_ColorValue2 = new byte[3];
							byte[] in_ColorValue3 = new byte[3];
							byte[] in_ColorValue4 = new byte[3];
							byte[] in_ColorValue5 = new byte[3];
							Pannel_DRAM_Renegade3.SetStyleEffect(sMode, sOption, in_ColorValue, in_ColorValue2, in_ColorValue3, in_ColorValue4, in_ColorValue5);
						}
					}
					else if (sOption == "OneColor")
					{
						if (Pannel_DRAM_Renegade3 != null)
						{
							byte[] array2 = new byte[3];
							byte[] in_ColorValue6 = new byte[3];
							byte[] in_ColorValue7 = new byte[3];
							byte[] in_ColorValue8 = new byte[3];
							byte[] in_ColorValue9 = new byte[3];
							array2[0] = ColorPalette.GetCurrentRGB[0];
							array2[1] = ColorPalette.GetCurrentRGB[1];
							array2[2] = ColorPalette.GetCurrentRGB[2];
							Pannel_DRAM_Renegade3.SetStyleEffect(sMode, sOption, array2, in_ColorValue6, in_ColorValue7, in_ColorValue8, in_ColorValue9);
						}
					}
					else if (sOption == "FiveColor" && Pannel_DRAM_Renegade3 != null)
					{
						byte[] array3 = new byte[3];
						byte[] array4 = new byte[3];
						byte[] array5 = new byte[3];
						byte[] array6 = new byte[3];
						byte[] array7 = new byte[3];
						for (int i = 0; i < 5; i++)
						{
							List<List<int>> colors = _DRAM_colors.ElementAt(3).Value.colors;
							switch (i)
							{
							case 0:
								array3[0] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(0)));
								array3[1] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(1)));
								array3[2] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(2)));
								break;
							case 1:
								array4[0] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(0)));
								array4[1] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(1)));
								array4[2] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(2)));
								break;
							case 2:
								array5[0] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(0)));
								array5[1] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(1)));
								array5[2] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(2)));
								break;
							case 3:
								array6[0] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(0)));
								array6[1] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(1)));
								array6[2] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(2)));
								break;
							case 4:
								array7[0] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(0)));
								array7[1] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(1)));
								array7[2] = Convert.ToByte(Convert.ToByte(colors[i].ElementAt(2)));
								break;
							}
						}
						Pannel_DRAM_Renegade3.SetStyleEffect(sMode, sOption, array3, array4, array5, array6, array7);
					}
					createdCount++;
					break;
				}
				break;
			}
			break;
		}
	}

	public void CheckDeviceSlot()
	{
		if (createdCount <= deviceCount)
		{
			switch (createdCount)
			{
			case 1:
			{
				TextBlock textBlock3 = new TextBlock();
				string text6 = (textBlock3.Text = (string)FindResource("StringSlot") + createdCount);
				textBlock3.Style = (Style)FindResource("style_tb_slot");
				gr_MultiColors_Childs.Children.Add(textBlock3);
				if (sOption == "FiveColor")
				{
					for (int k = 0; k < 5; k++)
					{
						System.Windows.Shapes.Path path3 = new System.Windows.Shapes.Path();
						path3.Name = "MultiColor_" + slotIndex + "_" + k;
						List<List<int>> colors3 = _DRAM_colors.ElementAt(0).Value.colors;
						byte b5 = Convert.ToByte(colors3[k].ElementAt(0));
						byte g3 = Convert.ToByte(colors3[k].ElementAt(1));
						byte b6 = Convert.ToByte(colors3[k].ElementAt(2));
						path3.Fill = new SolidColorBrush(System.Windows.Media.Color.FromRgb(b5, g3, b6));
						Geometry geometry6 = (path3.Data = Geometry.Parse("M0, 0 L52, 0 L52, 52 L0, 52"));
						Thickness margin5 = default(Thickness);
						margin5.Left = 430 - 64 * k;
						margin5.Top = 62 * (createdCount - 1);
						path3.Margin = margin5;
						path3.MouseLeftButtonUp += MultiColors_MouseLeftButtonUp;
						gr_MultiColors_Childs.Children.Add(path3);
					}
				}
				break;
			}
			case 2:
			{
				TextBlock textBlock2 = new TextBlock();
				string text4 = (textBlock2.Text = (string)FindResource("StringSlot") + createdCount);
				textBlock2.Style = (Style)FindResource("style_tb_slot");
				Thickness margin3 = default(Thickness);
				margin3.Top = 62 * (createdCount - 1) + 10;
				textBlock2.Margin = margin3;
				gr_MultiColors_Childs.Children.Add(textBlock2);
				if (sOption == "FiveColor")
				{
					for (int j = 0; j < 5; j++)
					{
						System.Windows.Shapes.Path path2 = new System.Windows.Shapes.Path();
						path2.Name = "MultiColor_" + slotIndex + "_" + j;
						List<List<int>> colors2 = _DRAM_colors.ElementAt(1).Value.colors;
						byte b3 = Convert.ToByte(colors2[j].ElementAt(0));
						byte g2 = Convert.ToByte(colors2[j].ElementAt(1));
						byte b4 = Convert.ToByte(colors2[j].ElementAt(2));
						path2.Fill = new SolidColorBrush(System.Windows.Media.Color.FromRgb(b3, g2, b4));
						Geometry geometry4 = (path2.Data = Geometry.Parse("M0, 0 L52, 0 L52, 52 L0, 52"));
						Thickness margin4 = default(Thickness);
						margin4.Left = 430 - 64 * j;
						margin4.Top = 62 * (createdCount - 1);
						path2.Margin = margin4;
						path2.MouseLeftButtonUp += MultiColors_MouseLeftButtonUp;
						gr_MultiColors_Childs.Children.Add(path2);
					}
				}
				break;
			}
			case 3:
			{
				TextBlock textBlock4 = new TextBlock();
				string text8 = (textBlock4.Text = (string)FindResource("StringSlot") + createdCount);
				textBlock4.Style = (Style)FindResource("style_tb_slot");
				Thickness margin6 = default(Thickness);
				margin6.Top = 62 * (createdCount - 1) + 10;
				textBlock4.Margin = margin6;
				gr_MultiColors_Childs.Children.Add(textBlock4);
				if (sOption == "FiveColor")
				{
					for (int l = 0; l < 5; l++)
					{
						System.Windows.Shapes.Path path4 = new System.Windows.Shapes.Path();
						path4.Name = "MultiColor_" + slotIndex + "_" + l;
						List<List<int>> colors4 = _DRAM_colors.ElementAt(2).Value.colors;
						byte b7 = Convert.ToByte(colors4[l].ElementAt(0));
						byte g4 = Convert.ToByte(colors4[l].ElementAt(1));
						byte b8 = Convert.ToByte(colors4[l].ElementAt(2));
						path4.Fill = new SolidColorBrush(System.Windows.Media.Color.FromRgb(b7, g4, b8));
						Geometry geometry8 = (path4.Data = Geometry.Parse("M0, 0 L52, 0 L52, 52 L0, 52"));
						Thickness margin7 = default(Thickness);
						margin7.Left = 430 - 64 * l;
						margin7.Top = 62 * (createdCount - 1);
						path4.Margin = margin7;
						path4.MouseLeftButtonUp += MultiColors_MouseLeftButtonUp;
						gr_MultiColors_Childs.Children.Add(path4);
					}
				}
				break;
			}
			case 4:
			{
				TextBlock textBlock = new TextBlock();
				string text2 = (textBlock.Text = (string)FindResource("StringSlot") + createdCount);
				textBlock.Style = (Style)FindResource("style_tb_slot");
				Thickness margin = default(Thickness);
				margin.Top = 62 * (createdCount - 1) + 10;
				textBlock.Margin = margin;
				gr_MultiColors_Childs.Children.Add(textBlock);
				if (sOption == "FiveColor")
				{
					for (int i = 0; i < 5; i++)
					{
						System.Windows.Shapes.Path path = new System.Windows.Shapes.Path();
						path.Name = "MultiColor_" + slotIndex + "_" + i;
						List<List<int>> colors = _DRAM_colors.ElementAt(3).Value.colors;
						byte b = Convert.ToByte(colors[i].ElementAt(0));
						byte g = Convert.ToByte(colors[i].ElementAt(1));
						byte b2 = Convert.ToByte(colors[i].ElementAt(2));
						path.Fill = new SolidColorBrush(System.Windows.Media.Color.FromRgb(b, g, b2));
						Geometry geometry2 = (path.Data = Geometry.Parse("M0, 0 L52, 0 L52, 52 L0, 52"));
						Thickness margin2 = default(Thickness);
						margin2.Left = 430 - 64 * i;
						margin2.Top = 62 * (createdCount - 1);
						path.Margin = margin2;
						path.MouseLeftButtonUp += MultiColors_MouseLeftButtonUp;
						gr_MultiColors_Childs.Children.Add(path);
					}
				}
				break;
			}
			}
		}
		if (createdCount == deviceCount)
		{
			createdCount = 0;
			deviceCount = 0;
		}
	}

	private void Slider_Speed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
	{
		speedValue = Convert.ToInt32(Slider_Speed.Value);
	}

	private void Slider_Speed_MouseUp(object sender, MouseButtonEventArgs e)
	{
		Thread.Sleep(200);
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Slider_Brightness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
	{
		brightnessValue = Convert.ToInt32(Slider_Brightness.Value);
	}

	private void Slider_Brightness_MouseUp(object sender, MouseButtonEventArgs e)
	{
		Thread.Sleep(200);
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Slider_Darkness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
	{
		darknessValue = Convert.ToInt32(Slider_Darkness.Value);
	}

	private void Slider_Darkness_MouseUp(object sender, MouseButtonEventArgs e)
	{
		Thread.Sleep(200);
		Send_Setting();
		Profile_Was_Changed();
	}

	public Func<object> ColorChangeCallBack()
	{
		if (bPalette)
		{
			if (ColorPalette.bColor_TextChanged && sOption == "OneColor")
			{
				if (!bRandom)
				{
					Send_Setting();
				}
				base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
				ColorPalette.bColor_TextChanged = false;
			}
			else if (ColorPalette.bColor_TextChanged && sOption == "FiveColor")
			{
				base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
				ColorPalette.bColor_TextChanged = false;
			}
		}
		return null;
	}

	private void MultiColors_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
	{
		(sender as System.Windows.Shapes.Path).Fill = new SolidColorBrush(System.Windows.Media.Color.FromRgb(ColorPalette.GetCurrentRGB[0], ColorPalette.GetCurrentRGB[1], ColorPalette.GetCurrentRGB[2]));
		string[] array = ((System.Windows.Shapes.Path)sender).Name.Split('_');
		if (array.Length >= 3)
		{
			int num = Convert.ToInt32(array[2]);
			if (_DRAM_colors.ContainsKey("slot_" + array[1]))
			{
				if (_DRAM_colors["slot_" + array[1]].colors.Count > num)
				{
					_DRAM_colors["slot_" + array[1]].colors[num] = new List<int>
					{
						Convert.ToInt32(ColorPalette.GetCurrentRGB[0]),
						Convert.ToInt32(ColorPalette.GetCurrentRGB[1]),
						Convert.ToInt32(ColorPalette.GetCurrentRGB[2])
					};
				}
				base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
			}
		}
		Send_Setting();
	}

	public void ColorPaletteEnable(bool state)
	{
		ColorPalette.IsEnabled = state;
		if (!state)
		{
			ColorPalette.SetSelectionEnable(4, enabled: false);
			ColorPalette.Canvas_Main.IsEnabled = false;
			canOneColor.Visibility = Visibility.Hidden;
			bt_Delete_Circle_Color.Visibility = Visibility.Hidden;
		}
		else if (state)
		{
			ColorPalette.Canvas_Main.IsEnabled = true;
			canOneColor.Visibility = Visibility.Visible;
			if (_DRAM_colors != null && flag_FirstSetting)
			{
				string value = _DRAM_colors.ElementAt(0).Value.colors.ElementAt(0).ElementAt(0).ToString();
				string value2 = _DRAM_colors.ElementAt(0).Value.colors.ElementAt(0).ElementAt(1).ToString();
				string value3 = _DRAM_colors.ElementAt(0).Value.colors.ElementAt(0).ElementAt(2).ToString();
				ColorPalette.Array_CurrentColorValue[0] = Convert.ToByte(value);
				ColorPalette.Array_CurrentColorValue[1] = Convert.ToByte(value2);
				ColorPalette.Array_CurrentColorValue[2] = Convert.ToByte(value3);
				ColorPalette.Ellipse_CurrentColor.Fill = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Convert.ToByte(value), Convert.ToByte(value2), Convert.ToByte(value3)));
				ColorPalette.SyncTextColor();
			}
			else
			{
				ColorPalette.Ellipse_CurrentColor.Fill = new SolidColorBrush(System.Windows.Media.Color.FromRgb(ColorPalette.GetCurrentRGB[0], ColorPalette.GetCurrentRGB[1], ColorPalette.GetCurrentRGB[2]));
			}
		}
	}

	private void SupportType_default()
	{
		sOption = "DefaultColor";
		Button_DefaultColor.IsEnabled = true;
		Button_DefaultColor.IsChecked = true;
		Button_OneColor.IsChecked = false;
		Button_FiveColor.IsChecked = false;
		Button_RandomColor.Visibility = Visibility.Hidden;
		bPalette = false;
		ColorPaletteEnable(bPalette);
		canMultiColors.Visibility = Visibility.Hidden;
		DarknessSettingOff();
	}

	private void SupportType_AllOff()
	{
		Button_Running.IsEnabled = false;
		Button_Running.IsChecked = false;
		Button_Breath.IsEnabled = false;
		Button_Breath.IsChecked = false;
		Button_Rainbow.IsEnabled = false;
		Button_Rainbow.IsChecked = false;
		Button_Meteor.IsEnabled = false;
		Button_Meteor.IsChecked = false;
		Button_Blink.IsChecked = false;
		Button_Blink.IsEnabled = false;
		Button_Static.IsChecked = false;
		Button_Static.IsEnabled = false;
		Button_DoubleBlink.IsChecked = false;
		Button_DoubleBlink.IsEnabled = false;
		Button_ColorCycle.IsEnabled = false;
		Button_ColorCycle.IsChecked = false;
		Button_DefaultColor.IsEnabled = false;
		Button_DefaultColor.IsChecked = false;
		Button_OneColor.IsEnabled = false;
		Button_OneColor.IsChecked = false;
		Button_FiveColor.IsEnabled = false;
		Button_FiveColor.IsChecked = false;
		Button_RandomColor.Visibility = Visibility.Hidden;
		bPalette = false;
		ColorPaletteEnable(bPalette);
		canMultiColors.Visibility = Visibility.Hidden;
		SpeedSettingOff();
		BrightnessSettingOff();
		DarknessSettingOff();
		sMode = "all_off";
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
	}

	private void SupportType_AllOn()
	{
		Button_Running.IsEnabled = true;
		Button_Running.IsChecked = false;
		Button_Breath.IsEnabled = true;
		Button_Breath.IsChecked = false;
		Button_Rainbow.IsEnabled = true;
		Button_Rainbow.IsChecked = true;
		Button_Meteor.IsEnabled = true;
		Button_Meteor.IsChecked = false;
		Button_Blink.IsChecked = false;
		Button_Blink.IsEnabled = true;
		Button_Static.IsChecked = false;
		Button_Static.IsEnabled = true;
		Button_DoubleBlink.IsChecked = false;
		Button_DoubleBlink.IsEnabled = true;
		Button_ColorCycle.IsEnabled = true;
		Button_ColorCycle.IsChecked = false;
		Button_DefaultColor.IsEnabled = true;
		Button_DefaultColor.IsChecked = true;
		Button_OneColor.IsEnabled = false;
		Button_OneColor.IsChecked = false;
		Button_FiveColor.IsEnabled = false;
		Button_FiveColor.IsChecked = false;
		Button_RandomColor.Visibility = Visibility.Hidden;
		bPalette = false;
		ColorPaletteEnable(bPalette);
		canMultiColors.Visibility = Visibility.Hidden;
		SpeedSettingOn();
		BrightnessSettingOn();
		DarknessSettingOff();
		Slider_Speed.Value = 97.0;
		speedValue = Convert.ToInt32(Slider_Speed.Value);
		Slider_Brightness.Value = 100.0;
		brightnessValue = Convert.ToInt32(Slider_Brightness.Value);
		sMode = "rainbow";
		sOption = "DefaultColor";
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
	}

	private void SpeedSettingOn()
	{
		TextBlock_Speed.Visibility = Visibility.Visible;
		Slider_Speed.Visibility = Visibility.Visible;
		TextBlock_Low.Visibility = Visibility.Visible;
		TextBlock_Fast.Visibility = Visibility.Visible;
	}

	private void SpeedSettingOff()
	{
		TextBlock_Speed.Visibility = Visibility.Hidden;
		Slider_Speed.Visibility = Visibility.Hidden;
		TextBlock_Low.Visibility = Visibility.Hidden;
		TextBlock_Fast.Visibility = Visibility.Hidden;
	}

	private void BrightnessSettingOn()
	{
		TextBlock_Brightness.Visibility = Visibility.Visible;
		Slider_Brightness.Visibility = Visibility.Visible;
		TextBlock_Dark.Visibility = Visibility.Visible;
		TextBlock_Bright.Visibility = Visibility.Visible;
	}

	private void BrightnessSettingOff()
	{
		TextBlock_Brightness.Visibility = Visibility.Hidden;
		Slider_Brightness.Visibility = Visibility.Hidden;
		TextBlock_Dark.Visibility = Visibility.Hidden;
		TextBlock_Bright.Visibility = Visibility.Hidden;
	}

	private void DarknessSettingOn()
	{
		TextBlock_Darknesss.Visibility = Visibility.Visible;
		Slider_Darkness.Visibility = Visibility.Visible;
		TextBlock_Darkness_Dark.Visibility = Visibility.Visible;
		TextBlock_Darkness_Bright.Visibility = Visibility.Visible;
	}

	private void DarknessSettingOff()
	{
		TextBlock_Darknesss.Visibility = Visibility.Hidden;
		Slider_Darkness.Visibility = Visibility.Hidden;
		TextBlock_Darkness_Dark.Visibility = Visibility.Hidden;
		TextBlock_Darkness_Bright.Visibility = Visibility.Hidden;
	}

	private void Button_Running_Click(object sender, RoutedEventArgs e)
	{
		SupportType_default();
		SpeedSettingOn();
		BrightnessSettingOn();
		Slider_Speed.Value = 98.0;
		speedValue = Convert.ToInt32(Slider_Speed.Value);
		Slider_Brightness.Value = 100.0;
		brightnessValue = Convert.ToInt32(Slider_Brightness.Value);
		sMode = "running";
		gr_MultiColors_Childs.Children.Clear();
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_Blink_Click(object sender, RoutedEventArgs e)
	{
		SupportType_default();
		SpeedSettingOn();
		BrightnessSettingOn();
		Slider_Speed.Value = 50.0;
		speedValue = Convert.ToInt32(Slider_Speed.Value);
		Slider_Brightness.Value = 100.0;
		brightnessValue = Convert.ToInt32(Slider_Brightness.Value);
		sMode = "blink";
		gr_MultiColors_Childs.Children.Clear();
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_Breath_Click(object sender, RoutedEventArgs e)
	{
		SupportType_default();
		Button_FiveColor.IsEnabled = false;
		SpeedSettingOn();
		BrightnessSettingOn();
		DarknessSettingOn();
		Slider_Speed.Value = 37.0;
		speedValue = Convert.ToInt32(Slider_Speed.Value);
		Slider_Brightness.Value = 100.0;
		brightnessValue = Convert.ToInt32(Slider_Brightness.Value);
		Slider_Darkness.Value = 0.0;
		darknessValue = Convert.ToInt32(Slider_Darkness.Value);
		sMode = "breath";
		gr_MultiColors_Childs.Children.Clear();
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_Static_Click(object sender, RoutedEventArgs e)
	{
		SupportType_default();
		SpeedSettingOff();
		BrightnessSettingOff();
		Slider_Brightness.Value = 100.0;
		brightnessValue = Convert.ToInt32(Slider_Brightness.Value);
		Button_RandomColor.Visibility = Visibility.Hidden;
		bPalette = false;
		ColorPaletteEnable(bPalette);
		sMode = "static_color";
		gr_MultiColors_Childs.Children.Clear();
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_Rainbow_Click(object sender, RoutedEventArgs e)
	{
		SupportType_default();
		Button_OneColor.IsEnabled = false;
		Button_FiveColor.IsEnabled = false;
		SpeedSettingOn();
		BrightnessSettingOn();
		Slider_Speed.Value = 97.0;
		speedValue = Convert.ToInt32(Slider_Speed.Value);
		Slider_Brightness.Value = 100.0;
		brightnessValue = Convert.ToInt32(Slider_Brightness.Value);
		sMode = "rainbow";
		gr_MultiColors_Childs.Children.Clear();
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_DoubleBlink_Click(object sender, RoutedEventArgs e)
	{
		SupportType_default();
		SpeedSettingOn();
		BrightnessSettingOn();
		Slider_Speed.Value = 50.0;
		speedValue = Convert.ToInt32(Slider_Speed.Value);
		Slider_Brightness.Value = 100.0;
		brightnessValue = Convert.ToInt32(Slider_Brightness.Value);
		sMode = "double_blink";
		gr_MultiColors_Childs.Children.Clear();
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_Meteor_Click(object sender, RoutedEventArgs e)
	{
		SupportType_default();
		Button_FiveColor.IsEnabled = false;
		SpeedSettingOn();
		BrightnessSettingOff();
		Slider_Speed.Value = 98.0;
		speedValue = Convert.ToInt32(Slider_Speed.Value);
		Slider_Brightness.Value = 100.0;
		brightnessValue = Convert.ToInt32(Slider_Brightness.Value);
		sMode = "meteor";
		gr_MultiColors_Childs.Children.Clear();
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_ColorCycle_Click(object sender, RoutedEventArgs e)
	{
		SupportType_default();
		Button_OneColor.IsEnabled = false;
		Button_FiveColor.IsEnabled = false;
		SpeedSettingOn();
		BrightnessSettingOn();
		Slider_Speed.Value = 50.0;
		speedValue = Convert.ToInt32(Slider_Speed.Value);
		Slider_Brightness.Value = 100.0;
		brightnessValue = Convert.ToInt32(Slider_Brightness.Value);
		sMode = "color_cycle";
		gr_MultiColors_Childs.Children.Clear();
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_DefaultColor_Click(object sender, RoutedEventArgs e)
	{
		canMultiColors.Visibility = Visibility.Hidden;
		if (Button_Running.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (Button_Breath.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
			DarknessSettingOn();
		}
		else if (Button_Rainbow.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (Button_Meteor.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOff();
		}
		else if (Button_Blink.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (Button_Static.IsChecked == true)
		{
			SpeedSettingOff();
			BrightnessSettingOff();
		}
		else if (Button_DoubleBlink.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (Button_ColorCycle.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		Button_RandomColor.Visibility = Visibility.Hidden;
		bPalette = false;
		ColorPaletteEnable(bPalette);
		sOption = "DefaultColor";
		gr_MultiColors_Childs.Children.Clear();
		base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_OneColor_Click(object sender, RoutedEventArgs e)
	{
		canMultiColors.Visibility = Visibility.Hidden;
		Button_RandomColor.Visibility = Visibility.Visible;
		if (Button_Running.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (Button_Breath.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
			DarknessSettingOn();
		}
		else if (Button_Rainbow.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOff();
		}
		else if (Button_Meteor.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOff();
		}
		else if (Button_Blink.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (Button_Static.IsChecked == true)
		{
			SpeedSettingOff();
			BrightnessSettingOn();
		}
		else if (Button_DoubleBlink.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
		}
		else if (Button_ColorCycle.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOff();
		}
		bPalette = true;
		ColorPaletteEnable(bPalette);
		sOption = "OneColor";
		gr_MultiColors_Childs.Children.Clear();
		try
		{
			_DRAM_colors = new Dictionary<string, DRAMCtrlColorObj>();
			for (int i = 0; i < _DRAMInfoRootObj.root.dram.Count; i++)
			{
				List<List<int>> colors = new List<List<int>>
				{
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					}
				};
				_DRAM_colors.Add(_DRAMInfoRootObj.root.dram.ElementAt(i).Key, new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(i).Value.index, colors));
			}
			base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		}
		catch
		{
		}
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_FiveColor_Click(object sender, RoutedEventArgs e)
	{
		if (Button_Running.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
			canMultiColors.Visibility = Visibility.Visible;
		}
		else if (Button_Breath.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
			DarknessSettingOn();
			canMultiColors.Visibility = Visibility.Visible;
		}
		else if (Button_Rainbow.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOff();
		}
		else if (Button_Meteor.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOff();
		}
		else if (Button_Blink.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
			canMultiColors.Visibility = Visibility.Visible;
		}
		else if (Button_Static.IsChecked == true)
		{
			SpeedSettingOff();
			BrightnessSettingOn();
			canMultiColors.Visibility = Visibility.Visible;
		}
		else if (Button_DoubleBlink.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOn();
			canMultiColors.Visibility = Visibility.Visible;
		}
		else if (Button_ColorCycle.IsChecked == true)
		{
			SpeedSettingOn();
			BrightnessSettingOff();
		}
		Button_RandomColor.Visibility = Visibility.Visible;
		bPalette = true;
		ColorPaletteEnable(bPalette);
		sOption = "FiveColor";
		gr_MultiColors_Childs.Children.Clear();
		try
		{
			_DRAM_colors = new Dictionary<string, DRAMCtrlColorObj>();
			bool flag = false;
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			for (int i = 0; i < _DRAMInfoRootObj.root.dram.Count; i++)
			{
				List<List<int>> colors = new List<List<int>>
				{
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					}
				};
				string key = "slot_";
				int index = 0;
				string[] array = _DRAMInfoRootObj.root.dram.ElementAt(i).Key.Split('_');
				if (i + 1 < _DRAMInfoRootObj.root.dram.Count)
				{
					string[] array2 = _DRAMInfoRootObj.root.dram.ElementAt(i + 1).Key.Split('_');
					if (array.Length >= 2 && array2.Length >= 2)
					{
						num = Convert.ToInt32(array[1]);
						num2 = Convert.ToInt32(array2[1]);
						if (num < num2)
						{
							if (flag)
							{
								key = "slot_" + num3;
								index = num3;
								flag = false;
							}
							else
							{
								key = "slot_" + num;
								index = num;
								flag = false;
							}
						}
						else
						{
							key = "slot_" + num2;
							index = num2;
							flag = true;
							num3 = num;
						}
					}
				}
				else
				{
					num = Convert.ToInt32(array[1]);
					key = "slot_" + num;
					index = num;
				}
				_DRAM_colors.Add(key, new DRAMCtrlColorObj(index, colors));
			}
			base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		}
		catch
		{
		}
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Window_SourceInitialized(object sender, EventArgs e)
	{
		HwndSource.FromHwnd(new WindowInteropHelper(this).Handle).AddHook(WndProc);
	}

	private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
	{
		if (msg == App.WM_SHOWAPP)
		{
			Show();
			base.WindowState = WindowState.Normal;
		}
		return IntPtr.Zero;
	}

	private void Button_LEDlightOnOff_Unchecked(object sender, RoutedEventArgs e)
	{
		SupportType_AllOn();
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_LEDlightOnOff_Checked(object sender, RoutedEventArgs e)
	{
		SupportType_AllOff();
		Send_Setting();
		Profile_Was_Changed();
	}

	private void cv_ColorPalette_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
	{
		if (!(sOption == "OneColor"))
		{
			return;
		}
		gr_MultiColors_Childs.Children.Clear();
		try
		{
			_DRAM_colors = new Dictionary<string, DRAMCtrlColorObj>();
			for (int i = 0; i < _DRAMInfoRootObj.root.dram.Count; i++)
			{
				List<List<int>> colors = new List<List<int>>
				{
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					},
					new List<int>
					{
						ColorPalette.GetCurrentRGB[0],
						ColorPalette.GetCurrentRGB[1],
						ColorPalette.GetCurrentRGB[2]
					}
				};
				_DRAM_colors.Add(_DRAMInfoRootObj.root.dram.ElementAt(i).Key, new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(i).Value.index, colors));
			}
			base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
		}
		catch
		{
		}
		Send_Setting();
		Profile_Was_Changed();
	}

	private void bt_Circle_Color_Click(object sender, RoutedEventArgs e)
	{
		System.Windows.Media.Color color = ((SolidColorBrush)((System.Windows.Controls.Button)sender).Background).Color;
		ColorPalette.Text_ColorR.Text = color.R.ToString();
		ColorPalette.Text_ColorG.Text = color.G.ToString();
		ColorPalette.Text_ColorB.Text = color.B.ToString();
		Profile_Was_Changed();
	}

	private void bt_Add_Circle_Color_Click(object sender, RoutedEventArgs e)
	{
		if (total_colors == 20)
		{
			return;
		}
		total_colors++;
		System.Windows.Controls.Button button = new System.Windows.Controls.Button();
		Circle_Color_list.Add(System.Windows.Media.Color.FromRgb(Convert.ToByte(ColorPalette.Text_ColorR.Text), Convert.ToByte(ColorPalette.Text_ColorG.Text), Convert.ToByte(ColorPalette.Text_ColorB.Text)));
		button.Background = new SolidColorBrush(Circle_Color_list.Last());
		button.Style = (Style)FindResource("style_bt_circle_color");
		Grid.SetColumn(button, index_Column);
		Grid.SetRow(button, index_Row);
		button.Click += bt_Circle_Color_Click;
		button.MouseRightButtonUp += bt_Circle_Color_MouseRightButtonUp;
		button.Tag = canOneColor.Children.Count;
		canOneColor.Children.Add(button);
		if (total_colors == 20)
		{
			bt_Add_Circle_Color.Visibility = Visibility.Hidden;
			return;
		}
		bt_Add_Circle_Color.Visibility = Visibility.Visible;
		if (index_Column < 7)
		{
			index_Column += 2;
		}
		else
		{
			index_Column = 1;
			index_Row += 2;
		}
		Grid.SetColumn(bt_Add_Circle_Color, index_Column);
		Grid.SetRow(bt_Add_Circle_Color, index_Row);
	}

	private void bt_Circle_Color_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
	{
		target_index_customize_color = (int)((System.Windows.Controls.Button)sender).Tag;
		System.Windows.Point position = e.GetPosition(canOneColor);
		bt_Delete_Circle_Color.Margin = new Thickness(canOneColor.Margin.Left + position.X, canOneColor.Margin.Top + position.Y, 0.0, 0.0);
		bt_Delete_Circle_Color.Visibility = Visibility.Visible;
	}

	private void bt_Delete_Circle_Color_Click(object sender, RoutedEventArgs e)
	{
		canOneColor.Children.RemoveAt(target_index_customize_color);
		Circle_Color_list.RemoveAt(target_index_customize_color - 11);
		for (int i = 11; i < canOneColor.Children.Count; i++)
		{
			((System.Windows.Controls.Button)canOneColor.Children[i]).Tag = i;
			Grid.SetColumn((System.Windows.Controls.Button)canOneColor.Children[i], (i - 1) % 4 * 2 + 1);
			Grid.SetRow((System.Windows.Controls.Button)canOneColor.Children[i], (i - 1) / 4 * 2 + 1);
		}
		index_Column = (canOneColor.Children.Count - 1) % 4 * 2 + 1;
		index_Row = (canOneColor.Children.Count - 1) / 4 * 2 + 1;
		Grid.SetColumn(bt_Add_Circle_Color, index_Column);
		Grid.SetRow(bt_Add_Circle_Color, index_Row);
		bt_Add_Circle_Color.Visibility = Visibility.Visible;
		bt_Delete_Circle_Color.Visibility = Visibility.Hidden;
		total_colors--;
	}

	private void Dc_main_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
	{
		if (bt_Delete_Circle_Color.Visibility != Visibility.Hidden)
		{
			bt_Delete_Circle_Color.Visibility = Visibility.Hidden;
			if (total_colors == 20)
			{
				bt_Add_Circle_Color.Visibility = Visibility.Hidden;
			}
			else
			{
				bt_Add_Circle_Color.Visibility = Visibility.Visible;
			}
		}
	}

	private void Button_Shutdown_Click(object sender, RoutedEventArgs e)
	{
		try
		{
			Environment.Exit(0);
		}
		catch
		{
		}
	}

	private void Button_RandomColor_Click(object sender, RoutedEventArgs e)
	{
		bRandom = true;
		rand = new Random();
		gr_MultiColors_Childs.Children.Clear();
		if (sOption == "OneColor" && Button_OneColor.IsChecked == true)
		{
			try
			{
				iRandom_R = rand.Next(0, 255);
				iRandom_G = rand.Next(0, 255);
				iRandom_B = rand.Next(0, 255);
				_DRAM_colors = new Dictionary<string, DRAMCtrlColorObj>();
				for (int i = 0; i < _DRAMInfoRootObj.root.dram.Count; i++)
				{
					List<List<int>> colors = new List<List<int>>
					{
						new List<int> { iRandom_R, iRandom_G, iRandom_B },
						new List<int> { iRandom_R, iRandom_G, iRandom_B },
						new List<int> { iRandom_R, iRandom_G, iRandom_B },
						new List<int> { iRandom_R, iRandom_G, iRandom_B },
						new List<int> { iRandom_R, iRandom_G, iRandom_B }
					};
					_DRAM_colors.Add(_DRAMInfoRootObj.root.dram.ElementAt(i).Key, new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(i).Value.index, colors));
				}
				ColorPalette.Text_ColorR.Text = iRandom_R.ToString();
				ColorPalette.Text_ColorG.Text = iRandom_G.ToString();
				ColorPalette.Text_ColorB.Text = iRandom_B.ToString();
				base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
			}
			catch
			{
			}
		}
		else if (sOption == "FiveColor" && Button_FiveColor.IsChecked == true)
		{
			try
			{
				_DRAM_colors = new Dictionary<string, DRAMCtrlColorObj>();
				for (int j = 0; j < _DRAMInfoRootObj.root.dram.Count; j++)
				{
					List<List<int>> list = new List<List<int>>();
					for (int k = 0; k < 5; k++)
					{
						int item = rand.Next(128, 256);
						int item2 = rand.Next(128, 256);
						switch (rand.Next(0, 3))
						{
						case 0:
							list.Add(new List<int> { 0, item, item2 });
							break;
						case 1:
							list.Add(new List<int> { item, 0, item2 });
							break;
						default:
							list.Add(new List<int> { item, item2, 0 });
							break;
						}
					}
					_DRAM_colors.Add(_DRAMInfoRootObj.root.dram.ElementAt(j).Key, new DRAMCtrlColorObj(_DRAMInfoRootObj.root.dram.ElementAt(j).Value.index, list));
				}
				base.Dispatcher.Invoke(DispatcherPriority.Normal, new UpdateDeviceInfoUIDispatcherDelegate(UpdateDeviceInfoUI));
			}
			catch
			{
			}
		}
		Send_Setting();
		Profile_Was_Changed();
	}

	private void Button_Apply_Click(object sender, RoutedEventArgs e)
	{
		SaveProfile_Device();
	}

	private void Button_Reset_Click(object sender, RoutedEventArgs e)
	{
		Button_LEDlightOnOff.IsChecked = false;
		SupportType_AllOn();
		Send_Setting();
		Profile_Was_Changed();
	}

	[DebuggerNonUserCode]
	[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
	public void InitializeComponent()
	{
		if (!_contentLoaded)
		{
			_contentLoaded = true;
			Uri resourceLocator = new Uri("/FURYCTRL;component/mainwindow.xaml", UriKind.Relative);
			System.Windows.Application.LoadComponent(this, resourceLocator);
		}
	}

	[DebuggerNonUserCode]
	[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
	[EditorBrowsable(EditorBrowsableState.Never)]
	void IComponentConnector.Connect(int connectionId, object target)
	{
		switch (connectionId)
		{
		case 1:
			((MainWindow)target).Closing += Window_Closing;
			((MainWindow)target).Loaded += Window_Loaded;
			((MainWindow)target).SizeChanged += Window_SizeChanged;
			((MainWindow)target).MouseUp += Window_MouseUp;
			((MainWindow)target).SourceInitialized += Window_SourceInitialized;
			break;
		case 2:
			dc_main = (Grid)target;
			dc_main.MouseLeftButtonUp += Dc_main_MouseLeftButtonUp;
			break;
		case 3:
			ThumbTop = (Thumb)target;
			ThumbTop.PreviewMouseLeftButtonDown += ThumbTop_Move;
			break;
		case 4:
			Img_Logo = (System.Windows.Controls.Image)target;
			break;
		case 5:
			Button_Profile = (System.Windows.Controls.Button)target;
			Button_Profile.Click += Button_Profile_Click;
			break;
		case 6:
			Button_Infomation = (System.Windows.Controls.Button)target;
			Button_Infomation.Click += Button_Infomation_Click;
			break;
		case 7:
			gr_SwitchLED = (Grid)target;
			break;
		case 8:
			TextBlock_LED = (TextBlock)target;
			break;
		case 9:
			Button_LEDlightOnOff = (System.Windows.Controls.CheckBox)target;
			Button_LEDlightOnOff.Checked += Button_LEDlightOnOff_Checked;
			Button_LEDlightOnOff.Unchecked += Button_LEDlightOnOff_Unchecked;
			break;
		case 10:
			Button_Min = (System.Windows.Controls.Button)target;
			Button_Min.Click += Button_Min_Click;
			break;
		case 11:
			Button_Exit = (System.Windows.Controls.Button)target;
			Button_Exit.Click += Button_Close_Click;
			break;
		case 12:
			gr_DevicePanel = (Grid)target;
			break;
		case 13:
			gr_DevicePanel_Childs = (Grid)target;
			break;
		case 14:
			gr_DevicePanel_8DIMM_1 = (Grid)target;
			break;
		case 15:
			gr_DevicePanel_8DIMM_1_Childs = (Grid)target;
			break;
		case 16:
			gr_DevicePanel_8DIMM_2 = (Grid)target;
			break;
		case 17:
			gr_DevicePanel_8DIMM_2_Childs = (Grid)target;
			break;
		case 18:
			TextBlock_Effec = (TextBlock)target;
			break;
		case 19:
			Button_Running = (System.Windows.Controls.RadioButton)target;
			Button_Running.Click += Button_Running_Click;
			break;
		case 20:
			Button_Blink = (System.Windows.Controls.RadioButton)target;
			Button_Blink.Click += Button_Blink_Click;
			break;
		case 21:
			Button_Breath = (System.Windows.Controls.RadioButton)target;
			Button_Breath.Click += Button_Breath_Click;
			break;
		case 22:
			Button_Static = (System.Windows.Controls.RadioButton)target;
			Button_Static.Click += Button_Static_Click;
			break;
		case 23:
			Button_Rainbow = (System.Windows.Controls.RadioButton)target;
			Button_Rainbow.Click += Button_Rainbow_Click;
			break;
		case 24:
			Button_DoubleBlink = (System.Windows.Controls.RadioButton)target;
			Button_DoubleBlink.Click += Button_DoubleBlink_Click;
			break;
		case 25:
			Button_Meteor = (System.Windows.Controls.RadioButton)target;
			Button_Meteor.Click += Button_Meteor_Click;
			break;
		case 26:
			Button_ColorCycle = (System.Windows.Controls.RadioButton)target;
			Button_ColorCycle.Click += Button_ColorCycle_Click;
			break;
		case 27:
			Button_DefaultColor = (System.Windows.Controls.RadioButton)target;
			Button_DefaultColor.Click += Button_DefaultColor_Click;
			break;
		case 28:
			Button_OneColor = (System.Windows.Controls.RadioButton)target;
			Button_OneColor.Click += Button_OneColor_Click;
			break;
		case 29:
			Button_FiveColor = (System.Windows.Controls.RadioButton)target;
			Button_FiveColor.Click += Button_FiveColor_Click;
			break;
		case 30:
			((Canvas)target).MouseLeftButtonUp += cv_ColorPalette_MouseLeftButtonUp;
			break;
		case 31:
			ColorPalette = (UC_ColorPalette3)target;
			break;
		case 32:
			canOneColor = (Grid)target;
			break;
		case 33:
			bt_Add_Circle_Color = (System.Windows.Controls.Button)target;
			bt_Add_Circle_Color.Click += bt_Add_Circle_Color_Click;
			break;
		case 34:
			((System.Windows.Controls.Button)target).Click += bt_Circle_Color_Click;
			break;
		case 35:
			((System.Windows.Controls.Button)target).Click += bt_Circle_Color_Click;
			break;
		case 36:
			((System.Windows.Controls.Button)target).Click += bt_Circle_Color_Click;
			break;
		case 37:
			((System.Windows.Controls.Button)target).Click += bt_Circle_Color_Click;
			break;
		case 38:
			((System.Windows.Controls.Button)target).Click += bt_Circle_Color_Click;
			break;
		case 39:
			((System.Windows.Controls.Button)target).Click += bt_Circle_Color_Click;
			break;
		case 40:
			((System.Windows.Controls.Button)target).Click += bt_Circle_Color_Click;
			break;
		case 41:
			((System.Windows.Controls.Button)target).Click += bt_Circle_Color_Click;
			break;
		case 42:
			((System.Windows.Controls.Button)target).Click += bt_Circle_Color_Click;
			break;
		case 43:
			((System.Windows.Controls.Button)target).Click += bt_Circle_Color_Click;
			break;
		case 44:
			bt_Delete_Circle_Color = (System.Windows.Controls.Button)target;
			bt_Delete_Circle_Color.Click += bt_Delete_Circle_Color_Click;
			break;
		case 45:
			canMultiColors = (Grid)target;
			break;
		case 46:
			gr_MultiColors_Childs = (Grid)target;
			break;
		case 47:
			Button_RandomColor = (System.Windows.Controls.Button)target;
			Button_RandomColor.Click += Button_RandomColor_Click;
			break;
		case 48:
			TextBlock_Speed = (TextBlock)target;
			break;
		case 49:
			Slider_Speed = (Slider)target;
			Slider_Speed.ValueChanged += Slider_Speed_ValueChanged;
			Slider_Speed.PreviewMouseUp += Slider_Speed_MouseUp;
			break;
		case 50:
			TextBlock_Low = (TextBlock)target;
			break;
		case 51:
			TextBlock_Fast = (TextBlock)target;
			break;
		case 52:
			TextBlock_Brightness = (TextBlock)target;
			break;
		case 53:
			Slider_Brightness = (Slider)target;
			Slider_Brightness.ValueChanged += Slider_Brightness_ValueChanged;
			Slider_Brightness.PreviewMouseUp += Slider_Brightness_MouseUp;
			break;
		case 54:
			TextBlock_Dark = (TextBlock)target;
			break;
		case 55:
			TextBlock_Bright = (TextBlock)target;
			break;
		case 56:
			TextBlock_Darknesss = (TextBlock)target;
			break;
		case 57:
			Slider_Darkness = (Slider)target;
			Slider_Darkness.ValueChanged += Slider_Darkness_ValueChanged;
			Slider_Darkness.PreviewMouseUp += Slider_Darkness_MouseUp;
			break;
		case 58:
			TextBlock_Darkness_Dark = (TextBlock)target;
			break;
		case 59:
			TextBlock_Darkness_Bright = (TextBlock)target;
			break;
		case 60:
			Button_Rest = (System.Windows.Controls.Button)target;
			Button_Rest.Click += Button_Reset_Click;
			break;
		case 61:
			Button_Apply = (System.Windows.Controls.Button)target;
			Button_Apply.Click += Button_Apply_Click;
			break;
		case 62:
			gr_Profile_mask = (Grid)target;
			break;
		case 63:
			gr_Profile = (Grid)target;
			break;
		case 64:
			Button_Close_Profile = (System.Windows.Controls.Button)target;
			Button_Close_Profile.Click += Button_Close_Profile_Click;
			break;
		case 65:
			gr_Profile_list = (Grid)target;
			break;
		case 66:
			TextBox_Profile = (TextBlock)target;
			break;
		case 67:
			gr_profile_position1 = (Grid)target;
			break;
		case 68:
			gr_profile_position2 = (Grid)target;
			break;
		case 69:
			gr_profile_position3 = (Grid)target;
			break;
		case 70:
			gr_profile_position4 = (Grid)target;
			break;
		case 71:
			gr_profile_position5 = (Grid)target;
			break;
		case 72:
			gr_AddProfile = (Grid)target;
			break;
		case 73:
			Button_AddProfile = (System.Windows.Controls.Button)target;
			Button_AddProfile.Click += Button_AddProfile_Click;
			break;
		case 74:
			TextBox_Modify_Profile_FileName = (System.Windows.Controls.TextBox)target;
			TextBox_Modify_Profile_FileName.LostFocus += TextBox_Modify_Profile_FileName_LostFocus;
			TextBox_Modify_Profile_FileName.GotFocus += TextBox_Modify_Profile_FileName_GotFocus;
			TextBox_Modify_Profile_FileName.PreviewTextInput += tb_Modify_Profile_FileName_PreviewTextInput;
			TextBox_Modify_Profile_FileName.AddHandler(CommandManager.PreviewExecutedEvent, new ExecutedRoutedEventHandler(tb_Modify_Profile_FileName_PreviewExecuted));
			break;
		case 75:
			gr_Infomation_mask = (Grid)target;
			break;
		case 76:
			gr_Infomation = (Grid)target;
			break;
		case 77:
			Button_Close_Information = (System.Windows.Controls.Button)target;
			Button_Close_Information.Click += Button_Close_Information_Click;
			break;
		case 78:
			TextBox_Language = (TextBlock)target;
			break;
		case 79:
			cb_Language = (System.Windows.Controls.ComboBox)target;
			break;
		case 80:
			TextBox_Software = (TextBlock)target;
			break;
		case 81:
			TextBox_App_description = (TextBlock)target;
			break;
		case 82:
			TextBox_App_Copyright = (TextBlock)target;
			break;
		case 83:
			((Hyperlink)target).Click += Hyperlink_Privacy_Click;
			break;
		case 84:
			TextBox_URL_Privacy = (TextBlock)target;
			break;
		case 85:
			TextBox_Startup = (TextBlock)target;
			break;
		case 86:
			cb_keep_status = (System.Windows.Controls.CheckBox)target;
			cb_keep_status.Click += Cb_keep_status_Click;
			break;
		case 87:
			TextBox_Startup_Content = (TextBlock)target;
			break;
		case 88:
			ThumbTop_ForMask = (Thumb)target;
			ThumbTop_ForMask.PreviewMouseLeftButtonDown += ThumbTop_Move;
			break;
		case 89:
			gr_wait_mask = (Grid)target;
			break;
		case 90:
			tbx_waiting_message = (TextBlock)target;
			break;
		case 91:
			gr_eula_mask = (Grid)target;
			break;
		case 92:
			tb_Privacy_Page_Title = (TextBlock)target;
			break;
		case 93:
			sv_Privacy = (ScrollViewer)target;
			sv_Privacy.ScrollChanged += Sv_Privacy_ScrollChanged;
			break;
		case 94:
			tb_Privacy_Title = (TextBlock)target;
			break;
		case 95:
			tb_Privacy_Version = (TextBlock)target;
			break;
		case 96:
			tb_Privacy_Acction1 = (TextBlock)target;
			break;
		case 97:
			tb_Privacy_Acction2 = (TextBlock)target;
			break;
		case 98:
			tb_Privacy_Acction3_1 = (TextBlock)target;
			break;
		case 99:
			tb_Privacy_Acction3_2 = (TextBlock)target;
			break;
		case 100:
			tb_Privacy_Acction3_3 = (TextBlock)target;
			break;
		case 101:
			tb_Privacy_Acction3_4 = (TextBlock)target;
			break;
		case 102:
			tb_Privacy_Acction3_5 = (TextBlock)target;
			break;
		case 103:
			tb_Privacy_Acction3_6 = (TextBlock)target;
			break;
		case 104:
			tb_Privacy_Acction3_7 = (TextBlock)target;
			break;
		case 105:
			tb_Privacy_Acction3_8 = (TextBlock)target;
			break;
		case 106:
			tb_Privacy_Acction3_9 = (TextBlock)target;
			break;
		case 107:
			tb_Privacy_Content_Title1_1 = (TextBlock)target;
			break;
		case 108:
			tb_Privacy_Content_Title1_2 = (TextBlock)target;
			break;
		case 109:
			tb_Privacy_Content1 = (TextBlock)target;
			break;
		case 110:
			tb_Privacy_Content_Title2_1 = (TextBlock)target;
			break;
		case 111:
			tb_Privacy_Content_Title2_2 = (TextBlock)target;
			break;
		case 112:
			tb_Privacy_Content2 = (TextBlock)target;
			break;
		case 113:
			tb_Privacy_Content_Title3_1 = (TextBlock)target;
			break;
		case 114:
			tb_Privacy_Content_Title3_2 = (TextBlock)target;
			break;
		case 115:
			tb_Privacy_Content3 = (TextBlock)target;
			break;
		case 116:
			tb_Privacy_Content_Title4_1 = (TextBlock)target;
			break;
		case 117:
			tb_Privacy_Content_Title4_2 = (TextBlock)target;
			break;
		case 118:
			tb_Privacy_Content4 = (TextBlock)target;
			break;
		case 119:
			tb_Privacy_Content_Title5_1 = (TextBlock)target;
			break;
		case 120:
			tb_Privacy_Content_Title5_2 = (TextBlock)target;
			break;
		case 121:
			tb_Privacy_Content5_1 = (TextBlock)target;
			break;
		case 122:
			tb_Privacy_Content5_2 = (TextBlock)target;
			break;
		case 123:
			tb_Privacy_Content5_3 = (TextBlock)target;
			break;
		case 124:
			tb_Privacy_Content5_4 = (TextBlock)target;
			break;
		case 125:
			tb_Privacy_Content5_5 = (TextBlock)target;
			break;
		case 126:
			tb_Privacy_Content5_6 = (TextBlock)target;
			break;
		case 127:
			tb_Privacy_Content5_7 = (TextBlock)target;
			break;
		case 128:
			tb_Privacy_Content5_8 = (TextBlock)target;
			break;
		case 129:
			tb_Privacy_Content5_9 = (TextBlock)target;
			break;
		case 130:
			tb_Privacy_Content_Title6_1 = (TextBlock)target;
			break;
		case 131:
			tb_Privacy_Content_Title6_2 = (TextBlock)target;
			break;
		case 132:
			tb_Privacy_Content6 = (TextBlock)target;
			break;
		case 133:
			tb_Privacy_Content_Title7_1 = (TextBlock)target;
			break;
		case 134:
			tb_Privacy_Content_Title7_2 = (TextBlock)target;
			break;
		case 135:
			tb_Privacy_Content7 = (TextBlock)target;
			break;
		case 136:
			tb_Privacy_Content_Title8_1 = (TextBlock)target;
			break;
		case 137:
			tb_Privacy_Content_Title8_2 = (TextBlock)target;
			break;
		case 138:
			tb_Privacy_Content8 = (TextBlock)target;
			break;
		case 139:
			tb_Privacy_Content_Title9_1 = (TextBlock)target;
			break;
		case 140:
			tb_Privacy_Content_Title9_2 = (TextBlock)target;
			break;
		case 141:
			tb_Privacy_Content9 = (TextBlock)target;
			break;
		case 142:
			tb_Privacy_Content_Title10_1 = (TextBlock)target;
			break;
		case 143:
			tb_Privacy_Content_Title10_2 = (TextBlock)target;
			break;
		case 144:
			tb_Privacy_Content10 = (TextBlock)target;
			break;
		case 145:
			tb_Privacy_Content_Title11_1 = (TextBlock)target;
			break;
		case 146:
			tb_Privacy_Content_Title11_2 = (TextBlock)target;
			break;
		case 147:
			tb_Privacy_Content11 = (TextBlock)target;
			break;
		case 148:
			tb_Privacy_Content_Title12_1 = (TextBlock)target;
			break;
		case 149:
			tb_Privacy_Content_Title12_2 = (TextBlock)target;
			break;
		case 150:
			tb_Privacy_Content12_1 = (TextBlock)target;
			break;
		case 151:
			tb_Privacy_Content12_2 = (TextBlock)target;
			break;
		case 152:
			tb_Privacy_Content12_3 = (TextBlock)target;
			break;
		case 153:
			tb_Privacy_Content12_4 = (TextBlock)target;
			break;
		case 154:
			tb_Privacy_Content12_5 = (TextBlock)target;
			break;
		case 155:
			tb_Privacy_Content_Title13_1 = (TextBlock)target;
			break;
		case 156:
			tb_Privacy_Content_Title13_2 = (TextBlock)target;
			break;
		case 157:
			tb_Privacy_Content13 = (TextBlock)target;
			break;
		case 158:
			tb_Privacy_Content_Title14_1 = (TextBlock)target;
			break;
		case 159:
			tb_Privacy_Content_Title14_2 = (TextBlock)target;
			break;
		case 160:
			tb_Privacy_Content14 = (TextBlock)target;
			break;
		case 161:
			tb_Privacy_Content_Title15_1 = (TextBlock)target;
			break;
		case 162:
			tb_Privacy_Content_Title15_2 = (TextBlock)target;
			break;
		case 163:
			tb_Privacy_Content15 = (TextBlock)target;
			break;
		case 164:
			tb_Privacy_Content_Title16_1 = (TextBlock)target;
			break;
		case 165:
			tb_Privacy_Content_Title16_2 = (TextBlock)target;
			break;
		case 166:
			tb_Privacy_Content16 = (TextBlock)target;
			break;
		case 167:
			tb_Privacy_Content_Title17_1 = (TextBlock)target;
			break;
		case 168:
			tb_Privacy_Content_Title17_2 = (TextBlock)target;
			break;
		case 169:
			tb_Privacy_Content17_1 = (TextBlock)target;
			break;
		case 170:
			tb_Privacy_Content17_2 = (TextBlock)target;
			break;
		case 171:
			tb_Privacy_Content17_3 = (TextBlock)target;
			break;
		case 172:
			tb_Privacy_Content17_4 = (TextBlock)target;
			break;
		case 173:
			tb_Privacy_Content17_5 = (TextBlock)target;
			break;
		case 174:
			tb_Privacy_Content_Title18_1 = (TextBlock)target;
			break;
		case 175:
			tb_Privacy_Content_Title18_2 = (TextBlock)target;
			break;
		case 176:
			tb_Privacy_Content18_1 = (TextBlock)target;
			break;
		case 177:
			tb_Privacy_Content18_2 = (TextBlock)target;
			break;
		case 178:
			((Hyperlink)target).Click += Hyperlink_Privacy_Click;
			break;
		case 179:
			tb_Privacy_Content18_3 = (TextBlock)target;
			break;
		case 180:
			tb_Privacy_Content18_4 = (TextBlock)target;
			break;
		case 181:
			tb_Privacy_Content18_5 = (TextBlock)target;
			break;
		case 182:
			tb_Privacy_Content18_6 = (TextBlock)target;
			break;
		case 183:
			tb_Privacy_Content18_7 = (TextBlock)target;
			break;
		case 184:
			tb_Privacy_Content18_8 = (TextBlock)target;
			break;
		case 185:
			tb_Privacy_Content_Title19_1 = (TextBlock)target;
			break;
		case 186:
			tb_Privacy_Content_Title19_2 = (TextBlock)target;
			break;
		case 187:
			tb_Privacy_Content19 = (TextBlock)target;
			break;
		case 188:
			tb_Privacy_Content_Title20_1 = (TextBlock)target;
			break;
		case 189:
			tb_Privacy_Content_Title20_2 = (TextBlock)target;
			break;
		case 190:
			tb_Privacy_Content20 = (TextBlock)target;
			break;
		case 191:
			tb_Privacy_Content_Title21_1 = (TextBlock)target;
			break;
		case 192:
			tb_Privacy_Content_Title21_2 = (TextBlock)target;
			break;
		case 193:
			tb_Privacy_Content21 = (TextBlock)target;
			break;
		case 194:
			tb_Privacy_Content_Title22_1 = (TextBlock)target;
			break;
		case 195:
			tb_Privacy_Content_Title22_2 = (TextBlock)target;
			break;
		case 196:
			tb_Privacy_Content22 = (TextBlock)target;
			break;
		case 197:
			tb_Privacy_Content_Title23_1 = (TextBlock)target;
			break;
		case 198:
			tb_Privacy_Content_Title23_2 = (TextBlock)target;
			break;
		case 199:
			tb_Privacy_Content23 = (TextBlock)target;
			break;
		case 200:
			tb_Privacy_Content_Title24_1 = (TextBlock)target;
			break;
		case 201:
			tb_Privacy_Content_Title24_2 = (TextBlock)target;
			break;
		case 202:
			tb_Privacy_Content24 = (TextBlock)target;
			break;
		case 203:
			chk_ReadPrivacy = (System.Windows.Controls.CheckBox)target;
			chk_ReadPrivacy.Click += Chk_ReadPrivacy_Click;
			break;
		case 204:
			bt_apply = (System.Windows.Controls.Button)target;
			bt_apply.Click += bt_apply_Click;
			break;
		case 205:
			bt_cancel = (System.Windows.Controls.Button)target;
			bt_cancel.Click += bt_cancel_Click;
			break;
		case 206:
			gr_setup_progress_mask = (Grid)target;
			break;
		case 207:
			tb_setup_progress = (TextBlock)target;
			break;
		case 208:
			((System.Windows.Controls.Button)target).Click += Button_Shutdown_Click;
			break;
		case 209:
			gr_update_setup_progress_mask = (Grid)target;
			break;
		case 210:
			tb_update_setup_progress = (TextBlock)target;
			break;
		case 211:
			gr_wait_sdk_come_back_mask = (Grid)target;
			break;
		case 212:
			tb_wait_sdk_come_back = (TextBlock)target;
			break;
		case 213:
			((System.Windows.Controls.Button)target).Click += Button_Shutdown_Click;
			break;
		case 214:
			gr_reinstall_app_mask = (Grid)target;
			break;
		case 215:
			((System.Windows.Controls.Button)target).Click += Button_Shutdown_Click;
			break;
		default:
			_contentLoaded = true;
			break;
		}
	}
}
