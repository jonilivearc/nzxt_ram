using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Script.Serialization;

namespace FuryCTRL;

public class JsonClass
{
	public class NullPropertiesConverter : JavaScriptConverter
	{
		public override IEnumerable<Type> SupportedTypes => GetType().Assembly.GetTypes();

		public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			PropertyInfo[] properties = obj.GetType().GetProperties();
			foreach (PropertyInfo propertyInfo in properties)
			{
				bool flag = propertyInfo.IsDefined(typeof(ScriptIgnoreAttribute), inherit: true);
				object value = propertyInfo.GetValue(obj, BindingFlags.Public, null, null, null);
				if (value != null && !flag)
				{
					dictionary.Add(propertyInfo.Name, value);
				}
			}
			return dictionary;
		}
	}

	public static T JavaScriptDeserialize<T>(string jsonString)
	{
		return new JavaScriptSerializer().Deserialize<T>(jsonString);
	}

	public static string JavaScriptSerialize<T>(T t)
	{
		JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
		javaScriptSerializer.RegisterConverters(new JavaScriptConverter[1]
		{
			new NullPropertiesConverter()
		});
		return javaScriptSerializer.Serialize(t);
	}
}
