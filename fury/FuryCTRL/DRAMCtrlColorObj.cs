using System.Collections.Generic;

namespace FuryCTRL;

public class DRAMCtrlColorObj
{
	public int index { get; set; }

	public List<List<int>> colors { get; set; }

	public DRAMCtrlColorObj()
	{
	}

	public DRAMCtrlColorObj(int _index, List<List<int>> _colors)
	{
		index = _index;
		colors = _colors;
	}
}
