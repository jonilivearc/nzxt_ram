using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace FuryCTRL;

[DataContract(Name = "Online_GameData")]
public class Online_GameDataV2
{
	[DataContract(Name = "CommonDefine")]
	public class CommonDefine
	{
		[DataMember(Name = "Title")]
		public string Title { get; set; }

		[DataMember(Name = "Description")]
		public string Description { get; set; }

		public CommonDefine Clone()
		{
			return (CommonDefine)MemberwiseClone();
		}
	}

	[DataContract(Name = "GameData")]
	public class GameData
	{
		[DataMember(Name = "GUID")]
		public string GUID { get; set; }

		[DataMember(Name = "Platform")]
		public int Platform { get; set; }

		[DataMember(Name = "RegName")]
		public string RegName { get; set; }

		[DataMember(Name = "FullPath")]
		public string FullPath { get; set; }

		[DataMember(Name = "File")]
		public string File { get; set; }

		[DataMember(Name = "Display")]
		public string Display { get; set; }

		[DataMember(Name = "Icon")]
		public string Icon { get; set; }

		[DataMember(Name = "Description")]
		public string Description { get; set; }

		[DataMember(Name = "Type")]
		public int Type { get; set; }

		[DataMember(Name = "SOption")]
		public int[] SupportOption { get; set; }

		[DataMember(Name = "Enabled")]
		public bool Enabled { get; set; }

		[DataMember(Name = "Installed")]
		public bool Installed { get; set; }

		[DataMember(Name = "Option")]
		public bool[] Option { get; set; }
	}

	[DataMember(Name = "Enabled")]
	public bool Enabled { get; set; }

	[DataMember(Name = "DefaultIcon")]
	public List<string> List_DefaultIcon { get; set; }

	[DataMember(Name = "Option")]
	public List<CommonDefine> List_Option { get; set; }

	[DataMember(Name = "Data")]
	public List<GameData> List_GameData { get; set; }

	public Online_GameDataV2 Clone()
	{
		Online_GameDataV2 online_GameDataV = new Online_GameDataV2();
		online_GameDataV.Enabled = Enabled;
		online_GameDataV.List_DefaultIcon = List_DefaultIcon.ToList();
		online_GameDataV.List_GameData = new List<GameData>();
		foreach (GameData list_GameDatum in List_GameData)
		{
			GameData gameData = new GameData();
			gameData.GUID = list_GameDatum.GUID;
			gameData.Enabled = list_GameDatum.Enabled;
			gameData.Display = list_GameDatum.Display;
			gameData.Description = list_GameDatum.Description;
			gameData.File = list_GameDatum.File;
			gameData.FullPath = list_GameDatum.FullPath;
			gameData.Icon = list_GameDatum.Icon;
			gameData.Installed = list_GameDatum.Installed;
			gameData.Option = list_GameDatum.Option.ToArray();
			gameData.Platform = list_GameDatum.Platform;
			gameData.RegName = list_GameDatum.RegName;
			gameData.Type = list_GameDatum.Type;
			gameData.SupportOption = list_GameDatum.SupportOption.ToArray();
			online_GameDataV.List_GameData.Add(gameData);
		}
		online_GameDataV.List_Option = new List<CommonDefine>();
		foreach (CommonDefine item in List_Option)
		{
			online_GameDataV.List_Option.Add(item);
		}
		return online_GameDataV;
	}
}
