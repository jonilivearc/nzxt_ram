using System.Collections.Generic;
using System.Linq;

namespace FuryCTRL;

public static class LanguageList
{
	private static readonly List<LanguageItem> _languageList = new List<LanguageItem>(new LanguageItem[18]
	{
		new LanguageItem("Traditional Chinese", 1028u),
		new LanguageItem("German", 1031u),
		new LanguageItem("English (U.S.)", 1033u),
		new LanguageItem("English (Belize)", 2809u),
		new LanguageItem("French", 1036u),
		new LanguageItem("Italian", 1040u),
		new LanguageItem("Japanese", 1041u),
		new LanguageItem("Korean", 1042u),
		new LanguageItem("Polish", 1045u),
		new LanguageItem("Russian", 1049u),
		new LanguageItem("Thai", 1054u),
		new LanguageItem("Turkish", 1055u),
		new LanguageItem("Bahasa Indonesia", 1057u),
		new LanguageItem("Vietnamese", 1066u),
		new LanguageItem("Simple Chinese", 2052u),
		new LanguageItem("Portuguese", 2070u),
		new LanguageItem("Spanish", 3082u),
		new LanguageItem("Spanish (Latin America)", 22538u)
	});

	public static IEnumerable<LanguageItem> GetLanguages()
	{
		return _languageList;
	}

	public static LanguageItem GetLanguageItem_ID(int languageId)
	{
		return _languageList.SingleOrDefault((LanguageItem li) => li.Id == languageId);
	}

	public static LanguageItem GetLanguageItem_Name(string language)
	{
		return _languageList.SingleOrDefault((LanguageItem li) => li.Name.Equals(language));
	}
}
