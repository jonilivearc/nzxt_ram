namespace FuryCTRL;

public class DRAMKernelInfo
{
	public string version { get; set; }

	public string setup_path { get; set; }

	public string setup_args { get; set; }

	public DRAMKernelInfo()
	{
	}

	public DRAMKernelInfo(string _version, string _setup_path, string _setup_args)
	{
		version = _version;
		setup_path = _setup_path;
		setup_args = _setup_args;
	}
}
