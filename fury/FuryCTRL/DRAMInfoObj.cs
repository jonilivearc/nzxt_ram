namespace FuryCTRL;

public class DRAMInfoObj
{
	public int index { get; set; }

	public string manufac { get; set; }

	public string brand { get; set; }

	public string product { get; set; }
}
