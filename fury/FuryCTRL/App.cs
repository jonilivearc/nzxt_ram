using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;
using Windows.ApplicationModel;

namespace FuryCTRL;

public class App : Application
{
	public static readonly string sAppTitleName = ((AssemblyTitleAttribute)Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), inherit: true)[0]).Title;

	private Mutex _mutex;

	public const int HWND_BROADCAST = 65535;

	public static readonly int WM_SHOWAPP = RegisterWindowMessage("WM_SHOWAPP_FURYCONTROLLER");

	[DllImport("user32")]
	public static extern int RegisterWindowMessage(string message);

	[DllImport("user32")]
	public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);

	private async void Application_Startup(object sender, StartupEventArgs e)
	{
		if (e.Args.Length != 0)
		{
			string[] array = e.Args[0].Split(':');
			if (array != null && array.Length == 2)
			{
				e.Args[0] = array[1];
			}
			string text = e.Args[0];
			if (text == "Startup")
			{
				DataCenter.bStartupApp = true;
				Log.RecordLogWriter("[Startup] The auto startup = true", bFailed: false);
			}
		}
		try
		{
			_mutex = Mutex.OpenExisting(sAppTitleName);
			PostMessage((IntPtr)65535, WM_SHOWAPP, IntPtr.Zero, IntPtr.Zero);
			Environment.Exit(0);
			return;
		}
		catch (WaitHandleCannotBeOpenedException)
		{
			_mutex = new Mutex(initiallyOwned: true, sAppTitleName);
		}
		try
		{
			if (DataCenter.bStartUp)
			{
				StartupTask val = await StartupTask.GetAsync(DataCenter.TaskId);
				if (!File.Exists(DataCenter.Path_StartUp_Switch))
				{
					await val.RequestEnableAsync();
					Log.RecordLogWriter("[Startup] DisableStartup.dat is not exists", bFailed: false);
				}
				else
				{
					val.Disable();
					DataCenter.bStartupApp = false;
					Log.RecordLogWriter("[Startup] DisableStartup.dat is exists", bFailed: false);
				}
			}
		}
		catch (Exception ex2)
		{
			Log.RecordLogWriter("[Startup]" + ex2, bFailed: true);
		}
		DataCenter.bShowGDPR = !File.Exists(DataCenter.Path_GDPR_Switch);
		DataCenter._MainWindow = new MainWindow();
		DataCenter._MainWindow.Title = DataCenter.ProjectName;
		DataCenter._MainWindow.Icon = new BitmapImage(DataCenter.Uri_AppIcon);
		if (DataCenter.bStartupApp)
		{
			Log.RecordLogWriter("[Startup] Fisrt startup, hide the taskbar", bFailed: false);
			DataCenter._MainWindow.ShowInTaskbar = false;
			DataCenter._MainWindow.Opacity = 0.0;
		}
	}

	[DebuggerNonUserCode]
	[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
	public void InitializeComponent()
	{
		base.Startup += Application_Startup;
		base.StartupUri = new Uri("MainWindow.xaml", UriKind.Relative);
	}

	[STAThread]
	[DebuggerNonUserCode]
	[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
	public static void Main()
	{
		App app = new App();
		app.InitializeComponent();
		app.Run();
	}
}
