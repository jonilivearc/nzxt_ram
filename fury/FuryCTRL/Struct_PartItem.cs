namespace FuryCTRL;

public struct Struct_PartItem
{
	public int Index;

	public string ItemType;

	public int LEDEffect;

	public string MainDevice;

	public EnumChipest[] Chipest;

	public string ShowName;

	public EnumDeviceName[] DeviceName;

	public int StyleIsEnabled;

	public EnumStyle[] Style;

	public int[] StyleID;

	public string[] StyleText;

	public int StyleSelectIndex;

	public int[] ExtendParameter1;

	public int[] DefaultRGBValue;

	public int[] RGBValue;

	public int RGBMode;

	public int[] Speed;

	public int[] Brightness;

	public int Filter_Device;

	public int[] Filter_Style;
}
