using System.Collections.Generic;

namespace FuryCTRL;

public class DRAMCtrlObj
{
	public string mode { get; set; }

	public string ctrl_mode { get; set; }

	public string other_option { get; set; }

	public int speed { get; set; }

	public int brightness { get; set; }

	public int darkness { get; set; }

	public Dictionary<string, DRAMCtrlColorObj> ctrl_color { get; set; }

	public DRAMCtrlObj()
	{
	}

	public DRAMCtrlObj(string _mode, string _ctrl_mode)
	{
		mode = _mode;
		ctrl_mode = _ctrl_mode;
	}

	public DRAMCtrlObj(string _mode, string _ctrl_mode, int _speed, int _brightness)
	{
		mode = _mode;
		ctrl_mode = _ctrl_mode;
		speed = _speed;
		brightness = _brightness;
	}

	public DRAMCtrlObj(string _mode, string _ctrl_mode, int _speed, int _brightness, int _darkness)
	{
		mode = _mode;
		ctrl_mode = _ctrl_mode;
		speed = _speed;
		brightness = _brightness;
		darkness = _darkness;
	}
}
