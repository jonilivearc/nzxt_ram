using System.Collections.Generic;

namespace FuryCTRL;

public class PartItem
{
	public int Index { get; set; }

	public string ItemType { get; set; }

	public bool LEDEffect { get; set; }

	public string MainDevice { get; set; }

	public List<EnumChipest> Chipest { get; set; }

	public string ShowName { get; set; }

	public List<EnumDeviceName> DeviceName { get; set; }

	public bool StyleIsEnabled { get; set; }

	public List<EnumStyle> Style { get; set; }

	public List<int> StyleID { get; set; }

	public List<string> StyleText { get; set; }

	public int StyleSelectIndex { get; set; }

	public object ExtendParameter1 { get; set; }

	public byte[] DefaultRGBValue { get; set; }

	public byte[] RGBValue { get; set; }

	public bool RGBMode { get; set; }

	public int[] Speed { get; set; }

	public int[] Brightness { get; set; }

	public int Filter_Device { get; set; }

	public List<int> Filter_Style { get; set; }
}
