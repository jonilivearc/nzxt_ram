namespace FuryCTRL;

public enum PowerEvent
{
	Unknow = 0,
	EnteringSuspend = 4,
	ResumeFromSuspend = 7,
	PowerStatusChange = 16,
	OEMEvent = 17,
	ResumeAutomatic = 24
}
