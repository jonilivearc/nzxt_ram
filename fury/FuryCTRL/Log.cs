using System;
using System.IO;

namespace FuryCTRL;

internal class Log
{
	private static object Recordlocker = new object();

	private static string RecordLogPath = string.Format("{0}\\Kingston Fury\\FuryCTRL\\Log\\CMDLog_{1}.log", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), DateTime.Now.ToString("MM_dd_yyyy"));

	private static int Log_MaxSize = 5242880;

	public static void RecordLogWriter(string sMessage, bool bFailed)
	{
		if (!File.Exists(RecordLogPath))
		{
			Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\Kingston Fury\\FuryCTRL\\Log\\");
			using (File.Create(RecordLogPath))
			{
			}
		}
		lock (Recordlocker)
		{
			TextWriter textWriter = TextWriter.Synchronized(File.AppendText(RecordLogPath));
			if (bFailed)
			{
				textWriter.WriteLine(string.Format("{0} | [ERROR] {1}", DateTime.Now.ToString("MM-dd-yyyy hh:mm:ss.fff"), sMessage));
			}
			else
			{
				textWriter.WriteLine(string.Format("{0} | {1}", DateTime.Now.ToString("MM-dd-yyyy hh:mm:ss.fff"), sMessage));
			}
			textWriter.Flush();
			textWriter.Dispose();
			textWriter.Close();
		}
	}
}
