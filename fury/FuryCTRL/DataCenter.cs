using System;
using System.IO;
using System.Windows.Forms;

namespace FuryCTRL;

public static class DataCenter
{
	internal static Uri Uri_AppIcon = new Uri("pack://application:,,,/Images/FURY_CTRL_Black.ico", UriKind.Absolute);

	internal static NotifyIcon notifyIcon = null;

	internal static string CurrentWorkPath = AppDomain.CurrentDomain.BaseDirectory;

	internal static string ProjectName = "FURY CTRL";

	internal static MainWindow _MainWindow = null;

	internal static bool FuryCTRL_WindowStatus = false;

	internal static bool bStartUp = true;

	internal static string TaskId = "FURYCTRL_UWP";

	internal static bool bShowGDPR = true;

	internal static string Path_GDPR_Switch = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "gdpr.dat");

	internal static string Path_StartUp_Switch = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DisableStartup.dat");

	internal static bool bStartupApp = false;
}
