namespace FuryCTRL;

public class LanguageItem
{
	public uint Id { get; set; }

	public string Name { get; set; }

	public LanguageItem(string name, uint id)
	{
		Id = id;
		Name = name;
	}
}
