using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace FuryCTRL;

internal class INI
{
	public string path;

	private static string DirPath = $"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\Kingston Fury\\FuryCTRL\\Profile\\";

	[DllImport("kernel32")]
	private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

	[DllImport("kernel32")]
	private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

	[DllImport("kernel32.dll")]
	private static extern bool ReleaseMutex(IntPtr hMutex);

	public INI(string INIPath)
	{
		if (!Directory.Exists(DirPath))
		{
			Directory.CreateDirectory(DirPath);
		}
		path = DirPath + INIPath;
	}

	public void IniWriteValue(string Section, string Key, string Value)
	{
		WritePrivateProfileString(Section, Key, Value, path);
	}

	public string IniReadValue(string Section, string Key)
	{
		StringBuilder stringBuilder = new StringBuilder(10240);
		GetPrivateProfileString(Section, Key, "", stringBuilder, 10240, path);
		return stringBuilder.ToString();
	}

	public string IniReadValue(string Section, string Key, string DefaultValue)
	{
		StringBuilder stringBuilder = null;
		try
		{
			stringBuilder = new StringBuilder(10240);
			GetPrivateProfileString(Section, Key, "", stringBuilder, 10240, path);
			return (stringBuilder.Length > 0) ? stringBuilder.ToString() : DefaultValue;
		}
		catch
		{
			return string.Empty;
		}
	}
}
