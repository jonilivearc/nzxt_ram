using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;

namespace FuryCTRL;

public class UC_ColorPalette3 : UserControl, IComponentConnector
{
	private string ClickItemName = "";

	private int ColorBlock_FocusIndex = -1;

	private byte[][] Array_ColorBlockValue;

	private Geometry[] Array_GeometryData;

	public Path[] Array_PathBlock;

	private SolidColorBrush PathBorder_Default;

	private SolidColorBrush PathBorder_Focus;

	private SolidColorBrush PathBorder_Disable;

	private byte[] Array_BaseColorValue = new byte[3] { 255, 0, 0 };

	public byte[] Array_CurrentColorValue = new byte[3] { 255, 0, 0 };

	private int CircleR;

	private double BrightnessOffest = 0.5;

	private LinearGradientBrush LinearGradientBrush_Brush = new LinearGradientBrush();

	private double CircleRadius;

	private double Area_Circle_Sum;

	private double Area_InSideCircle;

	private double Area_OutSideCircle;

	private double CircleAngle;

	private bool LockTextSync;

	private bool ColorPalette_Focus;

	private bool BrightnessBar_Focus;

	public bool bColor_TextChanged;

	private bool bShift;

	private Func<object> set_color_Callback;

	internal Canvas Canvas_Main;

	internal Image Image_ColorPalette;

	internal Ellipse Ellipse_CurrentColor;

	internal Rectangle Rectangle_Brightness;

	internal TextBox Text_ColorR;

	internal TextBox Text_ColorG;

	internal TextBox Text_ColorB;

	private bool _contentLoaded;

	public new bool IsEnabled
	{
		set
		{
			ControlUI(AllSwitch: true, value);
		}
	}

	public Func<object> SetColorCallback
	{
		get
		{
			return set_color_Callback;
		}
		set
		{
			set_color_Callback = value;
		}
	}

	public int ColorBlockFocus
	{
		get
		{
			return ColorBlock_FocusIndex;
		}
		set
		{
			if (ColorBlock_FocusIndex > -1)
			{
				Array_PathBlock[ColorBlock_FocusIndex].Stroke = PathBorder_Default;
			}
			if (value == -1)
			{
				ColorBlock_FocusIndex = -1;
			}
			if (value > -1 && value < 10)
			{
				ColorBlock_FocusIndex = value;
				Array_PathBlock[ColorBlock_FocusIndex].Stroke = PathBorder_Focus;
				ParseRGB(Array_ColorBlockValue[ColorBlock_FocusIndex]);
				Ellipse_CurrentColor.Fill = new SolidColorBrush(Color.FromArgb((byte)(Image_ColorPalette.IsEnabled ? byte.MaxValue : 60), Array_CurrentColorValue[0], Array_CurrentColorValue[1], Array_CurrentColorValue[2]));
				ParseBrightness(Image_ColorPalette.IsEnabled);
				SyncTextColor();
				if (ColorBlock_FocusIndex < 4)
				{
					ControlUI(AllSwitch: false, IsEnabled: true);
				}
				else
				{
					ControlUI(AllSwitch: false, IsEnabled: false);
				}
			}
		}
	}

	public byte[] GetCurrentRGB
	{
		get
		{
			if (ColorBlock_FocusIndex == -1)
			{
				return Array_CurrentColorValue;
			}
			if (ColorBlock_FocusIndex < 4)
			{
				return Array_CurrentColorValue;
			}
			return Array_ColorBlockValue[ColorBlock_FocusIndex];
		}
	}

	public UC_ColorPalette3()
	{
		InitializeComponent();
		PathBorder_Default = new SolidColorBrush(Color.FromArgb(0, 60, 60, 60));
		PathBorder_Focus = TryFindResource("ML_Main_MultiColor_Label_Foreground") as SolidColorBrush;
		PathBorder_Disable = new SolidColorBrush(Color.FromArgb(60, 60, 60, 60));
	}

	private void UserControl_Loaded(object sender, RoutedEventArgs e)
	{
		base.Loaded -= UserControl_Loaded;
		CircleRadius = Image_ColorPalette.Width / 2.0;
		Area_InSideCircle = (int)(CircleRadius * 0.67 * (CircleRadius * 0.67));
		Area_OutSideCircle = CircleRadius * CircleRadius;
		ParseBrightness(Image_ColorPalette.IsEnabled);
	}

	private void ColorBlock_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
	{
		ClickItemName = (sender as Path).Name;
	}

	private void ColorBlock_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
	{
		if (ClickItemName.Equals((sender as Path).Name))
		{
			if (ColorBlock_FocusIndex > -1)
			{
				Panel.SetZIndex(Array_PathBlock[ColorBlock_FocusIndex], 0);
				Array_PathBlock[ColorBlock_FocusIndex].Stroke = PathBorder_Default;
			}
			if (ColorBlock_FocusIndex > -1 && ColorBlock_FocusIndex.Equals((sender as Path).Tag))
			{
				if (ColorBlock_FocusIndex >= 4)
				{
					ControlUI(AllSwitch: false, IsEnabled: true);
				}
				ColorBlock_FocusIndex = -1;
			}
			else
			{
				ColorBlock_FocusIndex = (int)(sender as Path).Tag;
				Array_PathBlock[ColorBlock_FocusIndex].Stroke = PathBorder_Focus;
				Panel.SetZIndex(Array_PathBlock[ColorBlock_FocusIndex], 999);
				if (ColorBlock_FocusIndex < 4)
				{
					ParseRGB(Array_ColorBlockValue[ColorBlock_FocusIndex]);
					Ellipse_CurrentColor.Fill = new SolidColorBrush(Color.FromArgb((byte)(Image_ColorPalette.IsEnabled ? byte.MaxValue : 60), Array_CurrentColorValue[0], Array_CurrentColorValue[1], Array_CurrentColorValue[2]));
					ParseBrightness(Image_ColorPalette.IsEnabled);
					SyncTextColor();
					ControlUI(AllSwitch: false, IsEnabled: true);
				}
				else
				{
					ControlUI(AllSwitch: false, IsEnabled: false);
				}
			}
		}
		else
		{
			ClickItemName = "";
		}
		SetColorCallback();
	}

	private void Image_ColorPalette_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
	{
		ColorPalette_Focus = false;
		ParseCirclePoint(e.GetPosition(Image_ColorPalette).X, e.GetPosition(Image_ColorPalette).Y);
		ParseRectanglePoint(120.0);
		SyncColorBlock();
	}

	private void Image_ColorPalette_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
	{
		ColorPalette_Focus = true;
		ParseCirclePoint(e.GetPosition(Image_ColorPalette).X, e.GetPosition(Image_ColorPalette).Y);
		SyncColorBlock();
	}

	private void Image_ColorPalette_PreviewMouseMove(object sender, MouseEventArgs e)
	{
		if (ColorPalette_Focus)
		{
			ParseCirclePoint(e.GetPosition(Image_ColorPalette).X, e.GetPosition(Image_ColorPalette).Y);
			SyncColorBlock();
		}
	}

	private void Image_ColorPalette_MouseLeave(object sender, MouseEventArgs e)
	{
		ColorPalette_Focus = false;
	}

	private void Rectangle_Brightness_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
	{
		ColorPalette_Focus = false;
		ParseRectanglePoint(e.GetPosition(Rectangle_Brightness).Y);
		SyncColorBlock();
	}

	private void Rectangle_Brightness_PreviewMouseMove(object sender, MouseEventArgs e)
	{
		if (BrightnessBar_Focus)
		{
			ParseRectanglePoint(e.GetPosition(Rectangle_Brightness).Y);
			SyncColorBlock();
		}
	}

	private void Rectangle_Brightness_PreviewMouseUp(object sender, MouseButtonEventArgs e)
	{
		BrightnessBar_Focus = true;
		if (BrightnessBar_Focus)
		{
			ParseRectanglePoint(e.GetPosition(Rectangle_Brightness).Y);
			SyncColorBlock();
		}
	}

	private void Rectangle_Brightness_MouseLeave(object sender, MouseEventArgs e)
	{
		BrightnessBar_Focus = false;
	}

	private void ParseRectanglePoint(double Y)
	{
		BrightnessOffest = Y / Rectangle_Brightness.Height;
		if (BrightnessOffest <= 0.5)
		{
			Array_CurrentColorValue = ParseHSV(CircleR, BrightnessOffest, 1.0);
		}
		else
		{
			Array_CurrentColorValue = ParseHSV(CircleR, 1.0, 1.0 - BrightnessOffest);
		}
		if (Y < 2.0)
		{
			Array_CurrentColorValue[0] = byte.MaxValue;
			Array_CurrentColorValue[1] = byte.MaxValue;
			Array_CurrentColorValue[2] = byte.MaxValue;
		}
		if (Y > 268.0)
		{
			Array_CurrentColorValue[0] = 0;
			Array_CurrentColorValue[1] = 0;
			Array_CurrentColorValue[2] = 0;
		}
		Ellipse_CurrentColor.Fill = new SolidColorBrush(Color.FromArgb((byte)(Image_ColorPalette.IsEnabled ? byte.MaxValue : 60), Array_CurrentColorValue[0], Array_CurrentColorValue[1], Array_CurrentColorValue[2]));
		SyncTextColor();
	}

	private void TextBox_Color_PreviewKeyDown(object sender, KeyEventArgs e)
	{
		if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
		{
			bShift = true;
		}
		if (bShift || ((e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9) && e.Key != Key.Back && e.Key != Key.Delete && e.Key != Key.Left && e.Key != Key.Right && e.Key != Key.Return))
		{
			e.Handled = true;
		}
		else if (e.Key == Key.Return)
		{
			if (string.IsNullOrWhiteSpace((sender as TextBox).Text))
			{
				(sender as TextBox).Text = "0";
				e.Handled = true;
				return;
			}
			ParseRGB(new byte[3]
			{
				Convert.ToByte(Text_ColorR.Text),
				Convert.ToByte(Text_ColorG.Text),
				Convert.ToByte(Text_ColorB.Text)
			});
			Ellipse_CurrentColor.Fill = new SolidColorBrush(Color.FromArgb((byte)(Image_ColorPalette.IsEnabled ? byte.MaxValue : 60), Array_CurrentColorValue[0], Array_CurrentColorValue[1], Array_CurrentColorValue[2]));
			ParseBrightness(Image_ColorPalette.IsEnabled);
			SyncColorBlock();
			SetColorCallback();
		}
	}

	private void TextBox_Color_PreviewKeyUp(object sender, KeyEventArgs e)
	{
		if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
		{
			bShift = false;
		}
	}

	private void TextBox_Color_TextChanged(object sender, TextChangedEventArgs e)
	{
		if (!LockTextSync && int.TryParse((sender as TextBox).Text, out var result))
		{
			if (result > 255)
			{
				result = 255;
				(sender as TextBox).Text = "255";
			}
			if (string.IsNullOrWhiteSpace(Text_ColorR.Text))
			{
				Text_ColorR.Text = "0";
			}
			if (string.IsNullOrWhiteSpace(Text_ColorG.Text))
			{
				Text_ColorG.Text = "0";
			}
			if (string.IsNullOrWhiteSpace(Text_ColorB.Text))
			{
				Text_ColorB.Text = "0";
			}
			ParseRGB(new byte[3]
			{
				Convert.ToByte(Text_ColorR.Text),
				Convert.ToByte(Text_ColorG.Text),
				Convert.ToByte(Text_ColorB.Text)
			});
			Ellipse_CurrentColor.Fill = new SolidColorBrush(Color.FromArgb((byte)(Image_ColorPalette.IsEnabled ? byte.MaxValue : 60), Array_CurrentColorValue[0], Array_CurrentColorValue[1], Array_CurrentColorValue[2]));
			ParseBrightness(Image_ColorPalette.IsEnabled);
			SyncColorBlock();
			SetColorCallback();
			bColor_TextChanged = true;
		}
	}

	private void TextBox_Color_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
	{
		if (string.IsNullOrWhiteSpace((sender as TextBox).Text))
		{
			(sender as TextBox).Text = "0";
		}
	}

	public void SetSelectionEnable(int index, bool enabled)
	{
	}

	private void ControlUI(bool AllSwitch, bool IsEnabled)
	{
		Image_ColorPalette.IsEnabled = IsEnabled;
		Ellipse_CurrentColor.Fill = new SolidColorBrush(Color.FromArgb((byte)(IsEnabled ? byte.MaxValue : 60), Array_CurrentColorValue[0], Array_CurrentColorValue[1], Array_CurrentColorValue[2]));
		Rectangle_Brightness.IsEnabled = IsEnabled;
		ParseBrightness(IsEnabled);
		TextBox text_ColorR = Text_ColorR;
		TextBox text_ColorG = Text_ColorG;
		bool flag2 = (Text_ColorB.IsEnabled = IsEnabled);
		bool isEnabled = (text_ColorG.IsEnabled = flag2);
		text_ColorR.IsEnabled = isEnabled;
	}

	private void SyncColorBlock()
	{
		if (ColorBlock_FocusIndex > -1 && ColorBlock_FocusIndex < 4)
		{
			Array_ColorBlockValue[ColorBlock_FocusIndex][0] = Array_CurrentColorValue[0];
			Array_ColorBlockValue[ColorBlock_FocusIndex][1] = Array_CurrentColorValue[1];
			Array_ColorBlockValue[ColorBlock_FocusIndex][2] = Array_CurrentColorValue[2];
			Array_PathBlock[ColorBlock_FocusIndex].Fill = new SolidColorBrush(Color.FromRgb(Array_CurrentColorValue[0], Array_CurrentColorValue[1], Array_CurrentColorValue[2]));
		}
		SetColorCallback();
	}

	public void SyncTextColor()
	{
		LockTextSync = true;
		Text_ColorR.Text = Array_CurrentColorValue[0].ToString();
		Text_ColorG.Text = Array_CurrentColorValue[1].ToString();
		Text_ColorB.Text = Array_CurrentColorValue[2].ToString();
		LockTextSync = false;
	}

	private void ParseCirclePoint(double X, double Y)
	{
		Area_Circle_Sum = (X - CircleRadius) * (X - CircleRadius) + (Y - CircleRadius) * (Y - CircleRadius);
		if (Area_Circle_Sum >= Area_InSideCircle && Area_Circle_Sum <= Area_OutSideCircle)
		{
			CircleAngle = (float)Math.Atan2(Y - CircleRadius, X - CircleRadius) * -1f;
			if (CircleAngle < 0.0)
			{
				CircleR = 360 + (int)(CircleAngle * (180.0 / Math.PI) - 180.0);
			}
			else
			{
				CircleR = (int)(CircleAngle * (180.0 / Math.PI)) + 180;
			}
			if (CircleR < 0 || CircleR >= 360)
			{
				CircleR = 0;
			}
			Array_BaseColorValue = ParseHSV(CircleR, 1.0, 1.0);
			if (BrightnessOffest <= 0.5)
			{
				Array_CurrentColorValue = ParseHSV(CircleR, BrightnessOffest, 1.0);
			}
			else
			{
				Array_CurrentColorValue = ParseHSV(CircleR, 1.0, 1.0 - BrightnessOffest);
			}
			Ellipse_CurrentColor.Fill = new SolidColorBrush(Color.FromArgb((byte)(Image_ColorPalette.IsEnabled ? byte.MaxValue : 60), Array_CurrentColorValue[0], Array_CurrentColorValue[1], Array_CurrentColorValue[2]));
			ParseBrightness(Image_ColorPalette.IsEnabled);
			SyncTextColor();
		}
	}

	public void ParseRGB(byte[] In_RGB)
	{
		if (In_RGB[0] == In_RGB[1] && In_RGB[1] == In_RGB[2])
		{
			Array_BaseColorValue[0] = byte.MaxValue;
			Array_BaseColorValue[1] = byte.MaxValue;
			Array_BaseColorValue[2] = byte.MaxValue;
			Array_CurrentColorValue[0] = In_RGB[0];
			Array_CurrentColorValue[1] = In_RGB[0];
			Array_CurrentColorValue[2] = In_RGB[0];
			Rectangle_Brightness.Fill = new SolidColorBrush(Color.FromArgb(In_RGB[0], Array_BaseColorValue[0], Array_BaseColorValue[1], Array_BaseColorValue[2]));
			BrightnessOffest = 1.0 - (double)(int)In_RGB[0] / 255.0;
			return;
		}
		double num = (double)(int)In_RGB[0] / 255.0;
		double num2 = (double)(int)In_RGB[1] / 255.0;
		double num3 = (double)(int)In_RGB[2] / 255.0;
		double num4 = Math.Max(num, Math.Max(num2, num3));
		double num5 = Math.Min(num, Math.Min(num2, num3));
		double num6 = num4 - num5;
		if (num4 == num)
		{
			if (num2 >= num3)
			{
				CircleR = Convert.ToInt32(360.0 - 60.0 * ((num2 - num3) / num6));
			}
			else
			{
				CircleR = Convert.ToInt32(Math.Abs(60.0 * ((num2 - num3) / num6)));
			}
		}
		else if (num4 == num2)
		{
			CircleR = Convert.ToInt32(360.0 - (60.0 * ((num3 - num) / num6) + 120.0));
		}
		else if (num4 == num3)
		{
			CircleR = Convert.ToInt32(360.0 - (60.0 * ((num - num2) / num6) + 240.0));
		}
		BrightnessOffest = 1.0 - (num4 + num5) / 2.0;
		Array_BaseColorValue = ParseHSV(CircleR, 1.0, 1.0);
		Array_CurrentColorValue[0] = In_RGB[0];
		Array_CurrentColorValue[1] = In_RGB[1];
		Array_CurrentColorValue[2] = In_RGB[2];
	}

	private byte[] ParseHSV(int In_CurrentR, double S, double V)
	{
		byte[] array = new byte[3];
		int num = 0;
		if (S != 1.0)
		{
			S *= 2.0;
		}
		if (V != 1.0)
		{
			V *= 2.0;
		}
		double num2 = ((In_CurrentR != 360) ? ((double)((float)(359 - In_CurrentR) / 60f)) : 0.0);
		num = (int)Math.Truncate(num2);
		double num3 = num2 - (double)num;
		double num4 = V * (1.0 - S);
		double num5 = V * (1.0 - S * num3);
		double num6 = V * (1.0 - S * (1.0 - num3));
		switch (num)
		{
		case 0:
			array = new byte[3]
			{
				(byte)(V * 255.0),
				(byte)(num6 * 255.0),
				(byte)(num4 * 255.0)
			};
			break;
		case 1:
			array = new byte[3]
			{
				(byte)(num5 * 255.0),
				(byte)(V * 255.0),
				(byte)(num4 * 255.0)
			};
			break;
		case 2:
			array = new byte[3]
			{
				(byte)(num4 * 255.0),
				(byte)(V * 255.0),
				(byte)(num6 * 255.0)
			};
			break;
		case 3:
			array = new byte[3]
			{
				(byte)(num4 * 255.0),
				(byte)(num5 * 255.0),
				(byte)(V * 255.0)
			};
			break;
		case 4:
			array = new byte[3]
			{
				(byte)(num6 * 255.0),
				(byte)(num4 * 255.0),
				(byte)(V * 255.0)
			};
			break;
		case 5:
			array = new byte[3]
			{
				(byte)(V * 255.0),
				(byte)(num4 * 255.0),
				(byte)(num5 * 255.0)
			};
			break;
		}
		if (array[0] > 210 && array[1] < 50 && array[2] < 50)
		{
			array[0] = byte.MaxValue;
			array[1] = 0;
			array[2] = 0;
		}
		else if (array[0] < 30 && array[1] > 230 && array[2] < 30)
		{
			array[0] = 0;
			array[1] = byte.MaxValue;
			array[2] = 0;
		}
		else if (array[0] < 30 && array[1] < 30 && array[2] > 230)
		{
			array[0] = 0;
			array[1] = 0;
			array[2] = byte.MaxValue;
		}
		return array;
	}

	private void ParseBrightness(bool In_IsEnable = true)
	{
		LinearGradientBrush_Brush.GradientStops.Clear();
		if (In_IsEnable)
		{
			LinearGradientBrush_Brush.GradientStops.Add(new GradientStop(Color.FromArgb(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue), 0.0));
			LinearGradientBrush_Brush.GradientStops.Add(new GradientStop(Color.FromArgb(byte.MaxValue, Array_BaseColorValue[0], Array_BaseColorValue[1], Array_BaseColorValue[2]), 0.5));
			LinearGradientBrush_Brush.GradientStops.Add(new GradientStop(Color.FromArgb(byte.MaxValue, 0, 0, 0), 1.0));
		}
		else
		{
			LinearGradientBrush_Brush.GradientStops.Add(new GradientStop(Color.FromArgb(60, byte.MaxValue, byte.MaxValue, byte.MaxValue), 0.0));
			LinearGradientBrush_Brush.GradientStops.Add(new GradientStop(Color.FromArgb(60, Array_BaseColorValue[0], Array_BaseColorValue[1], Array_BaseColorValue[2]), 0.5));
			LinearGradientBrush_Brush.GradientStops.Add(new GradientStop(Color.FromArgb(60, 0, 0, 0), 1.0));
		}
		Rectangle_Brightness.Fill = LinearGradientBrush_Brush;
	}

	public void InitBlockColor(byte[][] BlockColor, byte[] DefaultColor)
	{
		Array.Copy(BlockColor, Array_ColorBlockValue, BlockColor.Length);
		for (int i = 0; i < BlockColor.Length; i++)
		{
			if (i != 4)
			{
				Array_PathBlock[i].Fill = new SolidColorBrush(Color.FromRgb(Array_ColorBlockValue[i][0], Array_ColorBlockValue[i][1], Array_ColorBlockValue[i][2]));
			}
		}
		ParseRGB(DefaultColor);
		Ellipse_CurrentColor.Fill = new SolidColorBrush(Color.FromArgb((byte)(Image_ColorPalette.IsEnabled ? byte.MaxValue : 60), Array_CurrentColorValue[0], Array_CurrentColorValue[1], Array_CurrentColorValue[2]));
		ParseBrightness(Image_ColorPalette.IsEnabled);
		SyncTextColor();
	}

	public void InitBlockColor_Dis(byte[][] BlockColor, byte[] DefaultColor)
	{
		Array.Copy(BlockColor, Array_ColorBlockValue, BlockColor.Length);
		for (int i = 0; i < BlockColor.Length; i++)
		{
			if (i != 4)
			{
				Array_PathBlock[i].Fill = new SolidColorBrush(Color.FromArgb(60, Array_ColorBlockValue[i][0], Array_ColorBlockValue[i][1], Array_ColorBlockValue[i][2]));
			}
		}
		for (int j = 4; j < 10; j++)
		{
			Array_PathBlock[j].Opacity = 0.23;
		}
		ParseRGB(DefaultColor);
		Ellipse_CurrentColor.Fill = new SolidColorBrush(Color.FromArgb(60, Array_CurrentColorValue[0], Array_CurrentColorValue[1], Array_CurrentColorValue[2]));
		ParseBrightness(In_IsEnable: false);
		SyncTextColor();
	}

	[DebuggerNonUserCode]
	[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
	public void InitializeComponent()
	{
		if (!_contentLoaded)
		{
			_contentLoaded = true;
			Uri resourceLocator = new Uri("/FURYCTRL;component/uc_colorpalette3.xaml", UriKind.Relative);
			Application.LoadComponent(this, resourceLocator);
		}
	}

	[DebuggerNonUserCode]
	[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
	[EditorBrowsable(EditorBrowsableState.Never)]
	void IComponentConnector.Connect(int connectionId, object target)
	{
		switch (connectionId)
		{
		case 1:
			((UC_ColorPalette3)target).Loaded += UserControl_Loaded;
			break;
		case 2:
			Canvas_Main = (Canvas)target;
			break;
		case 3:
			Image_ColorPalette = (Image)target;
			Image_ColorPalette.PreviewMouseLeftButtonUp += Image_ColorPalette_PreviewMouseLeftButtonUp;
			Image_ColorPalette.PreviewMouseLeftButtonDown += Image_ColorPalette_PreviewMouseLeftButtonDown;
			Image_ColorPalette.MouseLeave += Image_ColorPalette_MouseLeave;
			Image_ColorPalette.PreviewMouseMove += Image_ColorPalette_PreviewMouseMove;
			break;
		case 4:
			Ellipse_CurrentColor = (Ellipse)target;
			break;
		case 5:
			Rectangle_Brightness = (Rectangle)target;
			Rectangle_Brightness.PreviewMouseUp += Rectangle_Brightness_PreviewMouseUp;
			Rectangle_Brightness.MouseLeave += Rectangle_Brightness_MouseLeave;
			break;
		case 6:
			Text_ColorR = (TextBox)target;
			Text_ColorR.PreviewKeyDown += TextBox_Color_PreviewKeyDown;
			Text_ColorR.PreviewKeyUp += TextBox_Color_PreviewKeyUp;
			Text_ColorR.TextChanged += TextBox_Color_TextChanged;
			Text_ColorR.PreviewLostKeyboardFocus += TextBox_Color_PreviewLostKeyboardFocus;
			break;
		case 7:
			Text_ColorG = (TextBox)target;
			Text_ColorG.PreviewKeyDown += TextBox_Color_PreviewKeyDown;
			Text_ColorG.PreviewKeyUp += TextBox_Color_PreviewKeyUp;
			Text_ColorG.TextChanged += TextBox_Color_TextChanged;
			Text_ColorG.PreviewLostKeyboardFocus += TextBox_Color_PreviewLostKeyboardFocus;
			break;
		case 8:
			Text_ColorB = (TextBox)target;
			Text_ColorB.PreviewKeyDown += TextBox_Color_PreviewKeyDown;
			Text_ColorB.PreviewKeyUp += TextBox_Color_PreviewKeyUp;
			Text_ColorB.TextChanged += TextBox_Color_TextChanged;
			Text_ColorB.PreviewLostKeyboardFocus += TextBox_Color_PreviewLostKeyboardFocus;
			break;
		default:
			_contentLoaded = true;
			break;
		}
	}
}
